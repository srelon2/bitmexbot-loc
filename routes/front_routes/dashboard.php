<?php
Route::middleware('auth')->group(function () {
    Route::get('/', 'IndexController@index')->name('home');
    Route::post('/bot/pattern/{id?}', 'BotController@pattern')->name('bot.pattern');
    Route::get('/bot/info/{id?}', 'BotController@info')->name('bot.info');
    Route::post('/bot/update/{id?}', 'BotController@update')->name('bot.update');
    Route::post('/bot/signals/search/{id}', 'BotController@signals_search')->name('bot.signals.search');
    Route::post('/bot/exections/search/{id}', 'BotController@exection_search')->name('bot.exections.search');
    Route::post('/bot/search', 'BotController@bot_search')->name('bot.search');

    Route::get('/settings', 'SettingsController@index')->name('settings');
    Route::post('/settings/keys/search', 'SettingsController@keys_search')->name('settings.keys.search');
    Route::get('/settings/keys/action', 'SettingsController@itemAction')->name('settings.keys.action');
    Route::post('/settings/keys/create', 'SettingsController@create')->name('settings.keys.create');
    Route::post('/settings/telegram/update', 'SettingsController@telUpdate')->name('settings.telegram.update');
    Route::get('/settings/telegram/destroy/{id}', 'SettingsController@telDestroy')->name('settings.telegram.destroy');
});