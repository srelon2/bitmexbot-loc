<?php
    Route::prefix('cron')->namespace('Cron')->group(function () {
        Route::get('/check-signals/tkvkofaiyulgfzreesuntlbeoibmtp/check', 'BotCronController@check')->name('cron.check');
        Route::get('/check-signals/tkvkofaiyulgfzreesuntlbeoibmtp/startSocket', 'BotCronController@startSocket')->name('cron.startSocket');
        Route::get('/check-signals/tkvkofaiyulgfzreesuntlbeoibmtp/setGrid', 'BotCronController@setGrid')->name('cron.setGrid');
        Route::get('/check-signals/tkvkofaiyulgfzreesuntlbeoibmtp/checkStart', 'BotCronController@checkStart')->name('cron.checkStart');
        Route::get('/check-signals/tkvkofaiyulgfzreesuntlbeoibmtp/startTechBot', 'BotCronController@startTechBot')->name('cron.startTechBot');
    });