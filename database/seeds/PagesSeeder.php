<?php
use Illuminate\Database\Seeder;
class PagesSeeder extends Seeder
{
    /**
    $data[]= [["type"=> "get",]]$data[]= [["type"=> "get",]]     *
     * @return void
     */
    public function run()
    {

        $data[]= [
            'url'=> 'Test',
            'title_ru'=> 'Test',
            'desc_ru'=> '',
            'content_ru'=> '<p>Content</p>',
            'meta_title_ru'=> '',
            'meta_desc_ru'=> '',
            'meta_key_ru'=> '',
            'id'=> '1',
        ];
        $data[]= [
            'url'=> 'sadsad',
            'title_ru'=> 'sdasda',
            'desc_ru'=> '<p>sdasadsad</p>',
            'content_ru'=> '<p>asdsad</p>',
            'meta_title_ru'=> '',
            'meta_desc_ru'=> '',
            'meta_key_ru'=> '',
            'id'=> '1',
        ];
        $data[]= [
            'url'=> 'sadsda',
            'title_ru'=> 'sadsdasda',
            'desc_ru'=> '<p>sdasdasda</p>',
            'content_ru'=> '<p>sdasdasdasda</p>',
            'meta_title_ru'=> '',
            'meta_desc_ru'=> '',
            'meta_key_ru'=> '',
            'id'=> '1',
        ];
        $data[]= [
            'url'=> 'Test',
            'title_ru'=> 'dsasda',
            'desc_ru'=> '<p>sdasdasd</p>',
            'content_ru'=> '<p>saddassadsad</p>',
            'meta_title_ru'=> '',
            'meta_desc_ru'=> '',
            'meta_key_ru'=> '',
            'id'=> '1',
        ];
        $data[]= [
            'url'=> 'dsasaddsa',
            'title_ru'=> 'dsasaddsa',
            'desc_ru'=> '<p>dsadsa dssad</p>',
            'content_ru'=> '<p>dsa adsadssa dsa dsaadsda</p>',
            'meta_title_ru'=> '',
            'meta_desc_ru'=> '',
            'meta_key_ru'=> '',
            'id'=> '2',
        ];
        $data[]= [
            'url'=> 'sdsadsdsdaq',
            'title_ru'=> 'sadsdasad',
            'desc_ru'=> '<p>dsasda dassdasad</p>',
            'content_ru'=> '<p>dsasad dsadsa</p>',
            'meta_title_ru'=> '',
            'meta_desc_ru'=> '',
            'meta_key_ru'=> '',
            'id'=> '6',
        ];
        $data[]= [
            'url'=> 'yivfyivf',
            'title_ru'=> 'ывфывф',
            'desc_ru'=> '<p>ывфывфывфыфв</p>',
            'content_ru'=> '<p>ывфыфвывфвыфы</p>',
            'meta_title_ru'=> '',
            'meta_desc_ru'=> '',
            'meta_key_ru'=> '',
            'id'=> '1',
        ];
        DB::table('pages')->insert($data);
    }
}