<?php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     *
        $str= '';
        foreach (collect($controller)->except(['updated_at', 'created_at', 'deleted_at']) as $key=> $value) {
        $str.="            '".$key."'=> '".$value."',\r\n";
        }
        $seeds= file(database_path('/seeds/admin/ControllersSeeder.php'));
        $seeds[count($seeds)-4].='        $data[]= ['."\r\n".$str."        ];\r\n";
        file_put_contents( database_path('/seeds/admin/ControllersSeeder.php'), $seeds );
     *
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminsSeeder::class);
        $this->call(RoutesSeeder::class);
        $this->call(LanguagesSeeder::class);
        $this->call(ControllersSeeder::class);

        $this->command->info('Таблица загружена данными!');
    }
}
