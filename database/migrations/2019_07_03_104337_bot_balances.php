<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BotBalances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('bot_balances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('bot_id')->nullable();
            $table->integer('api_id')->nullable();
            $table->string('currency_pair')->default('XBTUSD');
            $table->integer('count')->default(0);
            $table->integer('account')->nullable();
            $table->string('avg_price')->nullable();
            $table->double('price', 20.8)->nullable();
            $table->string('order_qty')->nullable();
            $table->string('leaves_qty')->nullable();
            $table->string('current_qty')->nullable();
            $table->string('status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('bot_balances');
    }
}
