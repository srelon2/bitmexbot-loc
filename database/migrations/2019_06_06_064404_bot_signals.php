<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BotSignals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('bot_signals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('count');
            $table->integer('api_id');
            $table->integer('user_id');
            $table->integer('bot_id');
            $table->string('account');
//            $table->double('take_profit', 20.8);
//            $table->double('stop_loss', 20.8);
            $table->string('order_id');
            $table->string('order_qty');
            $table->string('leaves_qty');
            $table->string('cum_qty');
            $table->string('price');
            $table->string('side');
            $table->string('order_type');
            $table->string('order_status');
            $table->integer('status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('bot_signals');
    }
}
