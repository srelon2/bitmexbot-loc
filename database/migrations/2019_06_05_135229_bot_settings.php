<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BotSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('bot_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('api_id')->nullable();
            $table->string('name')->nullable();
            $table->double('take_profit', 20.8)->nullable();
            $table->double('stop_loss', 20.8)->nullable();
            $table->double('amount', 20.8)->nullable();
            $table->string('currency_pair')->default('XBTUSD');
            $table->integer('count')->default(0);
            $table->double('points', 20.8)->nullable();
            $table->text('steps')->default(0);
            $table->integer('active')->default(0);
            $table->integer('saved')->default(0);
            $table->integer('closed')->default(0);
            $table->integer('timer')->default(0);
            $table->integer('account')->nullable();
            $table->timestamp('start_date');
            $table->string('liquidation_price')->nullable();
            $table->integer('check_position')->default(0);
            $table->string('avg_price')->nullable();
            $table->string('current_qty')->nullable();
            $table->string('position')->default('All');
            $table->string('type')->default('default');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bot_settings');
    }
}
