<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BotExections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('bot_exections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('count');
            $table->integer('api_id');
            $table->integer('user_id');
            $table->integer('bot_id');
            $table->string('account');
            $table->string('type');
            $table->string('exec_id');
//            $table->double('take_profit', 20.8);
//            $table->double('stop_loss', 20.8);
            $table->string('currency_pair');
            $table->string('price');
            $table->string('avg_px');
            $table->string('order_id');
            $table->string('order_qty');
            $table->string('leaves_qty');
            $table->string('cum_qty');
            $table->string('side');
            $table->string('type');
            $table->string('commission');
            $table->string('exec_cost');
            $table->string('exec_comm');
            $table->string('order_type');
            $table->string('order_status');
            $table->string('home_notional');
            $table->string('foreign_notional');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('bot_exections');
    }
}
