@extends('admin.layouts.admin_layout')

@section('content')
    @if($edit)
        <form method="post" action="{{route('admin.routes.update', ['id'=>(isset($item) ? $item->id : '')])}}">
            @csrf
            @endif
            <div class="card pd-20 pd-sm-20">
                <div class="form-layout">
                    <p class="mg-b-20 mg-sm-b-30">
                        @if($edit)
                            @if(isset($item))Изменение маршрута Админ панели <a class="btn btn-success btn-sm mg-l-15" href="{{route('admin')}}"><i class="fa fa-plus mg-r-5"></i> Новый маршрут</a>
                            @else
                                Создание нового маршрута Админ панели
                            @endif
                        @else
                            Просмотр маршрута
                        @endif
                    </p>
                    <div class="row">
                        <div class="form-group col-lg-6 col-xl-1">
                            @php
                                $types= ['get', 'post'];
                            @endphp
                            <label for="type">Type: <span class="tx-danger">*</span></label>
                            <select class="form-control select2 {{ $errors->has('type') ? ' parsley-error' : '' }}" name="type" id='type'>
                                @foreach($types as $type)
                                    <option @if((isset($item) && $type== $item->type) || old('type')==$type) selected="selected" @endif >{{$type}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('type'))
                                <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('type') }}</li></ul>
                        </span>
                            @endif
                        </div>
                        <div class="form-group col-lg-6 col-xl-3">
                            <label for="link">Link: <span class="tx-danger">*</span></label>
                            <input type="text" name="link" id="link" class="form-control {{ $errors->has('link') ? ' parsley-error' : '' }}" placeholder="Enter link" value="{{(isset($item) ? $item->link : old('link') )}}" >
                            @if ($errors->has('link'))
                                <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('link') }}</li></ul>
                        </span>
                            @endif
                        </div>
                        <div class="form-group col-lg-6 col-xl-5">
                            <label for="controller">Controller: <span class="tx-danger">*</span></label>

                            <div class="input-group grout-toggle">
                        <span class="input-group-addon bg-transparent">
                          <label class="ckbox">
                            <input name="add" value="1" type="checkbox" class="checkbox-toggle {{ $errors->has('controller') ? ' parsley-error' : '' }}"><span class=" mg-l-5">Add</span>
                          </label>
                        </span>
                                <select name="controller" class="form-control select2 container-select2 {{ $errors->has('controller') ? ' parsley-error' : '' }}" id='currency'>
                                    @foreach($controllers as $controller)
                                        <option @if((isset($item) && $controller->id== $item->controller) || old('controller')==$controller->id) selected="selected" @endif >{{$controller->name}}</option>
                                    @endforeach
                                </select>
                                <input type="text" class="form-control form-toggle form-add d-none" placeholder="Enter controller" name="newController" value="{{(old('newController')==null) ? 'AdminController' : old('newController')}}">

                            </div>
                            @if ($errors->has('controller'))
                                <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('controller') }}</li></ul>
                        </span>
                            @endif
                            @if ($errors->has('newController'))
                                <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('newController') }}</li></ul>
                        </span>
                            @endif
                            <input name="controller" class="form-controller" type="hidden" value="{{(isset($item) ? $item->controller :  (old('controller')==null) ? $controllers[0] : old('controller') )}}">
                        </div>
                        <div class="form-group col-lg-6 col-xl-3">
                            <label for="function">Function: <span class="tx-danger">*</span></label>
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input type="text" name="function" id="function" class="form-control {{ $errors->has('function') ? ' parsley-error' : '' }}" placeholder="Enter function" value="{{(isset($item) ? $item->function : old('function') )}}" >
                            </div>
                            @if ($errors->has('function'))
                                <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('function') }}</li></ul>
                        </span>
                            @endif
                        </div>
                        <div class="form-group col-xl-6">
                            <label for="title">Title:</label>
                            <input type="text" name="title" id="title" class="form-control {{ $errors->has('title') ? ' parsley-error' : '' }}" placeholder="Enter title" value="{{(isset($item) ? $item->title : old('title') )}}" >
                            @if ($errors->has('title'))
                                <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('title') }}</li></ul>
                        </span>
                            @endif
                        </div>

                        <div class="form-group col-xl-6">
                            <label for="name">Name: <span class="tx-danger">*</span></label>
                            <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' parsley-error' : '' }}" placeholder="Enter name" value="{{(isset($item) ? $item->name : old('name') )}}" >
                            @if ($errors->has('name'))
                                <span class="help-block">
                            <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="control-label">Description</label>
                        <textarea id="description"  name="description" class="form-control text-area">
                        {{(isset($item) ? $item->description : old('description') )}}
                </textarea>
                    </div>
                </div>
                <div class="form-layout-footer">
                    @if($edit)
                        <button type="submit" class="btn btn-info mg-r-5"><i class="fa fa-save mg-r-10"></i>Сохранить</button>
                    @endif
                    <a href="{{ route('admin.routes') }}" class="btn btn-secondary">Назад</a>
                </div>
            </div>
            @if($edit)
        </form>
    @endif
@endsection


@push('scripts')
<script>

    $(document).ready(function () {
        $(".select2").select2();

        $(".checkbox-toggle").click(function(){toggleCheckbox()});
//            $('.container-select2').on('select2:select', function (e) {
//                $('.form-controller').val(e.params.data['id']);
//            });
//            $('.form-add').on('change', function () {
//                $('.form-controller').val(this.value);
//            });
        toggleCheckbox();
        CKEDITOR.replace('description');

        @if(!$edit)
            $('.form-control').prop('readonly', true);
        $('.select2').prop('disabled', true);
        @endif
    });

    function toggleCheckbox() {
        if($('.checkbox-toggle').prop('checked')) {
            $('.form-add').removeClass('d-none');
            $('.grout-toggle .select2').addClass('d-none');
        } else {
            $('.grout-toggle .select2').removeClass('d-none');
            $('.form-add').addClass('d-none');
        }
    }
</script>

@endpush