@extends('admin.layouts.layout')

@section('content')
    <div class="col-lg-12"></div>
    <div class="col-lg-12">
        <form class="form-horizontal login-form" role="form" novalidate="novalidate" method="POST" action="{{url('/connect/bd')}}">
        {{ csrf_field() }}
        <h5 class="tx-gray-800 mg-b-25">Подключение к базе данных</h5>

            <div class="form-group">
                <label class="form-control-label">DB_DATABASE:</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="database" name="database"/>
                {{--@if ($errors->has('email'))--}}
                    {{--<span class="help-block">--}}
                        {{--<ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('email') }}</li></ul>--}}
                    {{--</span>--}}
                {{--@endif--}}
            </div><!-- form-group -->
            <div class="form-group">
                <label class="form-control-label">DB_USERNAME:</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="username" name="username" />
                {{--@if ($errors->has('password'))--}}
                    {{--<span class="help-block">--}}
                        {{--<ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('password') }}</li></ul>--}}
                    {{--</span>--}}
                {{--@endif--}}
            </div><!-- form-group -->
            <div class="form-group">
                <label class="form-control-label">DB_PASSWORD:</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="password" name="password" />
                {{--@if ($errors->has('password'))--}}
                    {{--<span class="help-block">--}}
                        {{--<ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('password') }}</li></ul>--}}
                    {{--</span>--}}
                {{--@endif--}}
            </div><!-- form-group -->
            <button type="submit" class="btn btn-block">Создать подлючение</button>
        </form>
    </div><!-- col-7 -->
@endsection