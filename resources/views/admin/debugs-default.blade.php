
<div class="vtabs customvtab">
    <ul class="nav nav-tabs tabs-vertical" role="tablist">
        @foreach($files as $file)
            <li class="nav-item wd-200"><a href="?log={{ base64_encode($file) }}" class="nav-link @if ($current_file == $file) active @endif"><i class="voyager-file-text"></i> {{$file}}</a></li>
        @endforeach
    </ul>
    <div class="table-responsive tab-content">
        <table id="table-log" class="table table-striped" >
            <thead>
            <tr>
                <th class="pd-r-20-force">уровень</th>
                <th class="text-center">Дата</th>
                <th>Ошибка</th>
            </tr>
            </thead>
            <tbody>
            @foreach($logs as $key => $log)
                <tr data-display="stack{{$key}}">
                    <td class="text-{{$log['level_class']}} level"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> &nbsp;{{$log['level']}}</td>
                    <td class="text-center">{{$log['date']}}</td>
                    <td>
                        @if ($log['stack'])
                            <a class="pull-right btn-open btn btn-default btn-xs" data-display="stack{{$key}}">
                                <span class="fa fa-angle-up"></span>
                            </a>
                        @endif
                        {{$log['text']}}
                        @if (isset($log['in_file']))
                            <br/>{{$log['in_file']}}
                        @endif
                        @if ($log['stack'])
                            <div class="stack" id="stack{{$key}}" style="display: none; white-space: pre-wrap;">{{ trim($log['stack']) }}</div>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>уровень</th>
                    <th>Дата</th>
                    <th>Ошибка</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>