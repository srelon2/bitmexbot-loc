<div class="col-lg-9 col-12">
    <div class="box">
        <div class="box-header">
            <a aria-controls="googleContent" aria-expanded="false" data-toggle="collapse" href="#googleContent">
                <h4 class="mg-0">
                    Google Authenticator
                    <div class="pull-right">
                        <button type="button" class="btn btn-sm btn-toggle {{($google_two_step) ? 'active' : ''}}" aria-pressed="false" autocomplete="off">
                            <div class="handle"></div>
                        </button>
                    </div>
                </h4>
            </a>
        </div>
        <div class="panel-collapse @if(session('table')!= 'google_two_step' || !$errors->has('google_two_step'))collapse @endif" id="googleContent" role="tabpanel">
            <div class="box-body row">
               <div id="qrcode" class="qrcode align-self-start wd-150 col-md-3"></div>
               <div class="col-md-9">
                    <p>
                        Используя Authentificator, сканируйте QR-code. Если отсканировать по каким то причинам не получилось, введите код вручную
                    </p>
                   <div class="form-group">
                       <label>Secret code</label>
                       <div class="input-group">
                           <input class="form-control" disabled type="text" value="{{$google2fa_secret}}">
                           <div class="input-group-prepend">
                               <button type="button" class="btn btn-success btn-copy"><i class="fa fa-copy"></i></button>
                           </div>
                       </div>
                   </div>
               </div>
            </div>
           <div class="box-footer">
               <form method="post" action="{{route('admin.settings.update')}}">
                   @csrf
                   <div class="form-group {{ $errors->has('google_two_step') ? ' error' : '' }}">
                       <label for="google_two_step">Введите код аутентификации, из Google Authenticator</label>
                       <div class="input-group">
                           <!-- /btn-group -->
                           <input type="text" name="google_two_step" id="google_two_step" class="form-control">
                           <div class="input-group-prepend">
                               <button type="submit" value="google_two_step" name="submit" class="btn {{($google_two_step) ? 'btn-danger' : 'btn-primary'}}">{{($google_two_step) ? 'Выключить' : 'Включить'}}</button>
                           </div>
                       </div>
                       @if ($errors->has('google_two_step'))
                           <span class="help-block">
                                <ul class="alert"><li>{{ $errors->first('google_two_step') }}</li></ul>
                            </span>
                       @endif
                   </div>
               </form>
           </div>
        </div>
    </div>
    <div class="box">
        <div class="box-header">
            <a aria-controls="emailContent" aria-expanded="false" data-toggle="collapse" href="#emailContent">
                <h4 class="mg-0">
                    Email Authenticator
                    <div class="pull-right">
                        <button type="button" class="btn btn-sm btn-toggle {{($email_two_step) ? 'active' : ''}}" aria-pressed="false" autocomplete="off">
                            <div class="handle"></div>
                        </button>
                    </div>
                </h4>
            </a>
        </div>
        <div class="panel-collapse  @if(session('table')!= 'email_two_step' || !$errors->has('email_two_step')) collapse @endif" id="emailContent" role="tabpanel">
            <div class="box-body row">
               <div id="qrcode" class="qrcode align-self-start wd-150 col-md-3"><img width="100%" src="{{asset('admin')}}/images/email-security.png"></div>
               <div class="col-md-9">
                    <p>
                        После нажатия на кропку, на ваш email будет выслан код для активации email верификации
                    </p>
                   <div class="form-group">
                       <a href="{{route('admin.setting.secret')}}" class="btn btn-info">Выслать код</a>
                   </div>
               </div>
            </div>
           <div class="box-footer">
               <form method="post" action="{{route('admin.settings.update')}}">
                   @csrf
                   <div class="form-group {{ $errors->has('email_two_step') ? ' error' : '' }}">
                       <label for="email_two_step">Введите код из email сообщения</label>
                       <div class="input-group">
                           <!-- /btn-group -->
                           <input type="text" id="email_two_step" name="email_two_step" class="form-control">
                           <div class="input-group-prepend">
                               <button type="submit" value="email_two_step" name="submit" class="btn {{($email_two_step) ? 'btn-danger' : 'btn-primary'}}">{{($email_two_step) ? 'Выключить' : 'Включить'}}</button>
                           </div>
                       </div>
                       @if ($errors->has('email_two_step'))
                           <span class="help-block">
                                <ul class="alert"><li>{{ $errors->first('email_two_step') }}</li></ul>
                            </span>
                       @endif
                   </div>
               </form>
           </div>
       </div>
    </div>
    <div class="box">
        <div class="box-header">
            <a aria-controls="emailContent" aria-expanded="false" data-toggle="collapse" href="#telegramContent">
                <h4 class="mg-0">
                    Telegram Authenticator
                    <div class="pull-right">
                        <button type="button" class="btn btn-sm btn-toggle {{($telegram_two_step) ? 'active' : ''}}" aria-pressed="false" autocomplete="off">
                            <div class="handle"></div>
                        </button>
                    </div>
                </h4>
            </a>
        </div>
        <div class="panel-collapse @if(session('table')!= 'telegram_two_step' || !$errors->has('telegram_two_step')) collapse @endif" id="telegramContent" role="tabpanel">
            <div class="box-body row">
               <div id="qrcode" class="qrcode align-self-start wd-150 col-md-3"><img width="100%" src="{{asset('admin')}}/images/email-security.png"></div>
               <div class="col-md-9">
                       @if(count($admin->telegram->where('active', 1)))
                           <p>
                               После нажатия на кропку, на ваш email будет выслан код для активации email верификации
                           </p>
                           <div class="form-group">
                               <form method="get" action="{{route('admin.setting.secret', ['type'=>'telegram_two_step'])}}">
                                   @csrf
                                   <label for="chat_id">Выберете Chat ID для аутентификации</label>
                                   <div class="input-group input-group-sm">
                                       <select class="form-control input-sm" id="chat_id" name="chat_id">
                                           @foreach($admin->telegram->where('active', 1) as $telegram)
                                               <option value="{{$telegram->id}}">{{$telegram->chat_id}}</option>
                                           @endforeach
                                       </select>
                                       <div class="input-group-prepend">
                                           <button class="btn btn-success">Выслать код</button>
                                       </div>
                                   </div>
                               </form>
                           </div>
                       @else
                           <p>
                               Введите свой Chat ID, чтобы привязать его к вашему аккаунту
                           </p>
                           <div class="form-group">
                               <form method="post" action="{{route('admin.settings.update')}}">
                                   @csrf
                                   <label for="chat_id">Введите свой Chat ID</label>
                                   <div class="input-group">
                                       <input class="form-control" id="chat_id" type="text" name="chat_id">
                                       <div class="input-group-prepend">
                                           <button type="submit" name="submit" value="chat_id" class="btn btn-success">Добавить телеграм</button>
                                       </div>
                                   </div>
                               </form>
                           </div>
                       @endif
               </div>
            </div>
           <div class="box-footer">
               <form method="post" action="{{route('admin.settings.update')}}">
                   @csrf
                   <div class="form-group {{ $errors->has('telegram_two_step') ? ' error' : '' }}">
                       <label for="telegram_two_step">Введите код из email сообщения</label>
                       <div class="input-group">
                           <!-- /btn-group -->
                           <input type="text" id="telegram_two_step" name="telegram_two_step" class="form-control">
                           <div class="input-group-prepend">
                               <button type="submit" value="telegram_two_step" name="submit" class="btn {{($telegram_two_step) ? 'btn-danger' : 'btn-primary'}}">{{($telegram_two_step) ? 'Выключить' : 'Включить'}}</button>
                           </div>
                       </div>
                       @if ($errors->has('telegram_two_step'))
                           <span class="help-block">
                                <ul class="alert"><li>{{ $errors->first('telegram_two_step') }}</li></ul>
                            </span>
                       @endif
                   </div>
               </form>
           </div>
       </div>
    </div>
    <div class="box">
        <div class="box-header bg-info">
            <h4 class="box-title">Aктивные сеансы</h4>
            <div class="pull-right">
                <a href="{{route('admin.administrators.logout.other')}}" class="btn btn-danger btn-flat btn-sm"><i class="fa fa-power-off mg-r-10"></i> Завершить другие сессии</a>
            </div>
        </div>
        <div class="box-body card">
            <div class="table-responsive">
                <table class="table table-striped data-table" >
                    <thead>
                        <tr>
                            <th class="wd-150">Date</th>
                            <th>Agent</th>
                            <th>ip</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @php
                        $getId= Session::getId();
                    @endphp
                    @foreach($sessions as $key => $item)
                        <tr>
                            <td>{{date('Y-m-d H:i:s',$item->last_activity)}}</td>
                            <td @if($getId==$item->id) class="tx-bold" @endif>{{$item->user_agent}}</td>
                            <td>{{$item->ip_address}}</td>
                            <td> <a class="btn btn-danger btn-sm" href="{{(Session::getId()== $item->id) ? route('admin.logout') : route('admin.administrators.logout.other', ['id'=> $item->id])}}"><i class="fa fa-remove mg-r-10"></i>завершить</a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->
        </div>
    </div>
    <div class="box">
        <div class="box-header bg-primary">
            <h4 class="box-title">Мои логи</h4>
        </div>
        <div class="box-body card">
            <div class="table-responsive">
                <table class="table table-striped data-table" >
                    <thead>
                        <tr>
                            <th class="wd-150">Date</th>
                            <th>Action</th>
                            <th>location</th>
                            <th>ip</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($logs as $key => $item)
                        <tr>
                            <td>{{$item->date}}</td>
                            <td>{{$item->desc}}</td>
                            <td>{{$item->location}}</td>
                            <td>{{$item->ip}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{asset('admin/assets/vendor_components/qrcode/jquery.qrcode.js') }}"  type="text/javascript" charset="utf-8" ></script>
    <script src="{{asset('admin/assets/vendor_components/qrcode/qrcode.js') }}"  type="text/javascript" charset="utf-8" ></script>

    <script>
        $(document).ready(function () {
            //jQuery('#qrcode').qrcode("this plugin is great");
            $('.data-table').DataTable({
                "order": [1, 'desc'],
                "language": {
                    "sLengthMenu": '',
                },
                "ajax": null,
            });
            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

            jQuery('#qrcode').qrcode({
                width: '150',
                height: '150',
                render	: "table",
                text	: "otpauth://totp/{{config('app.name')}}:{{Auth::guard('admin')->user()->email}}?secret={{$google2fa_secret}}&issuer={{config('app.name')}}"
            });
        });
    </script>
@endpush