@extends('admin.layouts.layout')


@section('content')
    <div class="box-body card">
        <form method="post" action="{{route('admin.console.command')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Console</label>
                <input class="form-control" placeholder="commands" type="text" name="terminal">
            </div>
            <div class="form-group">
                <label>Result</label>
                <textarea class="form-control ht-400" placeholder="" readonly=""></textarea>
            </div>
        </form>
    </div>
@endsection

@push('scripts')

@endpush