@extends('admin.layouts.layout')

@push('topBtn')
    @if($answer)
        <a href="{{route("admin.support.read", ['id'=>$item->id])}}" class="btn btn-outline-primary btn-sm mg-l-10">Просмотреть</a>
    @endif
@endpush

@section('content')
    <div class="card box-body">
        <div class="row">
            <div class="col-lg-3 col-12">
                <div class="h-p100 p-15 bg-light">
                    <a href="{{route('admin.supports')}}" class="btn btn-success btn-block btn-shadow margin-bottom">В почтовый яшик</a>
                    @include('admin.supports-right-bar')
                </div>
            </div>
            <div class="col-lg-9 col-12">
                <form method="post" action="{{route('admin.support.send', ((isset($item)) ? ['id'=> $item->id, 'type'=> $answer] : ''))}}" enctype="multipart/form-data">
                    @csrf
                    <div class="box bg-transparent no-shadow">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{'Отправить сообщение'}}</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="form-group {{ $errors->has('email') ? ' error' : '' }}">
                                <label for="email">Email</label>
                                <input id="email" class="form-control" placeholder="To:" type="email" name="email" @if($answer) readonly @endif value="{{(isset($item) ? $item->email : old('email') )}}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <ul class="alert"><li>{{ $errors->first('email') }}</li></ul>
                                    </span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="form-group {{ $errors->has('name') ? ' error' : '' }} col-md-6">
                                    <label for="name">Name</label>
                                    <input id="name" class="form-control" placeholder="name:" type="text" name="name" value="{{(isset($item) ? $item->name : old('name') )}}">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <ul class="alert"><li>{{ $errors->first('name') }}</li></ul>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('subject') ? ' error' : '' }} col-md-6">
                                    <label for="subject">Subject</label>
                                    <input id="subject" class="form-control" placeholder="Subject:" type="text" name="subject" value="{{(isset($item) ? $item->subject : old('subject') )}}">
                                    @if ($errors->has('subject'))
                                        <span class="help-block">
                                            <ul class="alert"><li>{{ $errors->first('subject') }}</li></ul>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('text') ? ' error' : '' }}">
                                <textarea id="compose-textarea" class="form-control" name="text">{{(isset($item) ? $item->text : old('text') )}}</textarea>
                                @if ($errors->has('text'))
                                    <span class="help-block">
                                        <ul class="alert"><li>{{ $errors->first('text') }}</li></ul>
                                    </span>
                                @endif
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<div class="btn btn-info btn-file">--}}
                                    {{--<i class="fa fa-paperclip"></i> Attachment--}}
                                    {{--<input type="file" name="attachment">--}}
                                {{--</div>--}}
                                {{--<p class="help-block">Max. 32MB</p>--}}
                            {{--</div>--}}
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-default" value="draft" name="submit"><i class="fa fa-pencil mg-r-5"></i> Черновик</button>
                                <button type="submit" class="btn btn-success" value="send" name="submit"><i class="fa fa-envelope-o mg-r-5"></i> Отправить</button>
                            </div>
                            <a href="{{route('admin.supports')}}" class="btn btn-danger"><i class="fa fa-times mg-r-5"></i> Отменить</a>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /. box -->
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{asset('admin')}}/assets/vendor_components/ckeditor/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        CKEDITOR.replace('compose-textarea');
    });
</script>

@endpush