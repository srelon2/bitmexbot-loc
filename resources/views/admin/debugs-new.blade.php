<table id="table-log" class="table table-striped" >
    <thead>
        <tr>
            <th class="text-center">Дата</th>
            <th>Ошибка</th>
            <th>Пользователь</th>
            <th>Контроллер</th>
            <th class="text-center">line</th>
        </tr>
    </thead>
    <tbody>
    @foreach($logs as $key => $log)
        @php
            if(isset($log->user_id) && isset($log->table)) {
            dd($log->table);
                $user= \Illuminate\Support\Facades\DB::table($log->table)->find($log->user_id);
            }
        @endphp
        <tr data-display="stack{{$key}}">
            <td class="text-center">{{$log->date}}</td>
            <td>{{$log->message}}</td>
            <td><strong class="tx-semibold">Table {{$log->table}}: </strong>{{(isset($user)) ? $user->email : 'Null'}}</td>
            <td>{{$log->controller}}</td>
            <td class="text-center">{{$log->line}}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Дата</th>
            <th>Ошибка</th>
            <th>Пользователь</th>
            <th>Контроллер</th>
            <th>line</th>
        </tr>
    </tfoot>
</table>