<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="{{asset('admin')}}/images/favicon.ico">

        <title>{{(isset($pagetitle)) ? $pagetitle : 'Title'}}</title>

        <!-- Bootstrap 4.0-->
        <link rel="stylesheet" href="{{asset('admin')}}/assets/vendor_components/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" href="{{asset('admin')}}/assets/vendor_components/bootstrap-switch/switch.css">

        <!-- xeditable css -->
        <link href="{{asset('admin')}}/assets/vendor_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />

        <!-- Bootstrap extend-->
        <link rel="stylesheet" href="{{asset('admin')}}/main/css/bootstrap-extend.css">

        <link rel="stylesheet" href="{{asset('admin')}}/main/css/master_style.css">


        <link rel="stylesheet" href="{{asset('admin')}}/css/helpers.css">
        <link rel="stylesheet" href="{{asset('/')}}/css/styles.css">
    </head>

    <body class="bg-lightest">
        <div class="wrapper">
            <div class="tx-center">
                <div class="mg-t-15">
                    @if(Auth::user())
                        <a class="btn btn-primary" href="{{ route('dashboard.home') }}">Dashboard</a>
                        <a class="btn btn-danger btn-outline" href="{{ route('logout') }}">Logout</a>
                    @else
                        <a class="btn btn-success" href="{{ route('login') }}">Login</a>
                        <a class="btn btn-primary btn-outline" href="{{ route('register') }}">Register</a>
                    @endif
                </div>
            </div>
            <section class="content">
                @yield('content')
            </section>
        </div>
    </body>

    <!-- jQuery 3 -->
    <script src="{{asset('admin')}}/assets/vendor_components/jquery/dist/jquery.js"></script>

    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('admin')}}/assets/vendor_components/jquery-ui/jquery-ui.js"></script>

    <!-- Bootstrap 4.0-->
    <script src="{{asset('admin')}}/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- SlimScroll -->
    <script src="{{asset('admin')}}/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- FastClick -->
    <script src="{{asset('admin')}}/assets/vendor_components/fastclick/lib/fastclick.js"></script>

    @stack('scripts')
</html>