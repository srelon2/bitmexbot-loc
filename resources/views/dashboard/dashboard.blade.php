@extends('dashboard.layouts.layout')

@section('content')
    <div class="container mg-t-20">
        <div class="box-body card">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Название</th>
                        <th>Пара</th>
                        <th>Ликцидация</th>
                        <th>Цена ордара</th>
                        <th>Позиция</th>
                        <th>Профит</th>
                        <th class="text-center">Active</th>
                        <th>@if(count($apiKeys))<a href="{{route('dashboard.bot.info')}}" class="btn btn-info btn-sm pull-right"><i class="fa fa-plus mg-r-5"></i>Создать настройки</a> @else <a href="{{route('dashboard.settings')}}" class="btn btn-info btn-sm pull-right"><i class="fa fa-plus mg-r-5"></i>Создать Api key</a> @endif</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div><!-- table-wrapper -->
        </div>
    </div>
@endsection

@push('scripts')

    <!-- Dropzone Plugin JavaScript -->
    <script src="{{asset('admin')}}/assets/vendor_components/tiny-editable/mindmup-editabletable.js"></script>
    <script src="{{asset('admin')}}/assets/vendor_components/tiny-editable/numeric-input-example.js"></script>

    <script type="text/javascript">
        $(function(){
            var dataTable= $('#datatable1').DataTable({
                processing: true,
                serverSide: true,
                serverMethod: 'post',
                bPaginate:true,
                responsive: false,
                order: [[0, 'desc']],
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }],
                "ajax": {
                    "url": "{{route('dashboard.bot.search')}}",
                },
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
        });
    </script>
@endpush