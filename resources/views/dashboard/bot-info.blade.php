@extends('dashboard.layouts.layout')

@section('content')
    @include('dashboard.bot-info-form')
@endsection

@push('scripts')

<!-- Dropzone Plugin JavaScript -->
<script src="{{asset('admin')}}/assets/vendor_components/tiny-editable/mindmup-editabletable.js"></script>
<script src="{{asset('admin')}}/assets/vendor_components/tiny-editable/numeric-input-example.js"></script>

<script type="text/javascript">
    function update() {
        @if(isset($item))
            $(function(){

//            // Setup - add a text input to each footer cell
//            $('#datatable1 tfoot th').each( function () {
//                var title = $(this).text();
//                $(this).html( '<input type="text" placeholder="'+title+'" />' );
//            } );

            var dataTable= $('#datatable1').DataTable({
                processing: true,
                serverSide: true,
                serverMethod: 'post',
                bPaginate:true,
                "autoWidth": false,
                responsive: false,
                order: [[0, 'desc']],
                "ajax": {
                    "url": "{{route('dashboard.bot.signals.search', ['id'=>$item->id])}}",
                },
            });
            var dataTable= $('#datatable2').DataTable({
                processing: true,
                serverSide: true,
                serverMethod: 'post',
                bPaginate:true,
                "autoWidth": false,
                responsive: false,
                order: [[0, 'desc']],
                "ajax": {
                    "url": "{{route('dashboard.bot.exections.search', ['id'=>$item->id])}}",
                },
            });

//            // Apply the search
//            dataTable.columns().every( function () {
//                var that = this;
//
//                $( 'input', this.footer() ).on( 'keyup change', function () {
//                    if ( that.search() !== this.value ) {
//                        that
//                            .search( this.value )
//                            .draw();
//                    }
//                } );
//            } );

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
        });
        @endif
//        $('.btn-avg').on('click', function () {
//            var sum_point= 0;
//            var quantity= false;
//            $('td[data-input="points"]').each(function () {
//                var $parent= $(this).parents('tr');
//
//                $point= Number($(this).text());
//
//                old_quantity= quantity;
//                quantity= Number($parent.find('td[data-input="quantity"]').text());
//                if(old_quantity) {
//                    avg= (quantity+old_quantity)/2;
//                }
//
//                sum_point+= $point;
//            });
//        });
        function totalTable() {
            points= 0;
            $('td[data-input="points"]').each(function () {
                points+= Number($(this).text());
            });

            $('.summ_points').text(points);

            var avg_amount= 0;
            var amount= Number($('#amount').val());
            var avg_sum= amount;
            var sum_quantity= 0;
            $('td[data-input="amount"]').each(function ($key) {
                var $parent= $(this).parents('tr');
                if($key==0) {
                    amountVal= $('#amount').val();
                } else {
                    amountVal= $('td[data-input="amount"]')[$key-1].innerHTML;
                }
                quantity= Number($parent.find('td[data-input="points"]').text());
                multiplier= Number($parent.find('td[data-input="multiplier"]').text());
                if(multiplier) {
                    $(this).text((amountVal*multiplier).toFixed(2));
                } else {
                    $(this).text(amountVal);
                }
                if($key==0) {

                    $parent.find(('[data-input="avg"]')).text(1);
                    $parent.find(('[data-input="quantity"]')).text(1);
                } else {
                    avg_amount= Number($(this).text());
                    amount+= avg_amount;
                    sum_quantity+= quantity;
                    avg_sum+= sum_quantity*avg_amount;

                    $parent.find(('[data-input="avg"]')).text((avg_sum/amount).toFixed(2));
                    $parent.find(('[data-input="quantity"]')).text((sum_quantity).toFixed(2));
                }
            });

            $('.summ_amount').text(amount);
        }
        totalTable();
        @if(isset($item) && $item->active)
            $('.bots .form-control').attr('readonly', true);
//            $('.bots select.form-control').attr('disabled', true);
        @else
            var step= JSON.parse($('#values_step').val());
            function updateFunction() {
                $('#steps-table').editableTableWidget({
                    editor: $('<input type="number" step="0.01">'),
                });
                $('table .edit').on('change', function () {
                    var parent= $(this).parents('tr');
                    var id = $(this).parents('tr').data('id');
                    var input = $(this).data('input');
//                    if(input=='multiplier') {
//                        amount= $('#amount').val()*$(this).text();
//                        parent.find('td[data-input="amount"]').text(amount);
//                    }
//                    else if(input== 'avg') {
//                        var quantity= 0;
//                        var sum_quantity= 0;
//                        var price= 0;
//                        var sum_price= 0;
//                        var avg_sum= 0;
//                        $('td[data-input="avg"]').each(function () {
//                            var $parent= $(this).parents('tr');
//
//                            quantity= Number($parent.find('td[data-input="points"]').text());
//                            sum_price= Number($(this).text());
//                            price+= sum_price;
//                            sum_quantity+= quantity;
//
//                            avg_sum+= sum_quantity/sum_price;
//                            console.log(avg_sum*price);
//                            $parent.find(('[data-input="multiplier"]')).text((avg_sum*price).toFixed(2));
//                        });
//                    }

                    step[id][input] = $(this).text();
                    $('#values_step').val(JSON.stringify(step));

                    totalTable();
                });
                $('.btn-trash').on('click', function () {
                    $parent= $(this).parents('tr');

                    step.splice( $.inArray($parent.data('id'),step) ,1 );
                    $('#values_step').val(JSON.stringify(step));
                    $parent.remove();
                });
            }
            $(function(){
                $('#amount').bind('input', function () {
                    summ= $(this).val();

                    $('.table-striped tbody tr').each(function (i) {
                        $(this).attr('data-id', i);
                        $('.table-steps tr').each(function () {
                            multiplier= $(this).find('td[data-input="multiplier"]').text();
                            if(multiplier) {
                                amount= summ* multiplier;
                            } else {
                                amount= summ;
                            }

                            $(this).find('td[data-input="amount"]').text(amount);
                        });
                    });

                    totalTable();
                });

                updateFunction();
                $('.btn-add-steps').on('click', function () {
                    length= $('#steps-table tbody tr').length;
                    amountVal= $('td[data-input="amount"]')[length-1].innerHTML;
                    $('#steps-table tbody').append("<tr data-id='"+length+"'><td>Шаг </td><td contentEditable='true' class='edit' data-input='points'></td><td contentEditable='true' class='edit'  data-input='multiplier'>1.5</td><td data-input='amount'>"+(1.5*amountVal).toFixed(2)+"</td><td data-input='avg'></td><td data-input='quantity'></td><td class='tx-right'><a href='#' class='btn btn-danger btn-sm btn-trash'><i class='fa fa-trash'></i></a></td></tr>");
                    step[length]= {
                        'points': 2, 'multiplier': 1.5
                    };
                    $('#values_step').val(JSON.stringify(step));
                    updateFunction();
                    totalTable();
                });
                $('.pattern-load').on('click', function () {
                    $parents= $(this).parents('.input-group');
                    $value= $parents.find('.form-control').val();
                    ajax($value, "{{route('dashboard.bot.pattern', ['id'=> (isset($id) ? $id : false)])}}");
                })
            });


            function  ajax(value, url) {
                $data= $('.data');
                $.ajax({
                    type:'POST',
                    url: url,
                    data: {
                        'data': value,
                    },
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(data){
        //                    var msgBlock= $('.msg-block .alert-'+data['status']);
        //                    msgBlock.removeClass('d-none').find('.align-items-center span').text(data['msg']);
//                        console.log(data['content']);
                        $('.content').html(data['content']);
                        update();
                        if(data['status']=='success') {
                            $.toast({
                                heading: 'Alert. Success!',
                                text: data['msg'],
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'success',
                                hideAfter: 3500,
                                stack: 6
                            });
                        } else {
                            $.toast({
                                heading: 'Alert. Error!',
                                text: data['msg'],
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'error',
                                hideAfter: 3500
                            });
                        }
                    },
                    error: function(msg){
                        console.log(msg);
                    }
                });
            }
        @endif
    }
    update();
</script>
@endpush