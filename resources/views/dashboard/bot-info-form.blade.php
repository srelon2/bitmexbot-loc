<div class="container mg-t-20">
    <form method="post" action="{{route('dashboard.bot.update', ['id'=>(isset($id) ? $id : '')])}}">
        @csrf
        <div class="row">
            <div class="col-md-5">
                <div class="box">
                    <div class="box-body">
                        <div class="bots row">
                            @if(isset($patterns))
                                <div class="input-group form-group col-md-12">
                                    <!-- /btn-group -->
                                    <select class="form-control">
                                        @foreach($patterns as $pattern)
                                            <option value="{{$pattern->id}}">{{(isset($pattern->name)) ? $pattern->name : $pattern->currency_pair}}</option>
                                        @endforeach
                                    </select>
                                    <div class="input-group-prepend">
                                        <button type="button" class="btn btn-info btn-sm pattern-load">загрузить шаблон</button>
                                    </div>
                                </div>
                            @endif
                            @if(count($apiKeys))
                                <div class="form-group col-md-12 {{ $errors->has('api_id') ? ' error' : '' }}">
                                    <!-- /btn-group -->
                                    <label>Выберете Api ключ</label>
                                    <select class="form-control" name="api_id">
                                        @foreach($apiKeys as $api)
                                            <option value="{{$api->id}}" @if(isset($item) && $item->api_id== $api->id) selected @endif>{{$api->key}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('api_id'))
                                    <span class="help-block">
                                    <ul class="alert"><li>{{ $errors->first('api_id') }}</li></ul>
                                </span>
                                @endif
                            @else

                                <div class="form-group col-md-12">
                                    <!-- /btn-group -->
                                    <p>Необходимо добавить Api ключ</p>
                                    <a href="{{route('dashboard.settings')}}" target="_blank" class="btn btn-primary btn-block">Добавить Api ключ</a>
                                </div>
                            @endif

                            <div class="form-group {{ $errors->has('currency_pair') ? ' error' : '' }} col-md-6">
                                <label for="link">Валютная пара</label>
                                @php
                                    $currencies= ['XBTUSD','XBT7D_U105', 'XBTJPY', 'ADAU19', 'BCHU19', 'EOSU19', 'ETHUSD', 'LTCU19', 'TRXU19', 'XRPU19', 'XBTKRW'];
                                @endphp
                                <select class="form-control" name="currency_pair">
                                    @foreach($currencies as $currency)
                                        <option value="{{$currency}}" @if(isset($item) && $item->currency_pair== $currency) selected="" @endif>{{$currency}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('currency_pair'))
                                    <span class="help-block">
                                        <ul class="alert"><li>{{ $errors->first('currency_pair') }}</li></ul>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('amount') ? ' error' : '' }} col-md-6">
                                <label for="amount">Количество</label>
                                <input type="number" step="any" name="amount" id="amount" class="form-control" placeholder="Enter amount" value="{{(isset($item) ? $item->amount : old('amount') )}}">
                                @if ($errors->has('amount'))
                                    <span class="help-block">
                                            <ul class="alert"><li>{{ $errors->first('amount') }}</li></ul>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('take_profit') ? ' error' : '' }} col-md-6">
                                <label for="take_profit">Take Profit</label>
                                <input type="number" step="any" name="take_profit" id="take_profit" class="form-control" placeholder="Enter title" value="{{(isset($item) ? $item->take_profit : old('take_profit') )}}">
                                @if ($errors->has('take_profit'))
                                    <span class="help-block">
                                            <ul class="alert"><li>{{ $errors->first('take_profit') }}</li></ul>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('stop_loss') ? ' error' : '' }} col-md-6">
                                <label for="stop_loss">Stop Loss</label>
                                <input type="number" step="any" name="stop_loss" id="stop_loss" class="form-control" placeholder="Enter title" value="{{(isset($item) ? $item->stop_loss : old('stop_loss') )}}" >
                                @if ($errors->has('stop_loss'))
                                    <span class="help-block">
                                            <ul class="alert"><li>{{ $errors->first('stop_loss') }}</li></ul>
                                        </span>
                                @endif
                            </div>
                            {{--<div class="form-group {{ $errors->has('stop_loss') ? ' error' : '' }} col-md-6">--}}
                            {{--<label for="points">1 пункт</label>--}}
                            {{--@php--}}
                            {{--$points= ['1','0.00001', '0.000001', '0.0000001', '0.00000001', '1000', '100'];--}}
                            {{--@endphp--}}
                            {{--<select class="form-control" name="points">--}}
                            {{--@foreach($points as $point)--}}
                            {{--<option value="{{$point}}" @if(isset($item) && $item->points== $point) selected="" @endif>{{$point}}</option>--}}
                            {{--@endforeach--}}
                            {{--</select>--}}
                            {{--@if ($errors->has('points'))--}}
                            {{--<span class="help-block">--}}
                            {{--<ul class="alert"><li>{{ $errors->first('points') }}</li></ul>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--</div>--}}
                            <div class="form-group {{ $errors->has('stop_loss') ? ' error' : '' }} col-md-6">
                                <label for="position">Позиция</label>
                                @php
                                    $positions= ['All','Long', 'Short'];
                                @endphp
                                <select class="form-control" name="position">
                                    @foreach($positions as $position)
                                        <option value="{{$position}}" @if(isset($item) && $item->position== $position) selected="" @endif>{{$position}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('points'))
                                    <span class="help-block">
                                <ul class="alert"><li>{{ $errors->first('points') }}</li></ul>
                            </span>
                                @endif
                            </div>
                        </div>
                        <p>Настройки @if(isset($item) && $item->active)<button type="submit" value="update" name="submit" class="btn pull-right btn-success btn-sm">Сохранить</button>@endif</p>
                        <div class="row">
                            <div class="form-group {{ $errors->has('name') ? ' error' : '' }} col-md-12">
                                <!-- /btn-group -->
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Enter name" value="{{(isset($item) ? $item->name : old('name') )}}">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                    <ul class="alert"><li>{{ $errors->first('name') }}</li></ul>
                                </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('closed') ? ' error' : '' }} col-md-6">
                                <label for="closed">Следующая сетка ордеров</label>
                                @php
                                    $closed= ['Repeat','Closed', 'Closed SL'];
                                @endphp
                                <select class="form-control" name="closed">
                                    @foreach($closed as $key=> $close)
                                        <option value="{{$key}}" @if(isset($item) && $item->closed== $key) selected="" @endif>{{$close}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('closed'))
                                    <span class="help-block">
                                        <ul class="alert"><li>{{ $errors->first('closed') }}</li></ul>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="timer">Запустить после (мин)</label>
                                <input type="number" step="1" min="0" name="timer" id="timer" class="form-control" placeholder="Количество минут" value="{{(isset($item) ? $item->timer : old('timer') )}}">
                                @if ($errors->has('timer'))
                                    <span class="help-block">
                                        <ul class="alert"><li>{{ $errors->first('timer') }}</li></ul>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-md-12  {{ $errors->has('check_position') ? ' error' : '' }} ">
                                <label for="timer">Действие если осталась позиция</label>
                                @php
                                    $checkPosition= ['Выставить новый TP и SL','Продать по рынку', 'Торговать с открытой позицией'];
                                @endphp
                                <select class="form-control" name="check_position">
                                    @foreach($checkPosition as $key=> $check)
                                        <option value="{{$key}}" @if(isset($item) && $item->check_position== $key) selected="" @endif>{{$check}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('check_position'))
                                    <span class="help-block">
                                        <ul class="alert"><li>{{ $errors->first('check_position') }}</li></ul>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <input type="hidden" name="steps" id="values_step" value='@if(isset($item)) {{$item->steps}} @else [ {"points": "2.0","multiplier": ""}] @endif' autocomplete="off">
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-default">Cancel</button>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-outline btn-primary" name="submit" value="saved">Сохранить как шаблон</button>
                            @if(isset($item) && $item->active)
                                <button type="submit" class="btn btn-danger" name="submit" value="disabled">Остановить</button>
                            @else
                                <button type="submit" class="btn btn-success" name="submit" value="active">Запустить</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="steps-table">
                                <thead>
                                @if(isset($item))
                                    @php
                                        $steps= json_decode($item->steps);
                                    @endphp
                                @endif
                                <tr>
                                    <th class="wd-100"></th>
                                    <th class="wd-80">Пунктров</th>
                                    <th>Умножитель </th> {{--<a href="#" class="btn btn-xs btn-info mg-l-5 btn-avg">Усреднить</a>--}}
                                    <th>Кредитов</th>
                                    <th>Ср. цена</th>
                                    <th>Цена</th>
                                    <th class="tx-right"><a href="#" class="btn btn-sm btn-primary btn-add-steps">Добавить шаг</a></th>
                                </tr>
                                </thead>
                                <tbody class="table-steps">
                                <tr data-id="0">
                                    <td>Шаг </td>
                                    <td contentEditable='true' class='edit' data-input="points">{{(isset($item)) ? reset($steps)->points : '2.0'}}</td>
                                    <td  data-input="multiplier"></td>
                                    <td data-input="amount">{{(isset($item)) ? $item->amount : 0}}</td>
                                    <td data-input="avg"></td>
                                    <td data-input="quantity"></td>
                                    <td class="tx-right"></td>
                                </tr>
                                @if(isset($item))
                                    @foreach($steps as $key=> $step)
                                        @if($key>0)
                                            <tr data-id="{{$key}}">
                                                <td>Шаг </td>
                                                <td contentEditable='true' class='edit' data-input="points" class="td-points">{{$step->points}}</td>
                                                <td contentEditable='true' class='edit'  data-input='multiplier'>{{$step->multiplier}}</td>
                                                <td data-input="amount">{{$step->multiplier*$item->amount}}</td>
                                                <td data-input="avg"></td>
                                                <td data-input="quantity"></td>
                                                <td class="tx-right"><a href='#' class='btn btn-danger btn-sm btn-trash'><i class='fa fa-trash'></i></a></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>Итого</td>
                                        <td class="summ_points">0</td>
                                        <td></td>
                                        <td class="summ_amount">0</td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    @if(isset($item))
        <div class="box">
            <div class="box-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#orders" role="tab"><span class="hidden-sm-up"><i class="ion-home"></i></span> <span class="hidden-xs-down">Orders</span></a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#balances" role="tab"><span class="hidden-sm-up"><i class="ion-person"></i></span> <span class="hidden-xs-down">Изменения баланса</span></a> </li>
                    @if($exections)
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#statistics" role="tab"><span class="hidden-sm-up"><i class="ion-person"></i></span> <span class="hidden-xs-down">Статистика</span></a> </li>
                    @endif
                </ul>
                <div class="tab-content mg-t-15">
                    <div id="orders" class="tab-pane active" role="tabpanel">
                        <div class="table-responsive">
                            <table id="datatable1" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Count</th>
                                    <th>Order ID</th>
                                    <th>Order Qty</th>
                                    <th>Cum Qty</th>
                                    <th>Price</th>
                                    <th>Side</th>
                                    <th>Order type</th>
                                    <th class="text-right">Status</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                                {{--<tfoot>--}}
                                {{--<tr>--}}
                                {{--<th>ID</th>--}}
                                {{--<th>Count</th>--}}
                                {{--<th>Order ID</th>--}}
                                {{--<th>Order Qty</th>--}}
                                {{--<th>Cum Qty</th>--}}
                                {{--<th>Price</th>--}}
                                {{--<th>Side</th>--}}
                                {{--<th>Order type</th>--}}
                                {{--<th>Status</th>--}}
                                {{--</tr>--}}
                                {{--</tfoot>--}}
                            </table>
                        </div><!-- table-wrapper -->
                    </div>
                    <div id="balances" class="tab-pane" role="tabpanel">
                        <table id="datatable2" class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Count</th>
                                <th>Courency pair</th>
                                <th>Order type</th>
                                <th>Price</th>
                                <th>Avg price</th>
                                <th>Order qty</th>
                                <th>Current qty</th>
                                <th>Side</th>
                                <th>Commission</th>
                                <th>Exec Comm</th>
                                <th>Order Status</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                            {{--<tfoot>--}}
                            {{--<tr>--}}
                            {{--<th>ID</th>--}}
                            {{--<th>Count</th>--}}
                            {{--<th>Order ID</th>--}}
                            {{--<th>Order Qty</th>--}}
                            {{--<th>Cum Qty</th>--}}
                            {{--<th>Price</th>--}}
                            {{--<th>Side</th>--}}
                            {{--<th>Order type</th>--}}
                            {{--<th>Status</th>--}}
                            {{--</tr>--}}
                            {{--</tfoot>--}}
                        </table>
                    </div><!-- table-wrapper -->
                    @if($exections)
                        <div id="statistics" class="tab-pane" role="tabpanel">
                        <div class="box col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading clearfix pd-10">
                                    Статистика настроек
                                    {{--<div class="btn-group-xs btn-group pull-right statistics-date">--}}
                                        {{--<span class="btn btn-default " data-filter="all">All</span>--}}
                                        {{--<span class="btn btn-default " data-filter="day">Day</span>--}}
                                        {{--<span class="btn btn-default " data-filter="week">Week</span>--}}
                                        {{--<span class="btn btn-default " data-filter="month">Month</span>--}}
                                        {{--<span class="btn btn-default " data-filter="year">Year</span>--}}
                                    {{--</div>--}}
                                </div>
                                <div class="panel-body mg-t-10">
                                    <div class="row statistics">
                                        @php
                                            $profit= $func::printNum($exections->where('profit', '>', 0)->sum('profit'));
                                            $loss= $func::printNum($exections->where('profit', '<=', 0)->sum('profit'));

                                            $profit_buy= $func::printNum($exections->where('type', 'Buy')->sum('profit'));
                                            $profit_sell= $func::printNum($exections->where('type', 'Sell')->sum('profit'));
                                            $sum_profit= $func::printNum($exections->sum('profit'));

                                        @endphp
                                        <div class="col-sm-4">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <tbody>
                                                        <tr>
                                                            <td>Всего созданных сеток:</td>
                                                            <td class="pull-right">{{$exections->count()}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Profit Buy:</td>
                                                            <td class="pull-right">
                                                                @if($profit_buy > 0)
                                                                    <span class="text-success"><i class="fa fa-long-arrow-up"></i> {{$profit_buy}}</span>
                                                                @else
                                                                    <span class="text-success"><i class="fa fa-long-arrow-down"></i> {{$profit_buy}}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Profit Sell:</td>
                                                            <td class="pull-right">
                                                                @if($profit_sell > 0)
                                                                    <span class="text-success"><i class="fa fa-long-arrow-up"></i> {{$profit_sell}}</span>
                                                                @else
                                                                    <span class="text-success"><i class="fa fa-long-arrow-down"></i> {{$profit_sell}}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <tbody>
                                                    <tr>
                                                        <td>Take Profit:</td>
                                                        <td class="pull-right">
                                                            <span class="text-success"><i class="fa fa-long-arrow-up"></i> {{$profit}}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Stop Loss:</td>
                                                        <td class="pull-right">
                                                            <span class="text-danger"><i class="fa fa-long-arrow-down"></i> {{$loss}}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Заработано:</td>
                                                        <td class="pull-right">
                                                            @if($sum_profit > 0)
                                                                <span class="text-success"><i class="fa fa-long-arrow-up"></i> {{$sum_profit}}</span>
                                                            @else
                                                                <span class="text-success"><i class="fa fa-long-arrow-down"></i> {{$sum_profit}}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <tbody>
                                                        @foreach($remainder as $key=>$item)
                                                            <tr>
                                                                <td>Осталось шагов: {{$key}}</td>
                                                                <td class="pull-right">
                                                                    <span class="text-default">{{$item->count()}}</span>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- table-wrapper -->
                    @endif
                </div>
            </div>
        </div>
    @endif
</div>