@extends('dashboard.layouts.layout')

@section('content')
    <div class="container mg-t-20">
        <div class="row">
            <div class="col-md-3">
                @if(empty($user->telegram))
                    <form method="post" action="{{route('dashboard.settings.telegram.update')}}">
                        @csrf
                @endif
                    <div class="box">
                        <div class="box-header">
                            <h4 class="mg-b-0">Connect telegram</h4>
                        </div>
                        <div class="box-body">
                            <div class="form-group form-group-sm {{ $errors->has('chat_id') ? ' error' : '' }}">
                                <label for="chat_id">Chat ID</label>
                                    <input type="text" name="chat_id" id="chat_id" @if(isset($user->telegram)) readonly @endif class="form-control" placeholder="Enter Chat ID" value="{{($user->telegram) ? $user->telegram->chat_id : old('chat_id')}}">
                                @if ($errors->has('chat_id'))
                                    <span class="help-block">
                                        <ul class="alert"><li>{{ $errors->first('chat_id') }}</li></ul>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="box-footer">
                            <a target="_blank" href="https://t.me/BitoxbitBot" class="btn btn-info btn-sm">Telegram Bot</a>
                            @if(isset($user->telegram))
                                <a href="{{route('dashboard.settings.telegram.destroy', ['id'=>$user->telegram->id])}}" class="btn btn-danger pull-right btn-sm"><i class="fa fa-trash mg-r-10"></i>Удалить</a>
                            @else
                                <button type="submit" class="btn btn-success pull-right btn-sm">Подключить</button>
                            @endif
                        </div>
                    </div>
                @if(empty($user->telegram))
                    </form>
                @endif
            </div>
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header">
                        <h4 class="mg-b-0">Your Bitmex Api key</h4>
                    </div>
                    <div class="box-body">
                        <form method="post" action="{{route('dashboard.settings.keys.create')}}">
                            @csrf
                            <div class="row">
                                <div class="form-group form-group-sm {{ $errors->has('key') ? ' error' : '' }} col-md-5">
                                    <label for="link">Api key</label>
                                    <input type="text" name="key" id="key" class="form-control" placeholder="Enter Api key" value="{{old('key') }}">
                                    @if ($errors->has('key'))
                                        <span class="help-block">
                                        <ul class="alert"><li>{{ $errors->first('key') }}</li></ul>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group form-group-sm {{ $errors->has('key') ? ' error' : '' }} col-md-5">
                                    <label for="link">Api secret</label>
                                    <input type="text" name="secret" id="secret" class="form-control" placeholder="Enter Api secret" value="{{old('secret')}}">
                                    @if ($errors->has('secret'))
                                        <span class="help-block">
                                        <ul class="alert"><li>{{ $errors->first('secret') }}</li></ul>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('key') ? ' error' : '' }} col-md-2 flex flex-end">
                                    <button class="btn btn-sm btn-primary btn-block" type="submit"><i class="fa fa-plus mg-r-10"></i>Добавить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="box-footer">
                        <div class="box-body card">
                            <div class="table-responsive">
                                <table id="datatable1" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Key</th>
                                        <th>Secret</th>
                                        <th class="text-center">Active</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div><!-- table-wrapper -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

<!-- Dropzone Plugin JavaScript -->
<script src="{{asset('admin')}}/assets/vendor_components/tiny-editable/mindmup-editabletable.js"></script>
<script src="{{asset('admin')}}/assets/vendor_components/tiny-editable/numeric-input-example.js"></script>
<script type="text/javascript">
    $(function(){
        var dataTable= $('#datatable1').DataTable({
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            bPaginate:true,
            responsive: false,
            order: [[0, 'desc']],
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            "ajax": {
                    "url": "{{route('dashboard.settings.keys.search')}}",
            },
            "drawCallback": function () {
                //Warning Message
                $('.sa-warning').click(function(){
                    event= $(this);
                    swal({
                        title: "Вы уверены?",
                        text: $(event).data('mess'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Да",
                        closeOnConfirm: false
                    }, function(){
                        $.ajax({
                            type:'get',
                            url: "{{route('dashboard.settings.keys.action')}}",
                            data: {
                                'data': $(event).data('id'),
                                'action': $(event).data('action'),
                                'ajax': true,
                            },
                            headers: {
                                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                            },
                            success:function(data){
                                swal("Success!", data['msg'], data['status']);
                                dataTable.draw()
                                $('[data-toggle="tooltip"]').tooltip('dispose');
                            },
                            error: function(msg){
                                console.log(msg);
                                swal("Error!", 'Не удалось совершить действие.', 'error');
                            }
                        });


                    });
                });
            }
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    });
</script>
@endpush