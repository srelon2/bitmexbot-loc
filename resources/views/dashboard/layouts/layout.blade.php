<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="{{asset('admin')}}/images/favicon.ico">

        <title>{{(isset($pagetitle)) ? $pagetitle : 'Title'}}</title>

        <!-- Bootstrap 4.0-->
        <link rel="stylesheet" href="{{asset('admin')}}/assets/vendor_components/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" href="{{asset('admin')}}/assets/vendor_components/bootstrap-switch/switch.css">

        <!-- xeditable css -->
        <link href="{{asset('admin')}}/assets/vendor_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />


        <!-- Data Table-->
        <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/assets/vendor_components/datatable/datatables.min.css"/>
        <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/assets/vendor_components/select2/dist/css/select2.min.css"/>

        <!-- Bootstrap extend-->
        <link rel="stylesheet" href="{{asset('admin')}}/main/css/bootstrap-extend.css">

        <link rel="stylesheet" href="{{asset('admin')}}/main/css/master_style.css">
        <!--alerts CSS -->
        <link href="{{asset('admin')}}/assets/vendor_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">

        <!-- toast CSS -->
        <link href="{{asset('admin')}}/assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.css" rel="stylesheet">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('admin')}}/main/css/master_style.css">

        <link rel="stylesheet" href="{{asset('admin')}}/css/helpers.css">
        <link rel="stylesheet" href="{{asset('/')}}css/styles.css?1.1">
    </head>

    <body>
        <div class="wrapper bg-lightest">
            <div class="tx-center">
                <div class="mg-t-15">
                    @if(Auth::user())
                        <a class="btn btn-primary" href="{{ route('dashboard.home') }}">Dashboard</a>
                        <a class="btn btn-info" href="{{ route('dashboard.settings') }}">Настройки</a>
                        <a class="btn btn-danger btn-outline" href="{{ route('logout') }}">Logout</a>
                    @else
                        <a class="btn btn-success" href="{{ route('login') }}">Login</a>
                        <a class="btn btn-primary btn-outline" href="{{ route('register') }}">Register</a>
                    @endif
                </div>
            </div>
            <section class="content">
                @yield('content')
            </section>
        </div>
    </body>

    <!-- jQuery 3 -->
    <script src="{{asset('admin')}}/assets/vendor_components/jquery/dist/jquery.js"></script>

    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('admin')}}/assets/vendor_components/jquery-ui/jquery-ui.js"></script>

    <!-- Bootstrap 4.0-->
    <script src="{{asset('admin')}}/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- SlimScroll -->
    <script src="{{asset('admin')}}/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- FastClick -->
    <script src="{{asset('admin')}}/assets/vendor_components/fastclick/lib/fastclick.js"></script>

    <!-- Sweet-Alert  -->
    <script src="{{asset('admin')}}/assets/vendor_components/sweetalert/sweetalert.min.js"></script>
    <script src="{{asset('admin')}}/assets/vendor_components/sweetalert/jquery.sweet-alert.custom.js"></script>

    <!-- toast -->
    <script src="{{asset('admin')}}/assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.js"></script>

    <!-- select2 -->
    <script src="{{asset('admin')}}/assets/vendor_components/select2/dist/js/select2.js"></script>

    <!-- This is data table -->
    <script src="{{asset('admin')}}/assets/vendor_components/datatable/datatables.js"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
        @if(session('message'))
$.toast({
            heading: 'Alert. Info!',
            text: "{!! session('message') !!}.",
            position: 'top-right',
            loaderBg: '#1e88e5',
            icon: 'info',
            hideAfter: 3000,
            stack: 6
        });
        console.log("{!! session('message') !!}.");
        @endif
        @if(session('warning'))
$.toast({
            heading: 'Alert. Warning!',
            text: "{!! session('warning') !!}.",
            position: 'top-right',
            loaderBg: '#ffb22b',
            icon: 'warning',
            hideAfter: 3500,
            stack: 6
        });
        console.log("{!! session('warning') !!}.");
        @endif
        @if(session('success'))
$.toast({
            heading: 'Alert. Success!',
            text: "{!! session('success') !!}.",
            position: 'top-right',
            loaderBg: '#26c6da',
            icon: 'success',
            hideAfter: 3500,
            stack: 6
        });
        console.log("{!! session('success') !!}.");
        @endif
        @if(session('error'))
$.toast({
            heading: 'Alert. Error!',
            text: "{!! session('error') !!}.",
            position: 'top-right',
            loaderBg: '#ff6849',
            icon: 'error',
            hideAfter: 3500

        });
        console.log("{!! session('error') !!}.");
        @endif

$(document).ready(function () {
            $('.btn-copy').on('click', function () {
                var input= $(this).parents('.input-group').find('.form-control');
                console.log(input);
                input.select();
                document.execCommand('copy');
                input.append(' ');
                input.val().slice(0, -1);
                $.toast({
                    text: "Скопировано.",
                    position: 'top-right',
                    loaderBg: '#26c6da',
                    icon: 'success',
                    hideAfter: 750,
                    stack: 6
                });
            });
        });
    </script>

    @stack('scripts')
</html>