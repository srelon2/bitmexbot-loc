<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\Traits\GlobalTraits;
use App\Models\Admin\Crons;

class EveryMinute extends Command
{
    use GlobalTraits;

    protected $signature = 'everyMinute';
    protected $description = 'Command description';
    public function __construct()
    {
        parent::__construct();
    }
    public function handle(){
        $data = [
            Array('url'=>'http://'.env('APP_NAME').'/cron/check-signals/tkvkofaiyulgfzreesuntlbeoibmtp/checkStart'),
            Array('url'=>'http://'.env('APP_NAME').'/cron/check-signals/tkvkofaiyulgfzreesuntlbeoibmtp/setGrid'),
            Array('url'=>'http://'.env('APP_NAME').'/cron/check-signals/tkvkofaiyulgfzreesuntlbeoibmtp/startSocket'),
            Array('url'=>'http://'.env('APP_NAME').'/cron/check-signals/tkvkofaiyulgfzreesuntlbeoibmtp/startTechBot'),
            Array('url'=>'http://'.env('APP_NAME').'/cron/check-signals/tkvkofaiyulgfzreesuntlbeoibmtp/check'),
        ];

        $this->useCurl($data);

        sleep(20);
        $data = [
            Array('url'=>'http://'.env('APP_NAME').'/cron/check-signals/tkvkofaiyulgfzreesuntlbeoibmtp/setGrid'),
            Array('url'=>'http://'.env('APP_NAME').'/cron/check-signals/tkvkofaiyulgfzreesuntlbeoibmtp/checkStart'),
        ];
        $this->useCurl($data);

        sleep(20);
        $this->useCurl($data);
    }

}
