<?php

//namespace App\Http\Controllers\Cron;
namespace App\Console\Commands;
use App\Http\Controllers\Cron\EventsCoinsController;
use Illuminate\Console\Command;
use App\Classes\GlobalFunctions;
use Amp\Promise;
use function Amp\ParallelFunctions\parallelMap;
use App\Models\Admin\Crons;

class EveryHour extends Command
{
    protected $signature = 'everyHour';
    protected $description = 'Command description';
    public function __construct()
    {
        parent::__construct();
    }
    public function handle(){
//        GlobalFunctions::SendBotMsg(urlencode("everyHour  ".time()));

//        $data = [
//            Array('url'=>'http://doji.club/34jgv5jhg24v5jv23v5j23v5v23v5k/bittrex/market-currency'),
//            Array('url'=>'http://doji.club/34jgv5jhg24v5jv23v5j23v5v23v5k/payment_channel'),
//            Array('url'=>'http://doji.club/34jgv5jhg24v5jv23v5j23v5v23v5k/payment_robot'),
//            Array('url'=>'http://doji.club/34jgv5jhg24v5jv23v5j23v5v23v5k/events')
//        ];
        $items= Crons::where([
            ['deleted','=',0],
            ['timeframe', '=', '1h']
        ])->get();
        $data= [];
        foreach ($items as $item) {
            $data[]= array('url'=> $item->url);
        }

        if($data){
            $curls = array();
            $mh = curl_multi_init();
            foreach ($data as $id => $d) {

                $curls[$id] = curl_init();
                $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;

                curl_setopt($curls[$id], CURLOPT_URL,$url);
                curl_multi_add_handle($mh, $curls[$id]);

            }
            $running = null;
            do { curl_multi_exec($mh, $running);} while($running > 0);
            foreach($curls as $id => $c)
            {
                curl_multi_remove_handle($mh, $c);
            }
            curl_multi_close($mh);

        }
    }

}
