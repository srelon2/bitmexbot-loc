<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\Classes\GlobalFunctions;
use App\Models\Admin\Crons;

class EveryWeek extends Command
{
    protected $signature = 'everyWeek';
    protected $description = 'Command description';
    public function __construct()
    {
        parent::__construct();
    }
    public function handle(){
//        GlobalFunctions::SendBotMsg(urlencode("EveryWeek  ".time()));

//        $data = [
//            Array('url'=>'http://doji.club/34jgv5jhg24v5jv23v5j23v5v23v5k/events-coins')
//        ];
        $items= Crons::where([
            ['deleted','=',0],
            ['timeframe', '=', '1w']
        ])->get();
        $data= [];
        foreach ($items as $item) {
            $data[]= array('url'=> $item->url);
        }

        if($data){
            $curls = array();
            $mh = curl_multi_init();
            foreach ($data as $id => $d) {

                $curls[$id] = curl_init();
                $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;

                curl_setopt($curls[$id], CURLOPT_URL,$url);
                curl_multi_add_handle($mh, $curls[$id]);

            }
            $running = null;
            do { curl_multi_exec($mh, $running);} while($running > 0);
            foreach($curls as $id => $c)
            {
                curl_multi_remove_handle($mh, $c);
            }
            curl_multi_close($mh);

        }
    }

}
