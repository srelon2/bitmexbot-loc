<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class BotExections extends Model
{

    use SoftDeletes;
    protected $table = 'bot_exections';
    protected $dates = ['deleted_at'];

    public function getApi(){
        return $this->belongsTo('App\Models\ApiKeys', 'api_id');
    }
    public function bot(){
        return $this->belongsTo('App\Models\BotSettings', 'bot_id');
    }
}
