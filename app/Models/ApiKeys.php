<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ApiKeys extends Model
{

    use SoftDeletes;
    protected $table = 'api_keys';
    protected $dates = ['deleted_at'];

}
