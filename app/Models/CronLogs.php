<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class CronLogs extends Model
{

    use SoftDeletes;
    protected $table = 'cron_logs';
    protected $dates = ['deleted_at'];

}
