<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class TelegramBot extends Model
{
    use SoftDeletes;
    //
    protected $table = 'telegram_bots';
    protected $dates = ['deleted_at'];
}
