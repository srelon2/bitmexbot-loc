<?php
namespace App\Models\Admin;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Socket extends Authenticatable
{
    use SoftDeletes;
    protected $table = 'socket';
    protected $dates = ['deleted_at'];
}
