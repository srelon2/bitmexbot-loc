<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class BotSettings extends Model
{

    use SoftDeletes;
    protected $table = 'bot_settings';
    protected $dates = ['deleted_at'];

    public function getApi(){
        return $this->belongsTo('App\Models\ApiKeys', 'api_id');
    }
    public function signals(){
        return $this->hasMany('App\Models\BotSignals', 'bot_id');
    }
}
