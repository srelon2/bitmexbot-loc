<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(env("HTTPS")) {
            $this->app['url']->forceScheme('https');
        }

//        if (env("HTTPS")) {
//            $this->app['url']->forceScheme('https');
//        }
        Schema::defaultStringLength(191);
        if (file_exists($assetsFile = __DIR__ . '/../../resources/assets/assets.php')) {
            include $assetsFile;
        }
    }
}
