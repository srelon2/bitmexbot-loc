<?php

namespace App\Requests;

use Illuminate\Support\Facades\Auth;

class BotSettingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::guest()) abort('403');

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->submit=='active') {
            return [
                'amount' => 'required',
                'api_id' => 'required|integer',
                'currency_pair' => 'required',
                'take_profit' => 'required|numeric',
                'stop_loss' => 'required|numeric',
                'steps' => 'required',
                'submit' => 'required',
            ];
        } else {
            return [
                'submit' => 'required',
                'currency_pair' => 'required',
            ];
        }
    }

    public function messages()
    {
        return [
//            'name.required' => 'Please select page.',
        ];
    }

}
