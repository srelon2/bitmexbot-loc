<?php

namespace App\Requests;

use Illuminate\Support\Facades\Auth;

class ApiKeysRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::guest()) abort('403');

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key' => 'required',
            'secret' => 'required',
        ];
    }

    public function messages()
    {
        return [
//            'name.required' => 'Please select page.',
        ];
    }

}
