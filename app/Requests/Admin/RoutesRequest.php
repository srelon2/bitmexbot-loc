<?php

namespace App\Requests\Admin;

use App\Traits\Admin\RolesTraits;
use App\Requests\Request;
use Illuminate\Support\Facades\Auth;

class RoutesRequest extends Request
{

    use RolesTraits;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $admin= Auth::guard('admin')->user();
        if($this->accessesRoles($admin,'AdminRoutesController')['edit']) {
            return true;
        } else {
            return abort('403');
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data= [
            'name' => 'required',
            'link' => 'required',
            'function' => 'required',
            'title' => 'required',
        ];
        if($this->request->has('add')) {
            $data['newController'] = 'required|unique:controllers,name';
        } else {
            $data['controller'] = 'required';
        }
        return $data;
    }

    public function messages()
    {
        return [
//            'name.required' => 'Please select page.',
        ];
    }

}
