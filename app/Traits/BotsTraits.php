<?php

namespace App\Traits;

use App\Models\Admin\Socket;
//use App\Models\BotBalances;
use App\Models\BotExections;
use App\Models\BotSettings;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Unirest\Exception;

use App\Http\Controllers\Api\TraderFunction\Trader;

trait BotsTraits{
    public function getUserBots($user_id) {
        return BotSettings::where([
            ['user', $user_id]
        ])->get();
    }
    public function getSettings($where= false) {
        $bots= BotSettings::query();
        if($where) $bots->where($where);

        return $bots->get();
    }
    public function getPatterns($user_id) {
        return BotSettings::where([
            ['user_id', $user_id],
            ['saved', 1]
        ])->get();
    }
    public function getPattern($id) {
        return BotSettings::find($id);
    }


    public function updateBots($data, $id) {
        if($id) {
            $bot= BotSettings::find($id);
        } else {
            $bot= new BotSettings;
        }

        if(empty($id) || $data['submit']!= 'disabled') {
            $user_id= Auth::id();
            if($data['submit']== 'active') {
                $data['active']= 2;
            } elseif($data['submit']== 'update'){
                $data= [
                  'name'=> $data['name'], 'closed'=>$data['closed'], 'timer'=> $data['timer'], 'check_position'=>$data['check_position']
                ];
                $update= true;
            } elseif($data['submit']=='saved') {
                $data['saved']= 1;

                $bot= $this->getBotSettings($user_id, [['saved', 1],['name', (($data['name']) ? $data['name'] : $data['currency_pair'])]])->first();
                if(empty($bot)) {
                    $bot= new BotSettings;
                }
                $bot->name= ($data['name']) ? $data['name'] : $data['currency_pair'];
                unset($data['name']);
            } else {
                $data= [
                    'mess'=> 'Ошибка! Неправильные данные',
                    'status'=> 'error',
                    'bot_id'=> false
                ];
                return $data;
            }
            if(empty($update)) {
                $api=$this->getApiKey($data['api_id'], $user_id);
                if(empty($api)) {
                    $data= [
                        'mess'=> 'Ошибка Api Key! Ключ не найден',
                        'status'=> 'error',
                    ];
                    $data['bot_id']= $id;

                    return $data;
                }

                unset($data['submit']);
                $points= [
                    'XBTUSD'=> '1',
                    'XBT7D_U105'=> '0.00001',
                    'XBTJPY'=> '100',
                    'ADAU19'=> '0.00000001',
                    'BCHU19'=> '0.00001',
                    'EOSU19'=> '0.0000001',
                    'ETHUSD'=> '1',
                    'LTCU19'=> '0.000005',
                    'TRXU19'=> '0.00000001',
                    'XRPU19'=> '0.00000001',
                    'XBTKRW'=> '1000',
                ];
                if(isset($data['currency_pair'])) {
                    $bot->points= $points[$data['currency_pair']];
                }

                $data['user_id']= $user_id;
                $data['start_date']=Carbon::now()->format('Y-m-d H:i:s');
//                $this->balanceCountDisable($bot);
                $bot->count=$bot->count+1;

                if(is_array(json_decode($data['steps']))) {
                    $bot->steps= $data['steps'];
                    unset($data['steps']);
                } else {
                    $data= [
                        'mess'=> 'Ошибка! Неправильные данные',
                        'status'=> 'error',
                        'bot_id'=> false
                    ];
                    return $data;
                }
            }
            if(empty($data['timer'])) {
                $data['timer']= 0;
            }
            foreach ($data as $key=> $value) {
                if(isset($value)) {
                    $bot->$key= htmlspecialchars($value);
                }
            }
            $bot->save();
            if(isset($update)) {
                $bot_id= $bot->id;
                $data= [
                    'mess'=> 'Настройки успешно обновлены',
                    'status'=> 'success',
                ];
            } elseif($bot->active==2) {
                $activeBot= $this->getBotSettings(Auth::id(), [['id', '!=', $bot->id],['active', '!=', 0], ['api_id', $bot->api_id], ['currency_pair', $bot->currency_pair]]);
                if(count($activeBot)) {
                    $data= [
                        'mess'=> 'Данный Api Key используется в других настройках!',
                        'status'=> 'error',
                    ];
                } else {
                    $data= $this->startBot($bot->id);
                }

//                $data= [
//                    'mess'=> 'Настройки успешно обновлены',
//                    'status'=> 'success',
//                ];
                $bot_id= $bot->id;
            } else {
                $bot_id= false;
                $data= [
                    'mess'=> 'Шаблон настроек сохранен',
                    'status'=> 'success',
                ];
            }
        } else {
//            $this->clouseOpenSignals($bot);
            $bot->active= 0;
            $bot->save();
            $data= [
                'mess'=> 'Бот остановлен',
                'status'=> 'success',
            ];
            $bot_id= $bot->id;
        }
        $data['bot_id']= $bot_id;
        return $data;
    }
    public function getBot($id, $user_id=false) {
        $bot=  BotSettings::where([
            ['id','=', $id],
        ]);
        if($user_id) {
            $bot= $bot->where('user_id', $user_id);
        }
        return $bot->first();
    }

    public function getActiveBots($active= 1) {
        if(is_array($active)) {
            $bots= BotSettings::whereIn('active', $active)->get();
        } else {
            $bots= BotSettings::where([
                ['active', $active]
            ])->get();
        }
        return $bots;
    }




    public function searchBot($searchValue, $post= false) {
        $items= BotSettings::where([
            ['user_id', Auth::id()],
            ['saved', 0]
        ])->where(function ($query) use ($searchValue){
            $query->where('id', '=', $searchValue)
                ->orwhere('name', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('currency_pair', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('liquidation_price', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('avg_price', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('current_qty', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('active', 'LIKE', '%' . $searchValue . '%');
        })->get();
        $items= $items->transform(function ($item){

            $item->profit= 0;
            return  $item;
        });
        if($post) {
            if($post['order'][0]['dir']=='asc') {
                $items= $items->sortBy($post['table'][$post['order'][0]['column']]);
            } else {
                $items= $items->sortByDesc($post['table'][$post['order'][0]['column']]);
            }
            $data= [
                'total'=> $items->count(),
                'data'=> $items->slice($post['start'])->take($post['length'])
            ];
            return $data;
        } else {
            return $items;
        }
    }

    public function ajaxTableBot($post) {
        $post['table']= [
            'id', 'name', 'currency_pair', 'liquidation_price', "avg_price", "current_qty", "active"
        ];
        $items= $this->searchBot($post['search']['value'], $post);
        $data= array();
        foreach ($items['data'] as $item) {
            $profit= $this::printNum($item->profit);
            switch (true) {
                case ($item->profit>0):
                    $profit= "<span class='text-success'><i class='fa fa-long-arrow-up'></i> ".$profit."</span>";
                    break;
                case ($item->profit<0):
                    $profit= "<span class='text-danger'><i class='fa fa-long-arrow-down'></i> ".$profit."</span>";
                    break;
                default:
                    $profit= "<span class='text-default'>".$profit."</span>";
            }
            $data[]= [
                $item->id,
                $item->name,
                $item->currency_pair,
                $item->liquidation_price,
                $item->avg_price,
                $item->current_qty,
                $profit,
                "<div class='text-center'>".(($item->active) ? "<span class='label label-success'>Активный</span>" : "<span class='label label-danger'>Не активный</span>")."</div>",
                "<div class='pull-right'>
                    <a href='#' class='btn btn-danger btn-sm sa-warning' data-id='".$item->id."'><i class=\"fa fa-trash\"></i></a>
                    <a href=".route('dashboard.bot.info', ['id'=> $item->id])." class='btn btn-primary btn-sm'><i class=\"fa fa-edit mg-r-10\"></i>Редактировать</a>
                </div>",
            ];
        }
//        if(isset($post['order'][0]['dir'])) {
//            if($post['order'][0]['dir']=='asc') {
//                $data= $data->sortByDesc(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            } else {
//                $data= $data->sortBy(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            }
//        };
        ## Response
        return array(
            "iTotalRecords" => $items['total'],
            "iTotalDisplayRecords" => $items['total'],
            "aaData" => $data
        );
    }


    public function updateSocketDate($socket_id) {
        $socket= Socket::find($socket_id);
        $socket->date= Carbon::now()->format('Y-m-d H:i:s');
        $socket->save();

        return true;
    }
    public function socketUpPosition($response) {

        foreach ($response->data as $data) {
            $position= collect($data)->all();
            $bot= BotSettings::where([
                ['account', $position['account']],
                ['currency_pair', $position['symbol']],
                ['active', 1]
            ])->first();

            if($bot) {
                $this->updatePosition($position, $bot);
            }
        }
    }

    public function updatePosition($position, $bot, $bitmex= false) {
        if(empty($bitmex)) $bitmex= $this->getBitMex($bot->api_id, $bot->currency_pair);
        if(empty($position['avgCostPrice'])) {
            $position= collect($bitmex->getOpenPositions());
            $position= $position->where('symbol', $bot->currency_pair)->first();
        }
         $signals= $this->getSignals($bot->id, $bot->user_id, [['status', 1]]);
         if(count($signals)) {
//             $cum_qty= $this->getSignals($bot->id, $bot->user_id, [['order_type', 'Take Profit']])->sum('cum_qty');
             $orderTP= $this->getSignals($bot->id, $bot->user_id, [['order_type', '=','Take Profit'], ['status','=', 0]])->first();

             if(isset($position['currentQty'])) {
                if(is_array($position) && $orderTP && $bot->active==1 && $position['currentQty']==0) {
                    $this->telegramNotification(json_encode($position));
                    $this->telegramNotification(json_encode($orderTP));
                    $this->telegramNotification(json_encode($bot));
                    $bitmex->cancelAllOpenOrders();
                    if($bot->active) {
                        $bot= $this->updateBotStatus($bot);
                        if($bot->timer==0 && $bot->active==2) {
                            $this->startBot($bot->id);
                        }
                    }
                } else {
                    if($bot->current_qty=='update' || empty($orderTP) || abs($position['currentQty']) > abs($bot->current_qty)) {
                        $bot->liquidation_price= $position['liquidationPrice'];
                        $responce= $this->createTakeProfit($bot, $position['avgCostPrice'], $position['currentQty'],  $bitmex);
                        if($responce) {
                            $bot->avg_price= $position['avgCostPrice'];
                            $bot->current_qty= $position['currentQty'];
                            $bot->save();
                        } else {
                            $bot->current_qty= 'update';
                            $bot->start_date= Carbon::now()->addSeconds(10)->format('Y-m-d H:i:s');
                            $bot->save();
                        }
        //                if(isset($position['currentQty'])) {
        //                }

        //                if($bot->avg_price) {
        //                    $this->createTakeProfit($bot, $bitmex);
        //                }
                    }
                    if(abs($position['currentQty']) > 0) {
                        $orderSL= $this->getSignals($bot->id, $bot->user_id, [['order_type', '=','Stop Loss'], ['status','=', 0]])->first();
                        if($bot->active== 1 && empty($orderSL)) {
                            $this->createStopLoss($bot->id, $bitmex);
                        }
                    }
                }
            }
         }
        return true;
    }
    public function getSocket() {
        $socket= Socket::first();
        if(empty($socket)) {
            $socket= new Socket;
            $socket->date= Carbon::now()->format('Y-m-d H:i:s');
        }
        $socket->save();

        return $socket;
    }
    public function socketDisable() {
        $socket= $this->getSocket();
        $socket->status= 0;
        $socket->save();
        return true;
    }
    public function startSocketDate($socket_id, $start_date){
        $socket= Socket::find($socket_id);
        $socket->date_start= $start_date;
        $socket->save();

        return true;
    }
    public function checkSocketDate($socket_id, $start_date){
        $socket= Socket::find($socket_id);
        if(strtotime($socket->date_start) > strtotime($start_date)) {
            return 'clouse';
        } elseif($socket->status== 0) {
            $socket->status=1;
            $socket->save();
            return 'reset';
        } else {
            return false;
        }
    }
    public function updateBotStatus($bot) {
        $bot->start_date = Carbon::now()->addMinutes($bot->timer)->format('Y-m-d H:i:s');
        $stop = $this->getSignals($bot->id, $bot->user_id, [['order_type', '=', 'Stop Loss'],['status', '=', 1]]);
        $mess= "<b>Notification!</b>\n";
        $mess.= "<b>".hex2bin('F09F8694')." Bot ID</b>: /b".$bot->id."\n";
        $mess.= "<b>Name</b>: ".(($bot->name)? $bot->name : $bot->currency_pair)." Count: ".$bot->count."\n";
        $orders= $this->getSignals($bot->id, $bot->user_id, [['cum_qty', '>', 0]]);
        $bitmex=  $this->getBitMex($bot->api_id, $bot->currency_pair);
        foreach ($orders as $order) {
            $responces= $bitmex->getExection($order->order_id);
            if($responces && empty($responces['error'])) {

                foreach ($responces as $responce) {
                    $this->createBotExection($responce, $order->order_type, $bot);
                }
            } else {
                $mess= "Error save Exections";
                $mess.="\nBot Id: ".$bot->id."\n";
                $mess.="\nCount: ".$bot->count."\n";
                $mess.="\nSignal Id: ".$order->id."\n";
                $mess.="\nOrder Id: ".$order->order_id."\n";
                $this->telegramNotification($mess);
            }
        }

        $exections= $this->getExections($bot->id, $bot->count);
        $profit= $this->getCountProfit($exections);
        if($profit != 0) {
            $mess.= "<b>Profit</b>: ".$profit['profit'].' XBT'."\n";
        }
        $wallet= $bitmex->getWalletSummary()->last();
        if(isset($wallet)) {
            $mess.= "<b>Amount</b>: ".$this::printNum($wallet['amount']/100000000)."\n";
            $mess.= "<b>Realised Pnl</b>: ".$this::printNum($wallet['realisedPnl']/100000000)."\n";
            $mess.= "<b>Wallet Balance</b>: ".$this::printNum($wallet['walletBalance']/100000000)."\n";
            $mess.= "<b>Margin Balance</b>: ".$this::printNum($wallet['marginBalance']/100000000)."\n";
        }

        if ($bot->closed == 1) {
            $bot->active = 0;
            $mess.= "<b>Status</b>: ".((count($stop)) ? 'Сработал Stop Loss' : 'Сработал Take profit');
            $this->sendMess($bot->user_id, $mess, [[['text'=>'Запустить', 'callback_data'=> 'action=info-bots&id='.$bot->id.'&status=active']]]);
        } elseif (count($stop) && $bot->closed == 2) {
            $bot->active = 0;
            $mess.= "<b>Status</b>: Сработал Stop Loss";
            $this->sendMess($bot->user_id, $mess, [[['text'=>'Запустить', 'callback_data'=> 'action=info-bots&id='.$bot->id.'&status=active']]]);
        } else {
            if(count($stop)) {
                $mess.= "<b>Status</b>: Сработал Stop Loss"."\n";
            } else {
                $mess.= "<b>Status</b>: Сетка выполнена"."\n";
            }
            $this->sendMess($bot->user_id, $mess);
//            $this->balanceCountDisable($bot);
            $bot->count = $bot->count + 1;
            $bot->active = 2;
        }
        $this->telegramNotification($mess);

        $bot->save();
        
        return $bot;
    }
    public function getBotSettings($user_id, $where= false) {
        $bots= BotSettings::where('user_id', $user_id);

        if($where) {
            $bots= $bots->where($where);
        }
        return $bots->get();
    }
    public function accountBotSettings($account, $currency) {
        return BotSettings::where([
            ['account', $account],
            ['currency_pair', $currency],
            ['active', 1]
        ])->first();
    }

    public function destroySettings($api_id) {
        $bots= BotSettings::where([['api_id', $api_id]]);
        $bots->update(['active'=> 0]);
//        foreach ($bots->get() as $bot) {
//            $this->clouseOpenSignals($bot);
//        }
        return true;
    }
    public function clousePosition($bot, $bitmex= false) {
        if(empty($bitmex)) $bitmex= $this->getBitMex($bot->api_id, $bot->currency_pair);
        $responce= $bitmex->closePosition();
        $bitmex->cancelAllOpenOrders();
        if($responce) {
            if($bot->active) {
                $bot= $this->updateBotStatus($bot);
                if($bot->timer==0 && $bot->active==2) {
                    $this->startBot($bot->id);
                }
            }
            return $responce;
        }
        return false;
    }
    public function checkPositions() {
        $bots= $this->getSettings([['active', 1], ['current_qty', 'update'], ['start_date', '<', Carbon::now()->format('Y-m-d H:i:s')]]);
        if(count($bots)) {
            foreach($bots as $bot) {
                $this->updatePosition(false, $bot);
            }
        }
    }
    public function getExectionInID($exec_id) {
        return BotExections::where('exec_id', $exec_id)->first();
    }

    public function updateExecution($orders) {
        foreach ($orders->data as $data) {
            if(isset($data->commission)) {
                $signal = $this->getSignal([['order_id', '=', $data->orderID]]);
                if (isset($signal)) {
                    $this->createBotExection(collect($data)->all(), $signal->order_type, $signal->bot);
                }
            }
        }
    }

    public function createBotExection($response, $order_type, $bot) {

        if(isset($response['execID'])) {
                $exection= $this->getExectionInID($response['execID']);
                if(empty($exection)) {
                    $exection= new BotExections;
                    $exection->count= $bot->count;
                    $exection->api_id= $bot->api_id;
                    $exection->bot_id= $bot->id;
                    $exection->user_id= $bot->user_id;
                    $exection->account= $response['account'];
                    $exection->side= $response['side'];
                    $exection->exec_id= $response['execID'];
                    $exection->order_id= $response['orderID'];
                    $exection->currency_pair= $response['symbol'];
                    $exection->order_type= $response['ordType'];
                    $exection->type= $order_type;
                    $exection->order_qty= $response['orderQty'];
                    $exection->leaves_qty= $response['leavesQty'];
                    $exection->cum_qty= $response['cumQty'];
                    $exection->price= $response['price'];
                    $exection->avg_px= $response['avgPx'];
                    $exection->order_status= $response['ordStatus'];
                    $exection->commission= $response['commission'];
                    $exection->exec_cost= $response['execCost'];
                    $exection->exec_comm= $response['execComm'];
                    $exection->home_notional= $response['homeNotional'];
                    $exection->foreign_notional= $response['foreignNotional'];
                    $exection->save();
            }
        } else {
            $mess= "Execution not Found";
            $mess.="\nBot Id: ".$bot->id."\n";
            $mess.="\nCount: ".$bot->count."\n";
            $mess.="\nError: ".json_encode($bot['error'])."\n";
            $this->telegramNotification($mess);
            return false;
        }

    }

    public function searchExections($searchValue, $bot_id, $post= false) {
        $items= BotExections::where('bot_id', '=', $bot_id)->where(function ($query) use ($searchValue){
            $query->where('id', '=', $searchValue)
                ->orwhere('count', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('currency_pair', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('order_type', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('price', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('avg_px', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('order_qty', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('cum_qty', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('side', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('commission', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('exec_comm', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('order_status', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('updated_at', 'LIKE', '%' . $searchValue . '%');
        })->get();

        if($post) {
            if($post['order'][0]['dir']=='asc') {
//                if($post['table'][$post['order'][0]['column']]=='profit') {
//                    $items=$items->sortBy(function($item, $key) {
//                        $profit= (($item->type== 'Buy') ? $item->avg_price - $item->price : $item->price- $item->avg_price)*$item->current_qty;
//                        return $profit;
//                    });
//                } else {
//                    $items= $items->sortBy($post['table'][$post['order'][0]['column']]);
//                }
                $items= $items->sortBy($post['table'][$post['order'][0]['column']]);
            } else {
//                if($post['table'][$post['order'][0]['column']]=='profit') {
//                    $items=$items->sortByDesc(function($item, $key) {
//                        $profit= (($item->type== 'Buy') ? $item->avg_price - $item->price : $item->price- $item->avg_price)*$item->current_qty;
//                        return $profit;
//                    });
//                } else {
//                  $items= $items->sortByDesc($post['table'][$post['order'][0]['column']]);
//                }
                $items= $items->sortByDesc($post['table'][$post['order'][0]['column']]);
            }
            $data= [
                'total'=> $items->count(),
                'data'=> $items->slice($post['start'])->take($post['length'])
            ];
            return $data;
        } else {
            return $items;
        }
    }


    public function ajaxTableExections($post, $bot_id) {
        $post['table']= [
            'id', 'count', 'currency_pair','order_type','price', 'avg_px', 'order_qty', 'cum_qty', "side", "commission", "exec_comm", "order_status", "updated_at"
        ];
        $items= $this->searchExections($post['search']['value'], $bot_id, $post);
        $data= array();
        foreach ($items['data'] as $item) {

            switch ($item->side) {
                case 'Buy':
                    $type= "<span class='text-success'>".$item->side."</span>";
                    break;
                case 'Sell':
                    $type= "<span class='text-danger'>".$item->side."</span>";
                    break;
                default:
                    $type= "<span class='text-default'>".$item->side."</span>";
            }
            switch ($item->order_status) {
                case 'Filled':
                    $status= "<span class='label label-success'>".$item->order_status."</span>";
                    break;
                case 'PartiallyFilled':
                    $status= "<span class='label label-primary'>".$item->order_status."</span>";
                    break;
                default:
                    $status= "<span class='label label-default'>".$item->order_status."</span>";
            }
            if($item->commission>0) {
                $commission= "<span class='text-danger'>".$item->commission."%</span>";
            } else {
                $commission= "<span class='text-success'>".$item->commission."%</span>";
            }

            $data[]= [
                $item->id,
                $item->count,
                $item->currency_pair,
                $item->order_type,
                $item->price,
                $item->avg_px,
                $item->order_qty,
                $item->cum_qty,
                $type,
                $commission,
                $this::printNum($item->exec_comm/100000000),
                $status,
                $item->updated_at->format('Y-m-d H:i:s'),
            ];
        }
//        if(isset($post['order'][0]['dir'])) {
//            if($post['order'][0]['dir']=='asc') {
//                $data= $data->sortByDesc(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            } else {
//                $data= $data->sortBy(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            }
//        };
        ## Response
        return array(
            "iTotalRecords" => $items['total'],
            "iTotalDisplayRecords" => $items['total'],
            "aaData" => $data
        );
    }
    public function getExections($bot_id, $count= false) {
        $exections= BotExections::where([
            ['bot_id', $bot_id],
        ]);
        if($count) $exections= $exections->where('count', $count);
        return $exections->get();
    }
    public function getExProfit($bot_id, $count= false) {
        $exections= $this->getExections($bot_id);
        if($count) {
            $exections= $exections->where('count', '<=', $count);
        }
        $exGroup= $exections->groupBy('count');
        $profits= collect();
        foreach ($exGroup as $key=> $group) {
            $profits->put($key, $this->getCountProfit($group));
        }
        return $profits;
    }
    public function getCountProfit($exection) {
        if(count($exection)) {
            $last= $exection->last();
            $limit= $exection->whereNotIn('type', ['Take Profit', 'Stop Loss']);
            $limitBuy= abs($limit->where('side', 'Buy')->sum('exec_cost'));
            $limitSell= abs($limit->where('side', 'Sell')->sum('exec_cost'));

            $take= $exection->where('type', 'Take Profit');
            $takeBuy= abs($take->where('side', 'Buy')->sum('exec_cost'));
            $takeSell= abs($take->where('side', 'Sell')->sum('exec_cost'));

            $stop= $exection->where('type', 'Stop Loss');
            $stopBuy= abs($stop->where('side', 'Buy')->sum('exec_cost'));
            $stopSell= abs($stop->where('side', 'Sell')->sum('exec_cost'));
            if(abs($take->sum('exec_cost')) > 0 || abs($stop->sum('exec_cost')) > 0) {
                $commission= $exection->sum('exec_comm');
                $profitBuy= $takeBuy - $limitSell- $stopBuy;
                $profitSell= $limitBuy - $takeSell- $stopSell;
                $profit= $profitBuy+ $profitSell;
                if($profitBuy > $profitSell) {
                    $type= 'Buy';
                } else {
                    $type= 'Sell';
                }
                return [
                    'profit'=> $this::printNum(($profit-$commission)/100000000),
                    'type'=> $type,
                    'date'=> $last->updated_at,
                ];
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
//    public function getBotBalance($bot, $status, $type=false) {
//        $balance= BotBalances::where([
//            ['bot_id', $bot->id],
//            ['user_id', $bot->user_id],
//            ['currency_pair', $bot->currency_pair],
//            ['account', $bot->account],
//            ['count', $bot->count],
//        ]);
//        if($status) $balance= $balance->where('status', $status);
//        if($type) $balance= $balance->where('type', $type);
//        return $balance->first();
//    }
//    public function getBotBalances($bot, $count=false, $status= false, $type= false) {
//        $balance= BotBalances::where([
//            ['bot_id', $bot->id],
//            ['user_id', $bot->user_id],
//            ['currency_pair', $bot->currency_pair],
//            ['account', $bot->account],
//        ]);
//        if($count) $balance= $balance->where('count', $count);
//        if($status) $balance= $balance->where('status', $status);
//        if($type) $balance= $balance->where('type', $type);
//        return $balance->get();
//    }
//    public function createBotBalances($bot) {
////        if($bot->avg_price > 1) {
////            $avg_price= 1/$bot->avg_price;
////        } else {
////            $avg_price= $bot->avg_price;
////        }
//        switch ($bot->currency_pair) {
//            case 'XBTUSD':
//                $avg_price= 1/$bot->avg_price;
//                break;
//            case 'ETHUSD':
//                $avg_price= $bot->avg_price/1000000;
//                break;
//            case 'XBTJPY':
//            case 'XBTKRW':
//                $price= rtrim($bot->avg_price,'0');
//                $avg_price= 1/$price;
//                break;
//            default:
//                $avg_price= $bot->avg_price;
//        }
//
//        $bot= $this->getBot($bot->id);
//        if(isset($bot->current_qty) && $bot->current_qty != 0) {
//            if($bot->current_qty> 0) {
//                $old=  $this->getBotBalance($bot, 1, 'Buy');
//            } else {
//                $old=  $this->getBotBalance($bot, 1, 'Sell');
//            }
//            if(isset($old) && empty($old->price)) {
//                $balance= $old;
//            } elseif(isset($old) && $old->avg_price != $avg_price) {
//                $old->status = 2;
//                $old->save();
//                $balance = new BotBalances;
//            } elseif(isset($old)){
//                return false;
//            } else  {
//                $balance= new BotBalances;
//            }
//            if($bot->current_qty> 0) {
//                $balance->type= 'Buy';
//                $cum_qty= $this->getBotBalances($bot, $bot->count, false, 'Buy')->sum('current_qty');
//            } else {
//                $balance->type= 'Sell';
//                $cum_qty= $this->getBotBalances($bot, $bot->count, false, 'Sell')->sum('current_qty');
//            }
//
//
//            $balance->bot_id= $bot->id;
//            $balance->api_id= $bot->api_id;
//            $balance->user_id= $bot->user_id;
//            $balance->count= $bot->count;
//            $balance->currency_pair= $bot->currency_pair;
//            $balance->account= $bot->account;
//            $balance->avg_price= $avg_price;
//            $balance->order_qty= abs($bot->current_qty)-$cum_qty;
//            $balance->leaves_qty= abs($bot->current_qty)-$cum_qty;
//            $balance->status= 1;
//
//            $balance->save();
//
//            return $balance;
//        } else {
//            return false;
//        }
//    }
//
//    public function updateBotBalances($signal) {
//        $bot= $signal->bot;
//        if($signal->side== 'Buy') {
//            $type= 'Sell';
//        } else {
//            $type= 'Buy';
//        }
//        $item= $this->getBotBalance($bot, 1, $type);
//        if(isset($item)) {
//            $balances= $this->getBotBalances($bot, $bot->count,false, $item->type);
//            $signal_cum= $this->getSignals($signal->bot_id, $signal->user_id, [['order_type', '!=', 'Limit'], ['side', $signal->side]])->sum('cum_qty');
//            $cum_qty= $signal_cum-$balances->sum('current_qty');
//            if($cum_qty > 0) {
//                switch ($item->currency_pair) {
//                    case 'XBTUSD':
//                        $avg_price= 1/$signal->price;
//                        break;
//                    case 'ETHUSD':
//                        $avg_price= $signal->price/1000000;
//                        break;
//                    case 'XBTJPY':
//                    case 'XBTKRW':
//                        $price= rtrim($signal->price,'0');
//                        $avg_price= 1/$price;
//                        break;
//                    default:
//                        $avg_price= $signal->price;
//                }
//    //                if($bot->avg_price > 1) {
//    //                    $avg_price= 1/$signal->price;
//    //                } else {
//    //                    $avg_price= $signal->price;
//    //                }
//
//                if(isset($item->price)) {
//
//                    $this->telegramNotification('cum_qty '." ".$cum_qty." test ".$signal_cum.' current_qty: '.$balances->sum('current_qty')." \n".json_encode($signal));
/////
//                    if(isset($item->price) && $item->price!= $avg_price) {
//                        $item->status= 2;
//                        $item->save();
//
//                        $balance= new BotBalances;
//                        $balance->bot_id= $item->bot_id;
//                        $balance->api_id= $item->api_id;
//                        $balance->user_id= $item->user_id;
//                        $balance->currency_pair= $item->currency_pair;
//                        $balance->count= $bot->count;
//                        $balance->account= $item->account;
//                        $balance->avg_price= $item->avg_price;
//                        $balance->order_qty= $item->leaves_qty;
//                        $balance->leaves_qty= $item->leaves_qty;
//                        $balance->type= $item->type;
//                    } else{
//                        $balance= $item;
//                        $cum_qty= $cum_qty+ $item->current_qty;
//                    }
//
//                    $balance->price= $avg_price;
//
//                    if($balance->order_qty < $cum_qty) {
//                        $balance->order_qty= $cum_qty;
//                    }
//                        $balance->leaves_qty= $balance->order_qty- $cum_qty;
//                        $balance->current_qty= $cum_qty;
//                        if($cum_qty==$balance->order_qty) {
//                            $balance->status= 2;
//                        } else {
//                            $balance->status= 1;
//                        }
//                        $balance->save();
//
//                        $this->telegramNotification('leaves qty '." ".($balance->order_qty- $cum_qty)." order_qty ".$balance->current_qty.' current_qty: '.$cum_qty." \n");
//                        $this->telegramNotification(json_encode($balance));
//                        return $balance;
//                } else {
//                        if($cum_qty) {
//                            $balance= $item;
//                            $this->telegramNotification('FIRST leaves qty '." ".($balance->order_qty- $cum_qty)." order_qty ".$balance->current_qty.' current_qty: '.$cum_qty." \n".json_encode($signal));
//                            $balance->price= $avg_price;
//                            if($balance->order_qty < $cum_qty) {
//                                $balance->order_qty= $cum_qty;
//                            }
//
//                            $balance->leaves_qty= $balance->order_qty- $cum_qty;
//                            $balance->current_qty= $cum_qty;
//                            if($cum_qty==$balance->order_qty) {
//                                $balance->status= 2;
//                            } else {
//                                $balance->status= 1;
//                            }
//                            $balance->save();
//                            $this->telegramNotification(json_encode($balance));
//
//                            return $balance;
//                        }
//                }
//            }
//        }
//
//        return false;
//
//    }
//
//
//    public function searchBalances($searchValue, $bot_id, $post= false) {
//        $items= BotBalances::where('bot_id', '=', $bot_id)->where(function ($query) use ($searchValue){
//            $query->where('id', '=', $searchValue)
//                ->orwhere('count', 'LIKE', '%' . $searchValue . '%')
//                ->orwhere('currency_pair', 'LIKE', '%' . $searchValue . '%')
//                ->orwhere('type', 'LIKE', '%' . $searchValue . '%')
//                ->orwhere('avg_price', 'LIKE', '%' . $searchValue . '%')
//                ->orwhere('price', 'LIKE', '%' . $searchValue . '%')
//                ->orwhere('current_qty', 'LIKE', '%' . $searchValue . '%')
//                ->orwhere('status', 'LIKE', '%' . $searchValue . '%')
//                ->orwhere('updated_at', 'LIKE', '%' . $searchValue . '%');
//        })->get();
//
//        $items->transform(function ($item){
//            $item->profit=  (($item->type== 'Buy') ? $item->avg_price - $item->price : $item->price- $item->avg_price)*$item->current_qty;
//
//            return  $item;
//        });
//
//        if($post) {
//            if($post['order'][0]['dir']=='asc') {
////                if($post['table'][$post['order'][0]['column']]=='profit') {
////                    $items=$items->sortBy(function($item, $key) {
////                        $profit= (($item->type== 'Buy') ? $item->avg_price - $item->price : $item->price- $item->avg_price)*$item->current_qty;
////                        return $profit;
////                    });
////                } else {
////                    $items= $items->sortBy($post['table'][$post['order'][0]['column']]);
////                }
//                $items= $items->sortBy($post['table'][$post['order'][0]['column']]);
//            } else {
////                if($post['table'][$post['order'][0]['column']]=='profit') {
////                    $items=$items->sortByDesc(function($item, $key) {
////                        $profit= (($item->type== 'Buy') ? $item->avg_price - $item->price : $item->price- $item->avg_price)*$item->current_qty;
////                        return $profit;
////                    });
////                } else {
////                  $items= $items->sortByDesc($post['table'][$post['order'][0]['column']]);
////                }
//                $items= $items->sortByDesc($post['table'][$post['order'][0]['column']]);
//            }
//            $data= [
//                'total'=> $items->count(),
//                'data'=> $items->slice($post['start'])->take($post['length'])
//            ];
//            return $data;
//        } else {
//            return $items;
//        }
//    }
//
//
//    public function ajaxTableBalances($post, $bot_id) {
//        $post['table']= [
//            'id', 'count', 'currency_pair','type','profit', 'avg_price', 'price', 'order_qty', "current_qty", "status", "updated_at"
//        ];
//        $items= $this->searchBalances($post['search']['value'], $bot_id, $post);
//        $data= array();
//        foreach ($items['data'] as $item) {
//
//            switch ($item->type) {
//                case 'Buy':
//                    $type= "<span class='text-success'>".$item->type."</span>";
//                    break;
//                case 'Sell':
//                    $type= "<span class='text-danger'>".$item->type."</span>";
//                    break;
//                default:
//                    $type= "<span class='text-default'>".$item->type."</span>";
//            }
//            $profit= $this::printNum($item->profit);
//            switch (true) {
//                case ($item->profit>0):
//                    $profit= "<span class='text-success'><i class='fa fa-long-arrow-up'></i> ".$profit."</span>";
//                    break;
//                case ($item->profit<0):
//                    $profit= "<span class='text-danger'><i class='fa fa-long-arrow-down'></i> ".$profit."</span>";
//                    break;
//                default:
//                    $profit= "<span class='text-default'>".$profit."</span>";
//            }
//            switch ($item->status) {
//                case '1':
//                    $status= "<span class='label label-default'>Открыт</span>";
//                    break;
//                case '2':
//                    $status= "<span class='label label-success'>Завершено</span>";
//                    break;
//                default:
//                    $status= "<span class='label label-default'>Ожидание</span>";
//            }
//
//            $data[]= [
//                $item->id,
//                $item->count,
//                $item->currency_pair,
//                $type,
//                $profit,
//                $this::printNum($item->avg_price),
//                $this::printNum($item->price),
//                $item->order_qty,
//                $item->current_qty,
//                $status,
//                $item->updated_at->format('Y-m-d H:i:s'),
//            ];
//        }
////        if(isset($post['order'][0]['dir'])) {
////            if($post['order'][0]['dir']=='asc') {
////                $data= $data->sortByDesc(function($query, $key) use($post) {
////                    return $query[$post['order'][0]['column']];
////                });
////            } else {
////                $data= $data->sortBy(function($query, $key) use($post) {
////                    return $query[$post['order'][0]['column']];
////                });
////            }
////        };
//        ## Response
//        return array(
//            "iTotalRecords" => $items['total'],
//            "iTotalDisplayRecords" => $items['total'],
//            "aaData" => $data
//        );
//    }
//    public function getBalances($user_id, $bot_id) {
//        $balances= BotBalances::where([
//            ['user_id', $user_id],
//            ['bot_id', $bot_id]
//        ])->get();
//
//        $balances->transform(function ($item){
//            $profit=  (($item->type== 'Buy') ? $item->avg_price - $item->price : $item->price- $item->avg_price)*$item->current_qty;
//            switch ($item->currency_pair) {
//                case 'XBTUSD':
//                case 'XBTJPY':
//                case 'XBTKRW':
//                    break;
//                default:
//                    $profit= -1*$profit;
//            }
//
//            $item->profit= $profit;
//
//            return  $item;
//        });
//
//        return $balances;
//    }
//    public function balanceCountDisable($bot) {
//        BotBalances::where([
//            ['bot_id', $bot->id],
//            ['user_id', $bot->user_id],
//            ['currency_pair', $bot->currency_pair],
//            ['account', $bot->account],
//            ['count', $bot->count],
//            ['status', 1],
//        ])->update(['status'=>2]);
//
//        return true;
//    }
//    public function getBotProfit($bot, $count= false) {
//        $balances= $this->getBotBalances($bot, $count)->where('current_qty', '>', 0);
//        if(count($balances)) {
//            $balances->transform(function ($item){
//                $item->profit=  (($item->type== 'Buy') ? $item->avg_price - $item->price : $item->price- $item->avg_price)*$item->current_qty;
//                return $item;
//            });
//            $profit= preg_replace('/(\..{8}).*/', '$1',   sprintf("%.8f", $balances->sum('profit')));
//
//            switch ($bot->currency_pair) {
//                case 'XBTUSD':
//                case 'XBTJPY':
//                case 'XBTKRW':
//                    break;
//                default:
//                    $profit= -1*$profit;
//            }
//
//            return $profit;
//        } else {
//            return 0;
//        }
//    }
//
//    public function updateTradits($currencies) {
//        $bitmex= $this->getBitMex();
//        foreach ($currencies as $symbol) {
//            $tradits= collect($bitmex->getCandles('1m', $symbol, 500))->values();
//            $tradits= $tradits->transform(function ($item, $key){
//                $item['id']= $key;
//                return $item;
//            });
//
//        }
//    }
    public function checkStrategy($symbol, $strategy= 'wma') {
        $start= false;
        switch ($strategy) {
            case 'wma':
                $bitmex= $this->getBitMex();
                $tradits= $bitmex->getCandles('1m', $symbol, 500);
                $wma_one = Trader::wma( $tradits['close'] , 15);
                $wma_two = Trader::wma( $tradits['close'] , 30);
                $result_wma_one = end($wma_one);
                $result_wma_two = end($wma_two);
                if($result_wma_one < $result_wma_two) {
                    $start= true;
                }
            break;
        }
        return $start;
    }
}