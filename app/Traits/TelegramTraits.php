<?php

namespace App\Traits;
use App\Http\Controllers\Api\Telegram\Api;
use Carbon\Carbon;

use App\Models\Admin\TelegramBot;
use App\Models\TelegramUsers;

trait TelegramTraits{
    public function createTelUser($username, $chatId) {
        $tel_user= new TelegramUsers;
        $tel_user->login = $username;
        $tel_user->chat_id = $chatId;

        return $tel_user->save();
    }

    public function getTelUser($chat_id){
        return TelegramUsers::where('chat_id', $chat_id)->first();
    }

    public function getKeyboardTel(){
        $keyboard= [
            [
                ['text'=>'🤖 Bot list'],
            ],[
                ['text'=>'Bots Active'],
                ['text'=>'Bots Disabled'],
            ],
            [
                ['text'=>'🚨 Support'],
            ]
        ];
        return $keyboard;
    }

    public function updateTelUser($data, $user_id) {
        $tel_user= $this->getTelUser($data['chat_id']);
        if(isset($tel_user) && $tel_user->user_id) {
            $data= [
                'mess'=> 'Данный Chat ID уже используется!',
                'status'=> 'error',
            ];
        } elseif(empty($tel_user)) {
            $data= [
                'mess'=> 'Данный Chat ID не найден! Активируйте его в нашем боте',
                'status'=> 'error',
            ];
        } else {
            $tel_user->user_id= $user_id;
            $tel_user->active= 1;
            $tel_user->save();
            $data= [
                'mess'=> 'Chat ID успешно подключен!',
                'status'=> 'success',
            ];

            $this->sendMess($user_id, $data['mess']);
        }
        return $data;
    }

    public function destroyTelUser($id, $user_id) {
        $item= TelegramUsers::where([
            ['id', $id],
            ['user_id', $user_id],
        ])->first();

        if(isset($item)) {
            $data= [
                'mess'=> 'Chat ID успешно удален',
                'status'=> 'success',
            ];
            $this->sendMess($user_id, $data['mess'], [[['text'=>'Start']]], 'keyboard');
            $item->delete();

        } else {
            $data= [
                'mess'=> 'Данный Chat ID не найден',
                'status'=> 'error',
            ];
        }

        return $data;
    }
    public function sendMess($user_id, $mess, $keyboard= false, $type= 'inline_keyboard') {
        if(empty($keyboard)) {
            $keyboard= $this->getKeyboardTel();
            $type= false;
        }
        $user= $this->getUser($user_id);
        if(isset($user->telegram) && $user->telegram->active) {
            $api= $this->telegramApi('bot');

            if($type== 'inline_keyboard') {
                $reply_markup = $api->replyKeyboardMarkup([
                    'inline_keyboard'=> $keyboard,
                ]);
            } else {
                $reply_markup = $api->replyKeyboardMarkup([
                    'keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'selective' => false,
                    'one_time_keyboard' => false,
                ]);
            }

            $api->sendMessage([
                'chat_id' => $user->telegram->chat_id,
                "text"=> $mess,
                "parse_mode"=> "html",
                "reply_markup"=> $reply_markup,
            ]);

            return true;
        } else {
            return false;
        }
    }
}