<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;
use App\Models\ApiKeys;
use App\Http\Controllers\Api\Bitmex\Bitmex;

trait ApiKeysTraits{

    public function updateApiKeys($data) {

        $bitmex= new BitMex($data['key'],$data['secret']);

        $key= htmlspecialchars($data['key']);
        $secret= htmlspecialchars($data['secret']);
        $response= $bitmex->getWallet();
        $api= ApiKeys::where([
            ['key', $key],
            ['secret', $secret],
        ])->first();

        if(empty($response)) {
            $data= [
                'mess'=> 'Ошибка подключения. Неправильный доступ к Api',
                'status'=> 'error',
            ];
        } elseif($api) {
            $data= [
                'mess'=> 'Данный ключ уже используется',
                'status'=> 'error',
            ];
        } else {
            $api= new ApiKeys;
            $api->key= $key;
            $api->secret= $secret;
            $api->user_id= Auth::id();
            $api->active= 1;

            $this->socketDisable();
            $api->save();

            $data= [
                'mess'=> 'Ключ успешно добавлен',
                'status'=> 'success',
            ];
        }


        return $data;
    }
    public function getApiKey($id, $user_id) {
        return ApiKeys::where([
            ['id', '=', $id],
            ['user_id', '=', $user_id],
            ['active', '=', 1],
        ])->first();
    }

    public function getApiKeys($user_id=false){
        $apiKeys= ApiKeys::where([
            ['active', '=', 1],
        ]);
        if($user_id) {
            $apiKeys= $apiKeys->where('user_id', '=', $user_id);
        }

        return $apiKeys->get();
    }
    public function apiKeysDisable($id){
        $item= ApiKeys::find($id);
        $item->active= 0;
        $item->save();
        return $item;
    }



    public function searchApiKeys($searchValue, $post= false) {
        $items= ApiKeys::where([
            ['user_id', Auth::id()],
        ])->where(function ($query) use ($searchValue){
            $query->where('id', '=', $searchValue)
                ->orwhere('key', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('secret', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('active', 'LIKE', '%' . $searchValue . '%');
        });
        if($post) {
            if($post['order'][0]['dir']=='asc') {
                $items= $items->oldest($post['table'][$post['order'][0]['column']]);
            } else {
                $items= $items->latest($post['table'][$post['order'][0]['column']]);
            }
            $data= [
                'total'=> $items->count(),
                'data'=> $items->skip($post['start'])->take($post['length'])->get()
            ];
            return $data;
        } else {
            return $items->get();
        }
    }

    public function ajaxTableApiKeys($post) {
        $post['table']= [
            'id', 'key', 'secret', 'active'
        ];
        $items= $this->searchApiKeys($post['search']['value'], $post);
        $data= array();
        foreach ($items['data'] as $item) {
            $data[]= [
                $item->id,
                $item->key,
                $item->secret,
                "<div class='text-center tx-24'>".(($item->active) ? "<a href='#' data-mess='Все активные настройки по данному ключу будут отключены!' data-action='actionOff' data-id='".$item->id."' class='fa fa-toggle-on text-success sa-warning'></a>" : "<a href='#' data-id='".$item->id."' data-mess='Вы действительно хотите подключить этот Api Key?' data-action='action' class='fa fa-toggle-off text-danger sa-warning'></a>")."</div>",
                "<div class='pull-right'>
                    <a href='#' class='btn btn-danger btn-sm sa-warning' data-mess='Страница будет помещена в корзину!' data-action='delete' data-id='".$item->id."'><i class=\"fa fa-trash\"></i></a>
                </div>",
            ];
        }
//        if(isset($post['order'][0]['dir'])) {
//            if($post['order'][0]['dir']=='asc') {
//                $data= $data->sortByDesc(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            } else {
//                $data= $data->sortBy(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            }
//        };
        ## Response
        return array(
            "iTotalRecords" => $items['total'],
            "iTotalDisplayRecords" => $items['total'],
            "aaData" => $data
        );
    }
    public function actionApiKeys($action, $id) {
        $item= ApiKeys::withTrashed()->find($id);
        try {
            switch ($action) {
                case 'delete':
                    $this->destroySettings($item->id);
                    $item->delete();
                    $mess= 'Добавлено в корзину';
                    break;
                case 'forceDelete':
                    $this->destroySettings($item->id);
                    $item->forceDelete();
                    $mess= 'Удалено с базы';
                    break;
                case 'action':
                    $item->active = 1;
                    $mess= 'Ключ успешно включен';
                    break;
                case 'actionOff':
                    $item->active = 0;
                    $mess= 'Ключ успешно отключен';
                    $this->destroySettings($item->id);
                    break;
                default:
                    $item->restore();
                    $mess= 'Востанновлено с корзины';
            }
            $item->save();
            $data= [
                'status'=> 'success',
                'mess'=> $mess
            ];
            return $data;
        } catch(Exception $e) {
            $data= [
                'status'=> 'error',
                'mess'=> $e
            ];
            return $data;
        }
    }
}