<?php

namespace App\Traits;

use App\Models\Admin\Socket;
use App\Models\BotSignals;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

trait BotSignalsTraits{
    public function checkSignals($signal, $bitmex=false) {
        if(empty($bitmex)) $bitmex= $this->getBitMex($signal->bot->api_id, $signal->bot->currency_pair);
        $order= $bitmex->getOrder($signal->order_id);
        if($order) {
            $this->createBotSignal($order, $signal->bot, $signal);
            if($order['ordStatus']=='Filled') {
                $this->signalFilled($signal->bot, $bitmex);
            }
        }

        return true;
    }
    public function searchSignals($searchValue, $bot_id, $post= false) {
        $items= BotSignals::where('bot_id', '=', $bot_id)->where(function ($query) use ($searchValue){
            $query->where('id', '=', $searchValue)
                ->orwhere('count', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('order_id', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('order_qty', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('cum_qty', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('price', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('side', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('order_type', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('order_status', 'LIKE', '%' . $searchValue . '%');
        });
        if($post) {
            if($post['order'][0]['dir']=='asc') {
                $items= $items->oldest($post['table'][$post['order'][0]['column']]);
            } else {
                $items= $items->latest($post['table'][$post['order'][0]['column']]);
            }
            $data= [
                'total'=> $items->count(),
                'data'=> $items->skip($post['start'])->take($post['length'])->get()
            ];
            return $data;
        } else {
            return $items->get();
        }
    }

    public function ajaxTableSignals($post, $bot_id) {
        $post['table']= [
            'id', 'count', 'order_id', 'order_qty', "cum_qty", "price", "side", 'order_type', "order_status"
        ];
        $items= $this->searchSignals($post['search']['value'], $bot_id, $post);
        $data= array();
        foreach ($items['data'] as $item) {
            switch ($item->order_status) {
                case 'Filled':
                    $status= "<span class='label label-success'>".$item->order_status."</span>";
                    break;
                case 'PartiallyFilled':
                case 'New':
                    $status= "<span class='label label-info'>".$item->order_status."</span>";
                    break;
                default:
                    $status= "<span class='label label-default'>".$item->order_status."</span>";
            }
            switch ($item->order_type) {
                case 'Take Profit':
                    $type= "<span class='text-success'>".$item->order_type."</span>";
                    break;
                case 'Stop Loss':
                    $type= "<span class='text-danger'>".$item->order_type."</span>";
                    break;
                default:
                    $type= "<span class='text-default'>".$item->order_type."</span>";
            }

            $data[]= [
                $item->id,
                $item->count,
                $item->order_id,
                $item->order_qty,
                $item->cum_qty,
                $item->price,
                "<span class='".(($item->side=='Sell') ? 'text-danger' : 'text-success')."'>".$item->side."</span>",
                $type,
                "<div class='text-right'>".$status."</div>",
            ];
        }
//        if(isset($post['order'][0]['dir'])) {
//            if($post['order'][0]['dir']=='asc') {
//                $data= $data->sortByDesc(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            } else {
//                $data= $data->sortBy(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            }
//        };
        ## Response
        return array(
            "iTotalRecords" => $items['total'],
            "iTotalDisplayRecords" => $items['total'],
            "aaData" => $data
        );
    }

    public function startBot($id, $bitmex= false) {
        $bot= $this->getBot($id);
        if(isset($bot) && ($bot->active==2 || $bot->active==4)) {
            $bot->start_date = Carbon::now()->addSeconds(30)->format('Y-m-d H:i:s');
            $bot->save();
            if(empty($bitmex)) $bitmex= $this->getBitMex($bot->api_id, $bot->currency_pair);
            $position= collect($bitmex->getOpenPositions());
            $position= $position->where('symbol', $bot->currency_pair)->first();
            if($position['currentQty']!= 0 && $bot->check_position!= 2) {
                $data= [
                    'mess'=>'Ошибка выставления сетки. Открытыя позиция '.$bot->currency_pair,
                    'status'=> 'error',
                ];
            } else {
                $bitmex->cancelAllOpenOrders();
                $price= $bitmex->getTicker()['last'];
                if(isset($bot->points)) {
                    $points= $bot->points;
                } else {
                    $points= 1;
                }
                $priceSell= $price;
                $priceBuy= $price;

                $params= array();
                $amount= $bot->amount;

                foreach (json_decode($bot->steps) as $key=> $step) {
                    if(empty($step->points)) continue;
                    $multiplier= ($step->multiplier) ? $step->multiplier : 1;
                    if($key>0) {
                        $amount= $multiplier*$amount;
                    }
                    if(in_array($bot->position, ['All', 'Short'])) {
                        $priceBuy= $priceBuy-($step->points*$points);
                        $params[]= [
                            "symbol" => $bot->currency_pair,
                            "side" => 'Buy',
                            "price" => $priceBuy,
                            "orderQty" => $amount,
                            "ordType" => 'Limit',
                            "execInst" => 'ParticipateDoNotInitiate',
                        ];
//                        if(empty($responseSell)) {
//                            $price= $bitmex->getTicker()['last'];
//                            if($priceBuy > $price) {
//                                $priceBuy= $price;
//                                $priceSell= $price;
//                                $priceBuy= $priceBuy-(4*$points);
//                            }
//                            $responseSell= $bitmex->createOrder('Limit','Buy', $priceBuy, $amount);
//                            if(empty($responseSell)) {
//
//                                $data= [
//                                    'mess'=>'Ошибка выставления ордеров на покупку. Step '.$key,
//                                    'status'=> 'error',
//                                ];
//                                break;
//                            }
//                        }
//                        $signal= $this->createBotSignal($responseSell, $bot);
                    }
                    if(in_array($bot->position, ['All', 'Long'])) {
                        $priceSell= $priceSell+($step->points*$points);
                        $params[]= [
                            "symbol" => $bot->currency_pair,
                            "side" => 'Sell',
                            "price" => $priceSell,
                            "orderQty" => $amount,
                            "ordType" => 'Limit',
                            "execInst" => 'ParticipateDoNotInitiate',
                        ];
//                        if(empty($responseBuy)) {
//                            $price= $bitmex->getTicker()['last'];
//                            if($priceSell < $price) {
//                                $priceBuy= $price;
//                                $priceSell= $price;
//                                $priceSell= $priceSell+(4*$points);
//                            }

//                            $responseBuy= $bitmex->createOrder('Limit','Sell', $priceSell, $amount);
//
//                            if(empty($responseBuy)) {
//                                $data = [
//                                    'mess' => 'Ошибка выставления ордера на продажу. Step ' . $key,
//                                    'status' => 'error',
//                                ];
//                                break;
//                            }
//                        }
//                        $signal= $this->createBotSignal($responseBuy, $bot);
                    }
                }
                $responses= $bitmex->createOrders($params);
                if(isset($responses['error'])) {
                    $this->telegramNotification(json_encode($responses['error']));
                    $data = [
                        'mess' => $responses['error']['message'],
                        'status' => 'error',
                    ];
                } elseif(empty($responses)) {
                    $data = [
                        'mess' => 'Сетка не была выставлена!',
                        'status' => 'error',
                    ];
                } else {
                    foreach ($responses as $response) {
                        $signal= $this->createBotSignal($response, $bot);
                    }
                }
            }
            if(empty($data)) {
                $bot->liquidation_price= null;
                $bot->avg_price= null;
                $bot->current_qty= null;
                $bot->active= 1;
                $bot->account= $bitmex->getWallet()['account'];
                $bot->save();
                $data= [
                    'mess'=> 'Сетка ордеров успешно выставлена',
                    'status'=> 'success',
                ];
                $filed= $this->getSignal([['bot_id', $bot->id], ['count', $bot->count], ['status', 1]]);
                if(isset($filed)) {
                    $this->signalFilled($bot,$bitmex);
                }
            } else {
                $mess= "<b>Notification!</b>\n";
                $mess.= "<b>".hex2bin('F09F8694')." Bot ID</b>: /b".$bot->id."\n";
                $mess.= "<b>Name</b>: ".(($bot->name)? $bot->name : $bot->currency_pair)." Count: ".$bot->count."\n";
                $mess.= "<b>Status</b>: ".$data['status'].". ".$data['mess']."\n";
                $bitmex->cancelAllOpenOrders();
//                $bitmex->closePosition();
//                if($bot->active==2 && $position['currentQty']== 0) {
//                    $bot->active= 4;
//                    $bot->count = $bot->count + 1;
//                    $bot->start_date = Carbon::now()->format('Y-m-d H:i:s');
//                    $mess.= "<b>Message</b>: робот будет перезапущен автоматически!";
//                } else {
//                    $bot->active= 0;
//                    $mess.= "<b>Message</b>: робот был остановлен!";
//                }
                if(isset($responses['error']) && $responses['error']['message']== 'The system is currently overloaded. Please try again later.') {
                    $bot->active= 2;
                    $bot->count= $bot->count+1;
                    $bot->start_date = Carbon::now()->format('Y-m-d H:i:s');
                    $mess.= "<b>Message</b>: Робот будет перезапущен!";
                } else {
                    $bot->active= 0;
                    $mess.= "<b>Message</b>: робот был остановлен!";
                }
                $this->sendMess($bot->user_id, $mess);
                $this->telegramNotification($mess);
                $bot->save();
            }
            return $data;
        } else {
            $mess= "<b>Notification!</b>\n";
            $mess.= "<b>".hex2bin('F09F8694')." Bot ID</b>: /b".$bot->id."\n";
            $mess.= "<b>Name</b>: ".(($bot->name)? $bot->name : $bot->currency_pair)." Count: ".$bot->count."\n";
            $mess.= "<b>Status</b>: Error. Ошибка выставления ордеров";
            $this->sendMess($bot->user_id, $mess);
            return [
                'mess'=>'Ошибка выставления ордеров',
                'status'=> 'error',
            ];
        }
    }
    public function newBotSignal($bot, $side, $type) {
        $signal= new BotSignals;

        $signal->api_id= $bot->api_id;
        $signal->user_id= $bot->user_id;
        $signal->bot_id= $bot->id;
        $signal->count= $bot->count;
        $signal->side= $side;
        $signal->order_type= $type;
        $signal->status= 0;

        $signal->save();
        return $signal;
    }
    public function createBotSignal($response, $bot, $signal= false, $type= false, $bitmex=false) {
        if(empty($signal)) {
            $signal= new BotSignals;
            $signal->count= $bot->count;
            if($type) {
                $signal->order_type= $type;
            } else {
                $signal->order_type= $response['ordType'];
            }
            $new= true;
        } else {
            if(empty($response['orderID'])) {
                $response= collect($response);
                $response= $response->where('orderID', $signal->order_id)->first();
            }
            $new= false;
        }
        if(empty($signal->side)) {
            $signal->side= $response['side'];
        }
        if(isset($response['error'])) {
            $mess= "Error Create Bot signal ID";
            $mess.="\nBot Id: ".$signal->bot_id."\n";
            $mess.="\nCount: ".$signal->count."\n";
            $mess.="\nSignal Id: ".$signal->id."\n";
            $mess.="\nOrder Id: ".$signal->order_id."\n";
            $mess.="\nError: ".json_encode($response['error'])."\n";
            $this->telegramNotification($mess);
        } elseif(empty($response)) {
            return false;
        }
        $signal->api_id= $bot->api_id;
        $signal->user_id= $bot->user_id;
        $signal->bot_id= $bot->id;

        $signal->order_id= $response['orderID'];
        if(isset($response['price'])) {
            $signal->price= $response['price'];
        } elseif(isset($response['stopPx'])) {
            $signal->price= $response['stopPx'];
        }
        $signal->account= $response['account'];
        if(isset($response['orderQty'])) {
            $signal->order_qty= $response['orderQty'];
        }
        if(isset($response['leavesQty'])) {
            $signal->leaves_qty= $response['leavesQty'];
        }
        if(isset($response['cumQty'])) {
            $signal->cum_qty= $response['cumQty'];
        }
        if(isset($response['ordStatus'])) {
            switch ($response['ordStatus']) {
                case 'Filled':
                    $signal->status= 1;
                    if(isset($response['avgPx'])) {
                        $signal->price= $response['avgPx'];
                    }
                    break;
                case 'Canceled':
                case 'Rejected':
                    $signal->status= 2;
                    break;
                default:
                    $signal->status= 0;
            }

            $signal->order_status = $response['ordStatus'];
        }
        $signal->save();

        if($new && $signal->order_type=='Take Profit') {
            if(empty($bitmex))  $bitmex= $this->getBitMex($signal->bot->api_id, $signal->bot->currency_pair);
            if($signal->order_status== 'Filled') {
                $response= $this->clouseOpenSignals($signal->bot, $bitmex);

                $signal->bot->start_date= Carbon::now()->addSeconds(5)->format('Y-m-d H:i:s');
                $signal->bot->active= 3;
                $signal->bot->save();
            } elseif($signal->order_status== 'PartiallyFilled') {
                $this->updatePosition(false, $signal->bot, $bitmex);
//                if($signal->cum_qty>0) {
//                    $this->updateBotBalances($signal);
//                }
            }
        }
        return $signal;
    }
    public function getSignals($id, $user_id=false, $where=false) {
        if(empty($user_id)) $user_id= Auth::id();
        $bot= $this->getBot($id, $user_id);
        $signals= BotSignals::where([
            ['bot_id', $id],
            ['user_id', $user_id],
            ['count', $bot->count],
        ]);
        if($where) $signals= $signals->where($where);
        return $signals->get();
    }
    public function getBotSignals($id, $user_id, $where= false) {

        $signals= BotSignals::where([
            ['bot_id', $id],
            ['user_id', $user_id],
        ]);
        if($where) $signals= $signals->where($where);
        return $signals->get();

    }
    public function getSignal($where) {
        return BotSignals::where($where)->first();
    }
    public function updateSignal($orders) {
        foreach ($orders->data as $data) {
            $signal= $this->getSignal([['order_id', '=', $data->orderID]]);
            if(isset($signal)) {
                $bitmex=  $this->getBitMex($signal->bot->api_id, $signal->bot->currency_pair);
                switch ($signal->order_type) {
                    case 'Take Profit':
                    case 'Stop Loss':
                        $this->checkLimits(collect($data)->all(), $signal, $bitmex);
                        break;
                    default:
//                        $response= $bitmex->getOrder($signal->order_id);
                        if($data) {
                            $signal= $this->createBotSignal(collect($data)->all(), $signal->bot, $signal);
                            if($signal->order_status=='Filled') {
                                $this->signalFilled($signal->bot, $bitmex);
                            }
//                            if($signal->order_status=='Filled') {
//                                $this->signalFilled($signal, $bitmex);
//                            } elseif($signal->order_status=='PartiallyFilled') {
//                                $orderTP= $this->getSignals($signal->bot_id, $signal->user_id, [['order_type', '=', 'Take Profit']])->first();
//                                if(isset($orderTP)) {
//                                    $this->updatePosition($signal->bot_id, $bitmex);
//                                }
//                            }
                        } else {
                            $mess= "Error Order ID";
                            $mess.="\nBot Id: ".$signal->bot_id."\n";
                            $mess.="\nSignal Id: ".$signal->id."\n";
                            $mess.="\nCount: ".$signal->count."\n";
                            $mess.="\nOrder Id: ".$signal->order_id."\n";
                            dump($mess);
                            $this->telegramNotification($mess);
                        }
                }
            } else {
                $bot= $this->accountBotSettings($data->account, $data->symbol);
                if($bot) {
                    $signal= $this->createBotSignal(collect($data)->all(), $bot);
                    $this->createStopLoss($bot->id);
                }
            }
        }
    }
    public function clouseSignals($qty, $bot, $bitmex=false) {
        if(empty($bitmex)) $bitmex= $this->getBitMex($bot->api_id, $bot->currency_pair);
        if($qty > 0) {
            $signals= $this->getSignals($bot->id, $bot->user_id, [['side', '=','Sell'], ['status', '=',0], ['order_type', '=','Limit']]);
        } else {
            $signals= $this->getSignals($bot->id, $bot->user_id, [['side', '=','Buy'], ['status', '=',0], ['order_type', '=','Limit']]);
        }
        if(count($signals)) {
            $response= $this->clouseOrders($signals, $bot->id, $bitmex);
            if(empty($response)) {
                $mess= "Error clouse Order";
                $mess.="\nBot Id: ".$bot->id."\n";
                $mess.="\nCount: ".$bot->count."\n";

//                $this->telegramNotification($mess);
                dump($mess);
            }
        }
        return true;
    }

    public function clouseOrders($signals, $id, $bitmex=false) {
        $bot= $this->getBot($id);

        if(empty($bitmex)) $bitmex= $this->getBitMex($id, $bot->currency_pair);
        foreach ($signals as $signal) {
            $response= $bitmex->cancelOrder($signal->order_id);
            if(empty($response['error'])) {
                $this->createBotSignal($response, $signal->bot, $signal);
            } else {
                $mess= "Error Cancel Order";
                $mess.="\nBot Id: ".$signal->bot_id."\n";
                $mess.="\nCount: ".$signal->count."\n";
                $mess.="\nSignal Id: ".$signal->id."\n";
                $mess.="\nOrder Id: ".$signal->order_id."\n";
                $mess.="\nError: ".json_encode($response['error'])."\n";

                $this->telegramNotification($mess);
                dump($mess);
            }
        }
        return true;
    }

    public function createStopLoss($bot_id, $bitmex=false) {
//        $openSignals= $this->getSignals($signal->bot_id, $signal->user_id, [['order_type', '=', 'Limit'],['status','=',1]]);
//        if(count($openSignals)> 0) {
        $bot= $this->getBot($bot_id);
        if(empty($bitmex)) $bitmex= $this->getBitMex($bot->api_id, $bot->currency_pair);
        //        $price= $signal->price;
        //        $points= $this->getPoints($price, $signal->bot->length);
        if(isset($bot->points)) {
            $points= $bot->points;
        } else {
            $points= 1;
        }

        $position= collect($bitmex->getOpenPositions());
        $position= $position->where('symbol', $bot->currency_pair)->first();
        if($position['currentQty']!=0) {
            $this->clouseSignals($position['currentQty'], $bot, $bitmex);

            $signals= $this->getSignals($bot_id,$bot->user_id, [['order_type', '=', 'Limit']]);
            if(count($signals)) {
                if(count($signals->where('status', 0))== 0) {
                    $summ= abs($position['currentQty']);
                    if($position['currentQty'] > 0) {
                        $price= $signals->where('order_type', 'Limit')->sortBy('price')->first()->price;
                        $stop_loss= $price- ($bot->stop_loss*$points);
                        $type= 'Sell';
                    } else {
                        $price= $signals->where('order_type', 'Limit')->sortByDesc('price')->first()->price;
                        $stop_loss= $price+ ($bot->stop_loss*$points);
                        $type= 'Buy';
                    }
                } elseif($position['currentQty'] > 0) {
                    $summBuy= $signals->where('order_type', 'Limit')->where('side', 'Buy')->where('status','!=', 2)->sum('order_qty');
                    $summSell= $signals->where('side', 'Sell')->sum('cum_qty');
                    $summ= $summBuy-$summSell;
                    $price= $signals->where('order_type', 'Limit')->sortBy('price')->first()->price;
                    $stop_loss= $price- ($bot->stop_loss*$points);
                    $type= 'Sell';
                } else {
                    $summSell= $signals->where('order_type', 'Limit')->where('side', 'Sell')->where('status','!=', 2)->sum('order_qty');
                    $summBuy= $signals->where('side', 'Buy')->sum('cum_qty');
                    $summ= $summSell-$summBuy;

                    $price= $signals->where('order_type', 'Limit')->sortByDesc('price')->first()->price;
                    $stop_loss= $price+ ($bot->stop_loss*$points);
                    $type= 'Buy';
                }
                $orderSL= $this->getSignals($bot->id, $bot->user_id, [['order_type', '=','Stop Loss'], ['side', '=', $type], ['status','=', 0]])->first();
                if(empty($orderSL)) {
                    $response= $bitmex->createStopMarketOrder($summ, $stop_loss, $type);
                } elseif($orderSL->order_qty != $summ || $orderSL->price != $stop_loss) {
                    $response= $bitmex->editStopMarketQty($orderSL->order_id, $summ, $stop_loss);
                } else {
                    $response= false;
                }
                if(empty($response['error'])) {
                    if(empty($orderSL)) {
                        return $this->createBotSignal($response, $bot, false,'Stop Loss');
                    } else {
                        return $this->createBotSignal($response, $bot, $orderSL,'Stop Loss');
                    }
                } else {
                    $mess= "Error Stop Market";
                    $mess.="\nSumm: ".$summ."\n";
                    $mess.="\nType: ".$type."\n";
                    $mess.="\nPrice: ".$stop_loss."\n";
                    $mess.="\nCount: ".$bot->count."\n";
                    $mess.="\nCurrent Qtyy: ".$summ."\n";
                    $mess.="\nError: ".$response['error']['message']."\n";

                    $this->telegramNotification($mess);
                }
            }
        } else {
            $signals= $this->getSignals($bot_id, $bot->user_id, [['status', 1],['order_status','Limit']]);
            $signalsBuy= $signals->where('side', 'Buy')->sum('cum_qty');
            $signalsSell= $signals->where('side', 'Sell')->sum('cum_qty');
            if(count($signals) && ($signalsBuy-$signalsSell)== 0) {
                $this->telegramNotification('clousePosition');
                $this->telegramNotification(json_encode($position));
                $this->clousePosition($bot);
            }
        }
//        }
    }

    public function createTakeProfit($bot, $price, $current_qty, $bitmex= false){
        if(empty($bitmex)) $bitmex= $this->getBitMex($bot->api_id, $bot->currency_pair);

//        $position= collect($bitmex->getOpenPositions());
//        $position= $position->where('symbol', $bot->currency_pair)->first();
//        $price= $position['avgCostPrice'];
        $bot= $this->getBot($bot->id);
//        $price= $bot->avg_price;

//        $points= $this->getPoints($price, $bot->length);
        switch ($bot->currency_pair) {
            case 'EOSU19':
            case 'XBTJPY':
                $price= $this::strlenNum($price);
                break;
        }
        if(isset($bot->points)) {
            $points= $bot->points;
        } else {
            $points= 1;
        }
        if($current_qty== 0) {
            return true;
        } elseif($current_qty > 0) {
            $take_profit= $price+($bot->take_profit*$points);

//            $priceLast= $bitmex->getTicker()['ask'];
//            if($priceLast < $take_profit) {
//                $take_profit=$priceLast+ (2*$points);
//            }
            $type= 'Sell';
        } else {
            $take_profit= $price-($bot->take_profit*$points);

//            $priceLast= $bitmex->getTicker()['bid'];
//            if($priceLast > $take_profit) {
//                $take_profit=$priceLast- (2*$points);
//            }
            $type= 'Buy';
        }
        $orders= $this->getSignals($bot->id, $bot->user_id, [['order_type', '=','Take Profit'],['side', '=', $type], ['status','=', 0]]);
        $orderTP= $orders->first();
        if(abs($current_qty) > $orders->sum('cum_qty') && abs($current_qty) > $orders->sum('leaves_qty')) {
            if(empty($orderTP)) {
                $response= $bitmex->createOrder('Limit',$type, $take_profit, abs($current_qty));
                $orderTP= $this->newBotSignal($bot, $type, 'Take Profit');
                if(empty($response['error']) && isset($response['account'])) {
                    $orderTP= $this->createBotSignal($response, $bot, $orderTP, 'Take Profit');
                } else {
                    $mess= "Error Create Order TP";
                    $mess.="\nBot ID: ".$bot->id."\n";
                    $mess.="\nCount: ".$bot->count."\n";
                    $mess.="\nType: ".$type."\n";
                    $mess.="\nTP: ".$take_profit."\n";
                    $mess.="\nCurrent Qty: ".abs($current_qty)."\n";
                    $mess.="\nError: ".json_encode($response['error'])."\n";
                    $this->telegramNotification($mess);
                    $orderTP->forceDelete();
                    return false;
                }
            } else {
                if(abs($current_qty) > abs($orderTP->leaves_qty) && $orderTP->cum_qty!= $orderTP->order_qty) {
                    $response= $bitmex->editOrderPrice($orderTP->order_id, $take_profit,  abs($current_qty)+ $orderTP->cum_qty);
                    if(empty($response['error']) && $response) {
                        $orderTP = $this->createBotSignal($response, $bot, $orderTP, 'Take Profit');
                    } else {
                        $mess = "Error edit Order TP";
                        $mess .= "\nBot ID: " . $bot->id . "\n";
                        $mess .= "\nCount: " . $bot->count . "\n";
                        $mess .= "\nOrder ID: " . $orderTP->order_id . "\n";
                        $mess .= "\nType: " . $type . "\n";
                        $mess .= "\nTP: " . $price . "\n";
                        $mess .= "\nCurrent Qty: " . (abs($current_qty) + $orderTP->cum_qty) . "\n";
                        $mess.="\nError: ".json_encode($response['error'])."\n";

                        $this->telegramNotification($mess);
                        return false;
                    }
                }
            }
        }
        return $orderTP;
    }

    public function checkLimits($order=false, $signal, $bitmex=false) {
        if(empty($bitmex)) $bitmex= $this->getBitMex($signal->bot->api_id, $signal->bot->currency_pair);
        if(empty($order)) {
            $order= $bitmex->getOrder($signal->order_id);
            $this->updatePosition(false, $signal->bot, $bitmex);
        }
        if($order) {
            $signal= $this->createBotSignal($order, $signal->bot, $signal);
            if(isset($order['ordStatus'])) {
                if($order['ordStatus']=='Filled') {
                    $response= $this->clouseOpenSignals($signal->bot, $bitmex);

                    $signal->bot->start_date= Carbon::now()->addSeconds(5)->format('Y-m-d H:i:s');
                    $signal->bot->active= 3;
                    $signal->bot->save();

//                $this->updateBotBalances($signal);
                } elseif($order['ordStatus']== 'PartiallyFilled') {
//                } else {
                    $this->createStopLoss($signal->bot_id, $bitmex);
                    $this->updatePosition(false, $signal->bot, $bitmex);


//                if($signal->cum_qty>0) {
//                    $this->updateBotBalances($signal);
//                }
                }
            }
            return $signal;
        }

        return false;
    }
    public function clouseOpenSignals($bot, $bitmex= false) {
        if(empty($bitmex)) $bitmex= $this->getBitMex($bot->api_id, $bot->currency_pair);

        $responses= $bitmex->cancelAllOpenOrders();
        if(isset($responses['error'])){
            $mess= "Error clouse signals";
            $mess.="\nBot ID: ".$bot->id."\n";
            $mess.="\nCount: ".$bot->count."\n";
            $mess.="\nError: ".json_encode($responses['error'])."\n";

//            dump($mess);
            $this->telegramNotification($mess);
        } else {
            foreach ($responses as $response) {
                $signal= $this->getSignal([['order_id', '=', $response['orderID']]]);
                $this->createBotSignal($response, $bot, $signal);
            }
        }
        return true;
    }
    public function signalFilled($bot, $bitmex= false) {
        if(empty($bitmex)) $bitmex= $this->getBitMex($bot->api_id, $bot->currency_pair);
        $bot= $this->getBot($bot->id);
//        $this->updatePosition($signal->bot_id, $bitmex);
//
        $orderSL= $this->getSignals($bot->id, $bot->user_id, [['order_type', '=','Stop Loss'], ['status','=', 0]])->first();
        if($bot->active== 1 && empty($orderSL)) {
            $this->createStopLoss($bot->id, $bitmex);
        }
    }
    public function positonQty($bot, $bitmex) {
        if(empty($bitmex)) $bitmex= $this->getBitMex($bot->api_id, $bot->currency_pair);

        $position= collect($bitmex->getOpenPositions());
        $position= $position->where('symbol', $bot->currency_pair)->first();
        return $position['currentQty'];
    }
}