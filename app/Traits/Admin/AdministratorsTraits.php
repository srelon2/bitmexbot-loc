<?php

namespace App\Traits\Admin;
use App\Models\Admin\Admin;

trait AdministratorsTraits {

    public function getAdministrators() {
        return Admin::get();
    }
    public function getAdministrator($key, $type= 'id') {
        return Admin::withTrashed()->where($type, $key)->first();
    }

    public function searchAdministrators($searchValue , $post= false, $table= false) {
        $roles= $this->searchRoles($searchValue)->pluck('id');
        $items= Admin::where(function ($query) use ($searchValue, $roles){
            $query->where('id', '=', $searchValue)
                ->orwhere('name', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('email', 'LIKE', '%' . $searchValue . '%')
                ->orwhereIn('role_id', $roles);
        });
        if($post) {
            if($post['order'][0]['dir']=='asc') {
                $items= $items->oldest($post['table'][$post['order'][0]['column']]);
            } else {
                $items= $items->latest($post['table'][$post['order'][0]['column']]);
            }
            $data= [
                'total'=> $items->count(),
                'data'=> $items->skip($post['start'])->take($post['length'])->get()
            ];
            return $data;
        } else {
            return $items->get();
        }
    }

    public function ajaxTableAdministrators($post) {
        $post['table']= [
            'id', 'name', 'email', 'role_id',
        ];
        $items= $this->searchAdministrators($post['search']['value'], $post);
        $accessRole=$this->accessesRoles($this->admin, 'AdminRolesController')['view'];

        $data= array();
        foreach ($items['data'] as $item) {
            $data[]= [
                $item->id,
                $item->name,
                $item->email,
                (($accessRole) ? "<a target='_blank' href=".route('admin.roles.info', ['id'=> $item->role_id])." class='btn btn-info btn-sm'>".$item->role->role."</a>" : $item->role->role),
                "<div class='pull-right'>
                    <a href='#' class='btn btn-danger btn-sm sa-warning' data-id='".$item->id."'><i class=\"fa fa-trash\"></i></a>
                    <a href=".route('admin.administrators.info', ['id'=> $item->id])." class='btn btn-primary btn-sm'><i class=\"fa fa-edit mg-r-10\"></i>Редактировать</a>
                </div>",
            ];
        }
//        if(isset($post['order'][0]['dir'])) {
//            if($post['order'][0]['dir']=='asc') {
//                $data= $data->sortByDesc(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            } else {
//                $data= $data->sortBy(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            }
//        };
        ## Response
        return array(
            "iTotalRecords" => $items['total'],
            "iTotalDisplayRecords" => $items['total'],
            "aaData" => $data
        );
    }

    public function updateAdministrator($data, $id) {
        if($id) {
            $item= Admin::find($id);
            $mess= 'изменен';
        } else {
            $item= new Admin;
            $mess= 'создан';
        }
        if(isset($data['password'])) {
            $item->password= bcrypt($data['password']);
        }
        unset($data['password']);
        foreach ($data as $key=> $value) {
            $item->$key= $value;
        }

//        $item->save()
        if($item->save()) {
            $data= [
                'mess'=>'Администратор успешно '.$mess,
                'status'=> 'success',
            ];
            $this->saveHistory($item, 'admins', (($id) ? 'Изменил' : 'Создал').' администратора ID: '.$item->id);
        } else {
            $data= [
                'mess'=>'Администратор не был '.$mess,
                'status'=> 'error',
            ];
        }
        $data['item']= $item;
        return $data;
    }

    public function openTwoStep($admin){
        if($admin->email_two_step) {
            $this->secretSend(false, 'email_two_step', $admin->id);
        }
        if($admin->telegram_two_step) {
            $this->secretSend(false, 'telegram_two_step', $admin->id);
        }

        return true;
    }
    public function actionAdministrators($action, $id) {
        $item= Admin::withTrashed()->find($id);

        try {
            switch ($action) {
                case 'delete':
                    $item->delete();
                    $mess= 'Добавлено в корзину';
                    break;
                case 'forceDelete':
                    $item->forceDelete();
                    $mess= 'Удалено с базы';
                    break;
                case 'view':
                    if($item->view) {
                        $item->view= 0;
                        $mess= 'Помечено как не прочитанное';
                    } else {
                        $item->view= 1;
                        $mess= 'Помечено как прочитанное';
                    }
                    break;
                default:
                    $item->restore();
                    $mess= 'Востанновлено с корзины';
            }
            $item->save();
            $data= [
                'status'=> 'success',
                'mess'=> $mess
            ];
            return $data;
        } catch(Exception $e) {
            $data= [
                'status'=> 'error',
                'mess'=> $e
            ];
            return $data;
        }
    }
}
