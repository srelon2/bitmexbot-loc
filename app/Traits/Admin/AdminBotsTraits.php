<?php

namespace App\Traits\Admin;

use App\Models\BotSettings;

trait AdminBotsTraits {

    public function getUsersBots($id, $where)
    {
        $bot = BotSettings::query();
        if ($where) {
            $bot = $bot->where($where);
        }
        return $bot->get();
    }
    public function searchUsersBot($searchValue, $post= false) {
        $items= BotSettings::where([
            ['saved', 0]
        ])->where(function ($query) use ($searchValue){
            $query->where('id', '=', $searchValue)
                ->orwhere('name', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('currency_pair', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('liquidation_price', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('avg_price', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('current_qty', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('active', 'LIKE', '%' . $searchValue . '%');
        })->get();

        $items= $items->transform(function ($item){
            $item->profit= 0;
            return  $item;
        });
        if($post) {
            if($post['order'][0]['dir']=='asc') {
                $items= $items->sortBy($post['table'][$post['order'][0]['column']]);
            } else {
                $items= $items->sortByDesc($post['table'][$post['order'][0]['column']]);
            }
            $data= [
                'total'=> $items->count(),
                'data'=> $items->slice($post['start'])->take($post['length'])
            ];
            return $data;
        } else {
            return $items;
        }
    }

    public function ajaxTableUsersBot($post) {
        $post['table']= [
            'id', 'name', 'currency_pair', 'liquidation_price', "avg_price", "current_qty", "profit", "active"
        ];
        $items= $this->searchUsersBot($post['search']['value'], $post);
        $data= array();
        foreach ($items['data'] as $item) {
            $profit= $this::printNum($item->profit);
            switch (true) {
                case ($item->profit>0):
                    $profit= "<span class='text-success'><i class='fa fa-long-arrow-up'></i> ".$profit."</span>";
                    break;
                case ($item->profit<0):
                    $profit= "<span class='text-danger'><i class='fa fa-long-arrow-down'></i> ".$profit."</span>";
                    break;
                default:
                    $profit= "<span class='text-default'>".$profit."</span>";
            }
            $data[]= [
                $item->id,
                $item->name,
                $item->currency_pair,
                $item->liquidation_price,
                $item->avg_price,
                $item->current_qty,
                $profit,
                "<div class='text-center'>".(($item->active) ? "<span class='label label-success'>Активный</span>" : "<span class='label label-danger'>Не активный</span>")."</div>",
                "<div class='pull-right'>
                    <a href='#' class='btn btn-danger btn-sm sa-warning' data-id='".$item->id."'><i class=\"fa fa-trash\"></i></a>
                    <a href=".route('admin.bots.info', ['id'=> $item->id])." class='btn btn-primary btn-sm'>Подробнее</a>
                </div>",
            ];
        }
//        if(isset($post['order'][0]['dir'])) {
//            if($post['order'][0]['dir']=='asc') {
//                $data= $data->sortByDesc(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            } else {
//                $data= $data->sortBy(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            }
//        };
        ## Response
        return array(
            "iTotalRecords" => $items['total'],
            "iTotalDisplayRecords" => $items['total'],
            "aaData" => $data
        );
    }

}