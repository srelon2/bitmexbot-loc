<?php

namespace App\Traits\Admin;
use App\Models\Admin\Controllers;
use App\Models\Admin\Roles;

trait RolesTraits {

    public function getRoles() {
        return Roles::get();
    }
    public function getRole($key, $type= 'id') {
        $item= Roles::where($type, $key)->first();
        $accesses= json_decode($item->accesses_id);
        $item->view= collect($accesses->view);
        $item->edit= collect($accesses->edit);
        return $item;
    }

    public function searchRoles($searchValue , $post= false, $table= false) {
        $items= Roles::where(function ($query) use ($searchValue){
            $query->where('id', '=', $searchValue)
                ->orwhere('role', 'LIKE', '%' . $searchValue . '%');
        });
        if($post) {
            if($post['order'][0]['dir']=='asc') {
                $items= $items->oldest($post['table'][$post['order'][0]['column']]);
            } else {
                $items= $items->latest($post['table'][$post['order'][0]['column']]);
            }
            $data= [
                'total'=> $items->count(),
                'data'=> $items->skip($post['start'])->take($post['length'])->get()
            ];
            return $data;
        } else {
            return $items->get();
        }
    }

    public function ajaxTableRoles($post) {
        $post['table']= [
            'id', 'role',
        ];
        $items= $this->searchRoles($post['search']['value'], $post);
        $data= array();
        foreach ($items['data'] as $item) {
            $data[]= [
                $item->id,
                $item->role,
                "<div class='pull-right'>
                    <a href='#' class='btn btn-danger btn-sm sa-warning' data-id='".$item->id."'><i class=\"fa fa-trash\"></i></a>
                    <a href=".route('admin.roles.info', ['id'=> $item->id])." class='btn btn-primary btn-sm'><i class=\"fa fa-edit mg-r-10\"></i>Редактировать</a>
                </div>"];
        }
//        if(isset($post['order'][0]['dir'])) {
//            if($post['order'][0]['dir']=='asc') {
//                $data= $data->sortByDesc(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            } else {
//                $data= $data->sortBy(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            }
//        };
        ## Response
        return array(
            "iTotalRecords" => $items['total'],
            "iTotalDisplayRecords" => $items['total'],
            "aaData" => $data
        );
    }

    public function updateRole($data, $id) {
        if($id) {
            $item= Roles::find($id);
            $mess= 'изменена';
        } else {
            $item= new Roles;
            $mess= 'создана';
        }
        $item->role= $data['role'];
        unset($data['role']);
        $item->accesses_id= json_encode($data);

//        $item->save()
        if($item->save()) {
            $data= [
                'mess'=>'Роль успешно '.$mess,
                'status'=> 'message',
            ];
            $this->saveHistory($item, 'roles', (($id) ? 'Изменил' : 'Создал').' роль ID: '.$item->id);
        } else {
            $data= [
                'mess'=>'Роль не была '.$mess,
                'status'=> 'error',
            ];
        }
        $data['item']= $item;
        return $data;

    }

    public function getController($key, $type= 'name') {
        return Controllers::where([
            [$type, '=', $key],
        ])->first();
    }


    public function accessesRoles($admin, $key) {
        $data['accesses']= $key;
        $data['view']= false;
        $data['edit']= false;

        $controller= $this->getController($key);
        if(isset($controller)) {
            $role= $this->getRole($admin->role_id);

            $data['view']= $role->view->contains($controller->id);
            $data['edit']= $role->edit->contains($controller->id);
        }

        return $data;
    }

    public function actionRoles($action, $id) {
        $item= Roles::withTrashed()->find($id);

        try {
            switch ($action) {
                case 'delete':
                    $item->delete();
                    $mess= 'Добавлено в корзину';
                    break;
                case 'forceDelete':
                    $item->forceDelete();
                    $mess= 'Удалено с базы';
                    break;
                case 'view':
                    if($item->view) {
                        $item->view= 0;
                        $mess= 'Помечено как не прочитанное';
                    } else {
                        $item->view= 1;
                        $mess= 'Помечено как прочитанное';
                    }
                    break;
                default:
                    $item->restore();
                    $mess= 'Востанновлено с корзины';
            }
            $item->save();
            $data= [
                'status'=> 'success',
                'mess'=> $mess
            ];
            return $data;
        } catch(Exception $e) {
            $data= [
                'status'=> 'error',
                'mess'=> $e
            ];
            return $data;
        }
    }
}


//['title' => 'Артефакты',
//    'url' => Roles('admin.home'),
//    'accessLogic' => function ()
//    {
//        return auth()->guard('admin')->user()->accessesRoles('UserVerification');
//    },],