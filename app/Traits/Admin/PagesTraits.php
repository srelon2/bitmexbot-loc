<?php

namespace App\Traits\Admin;

use App\Models\Pages;
use Illuminate\Support\Facades\Auth;

trait PagesTraits {



    public function getPages() {
        return Pages::get();
    }
    public function getPage($key, $type= 'id') {
        return Pages::withTrashed()->where($type, $key)->first();
    }

    public function searchPages($searchValue , $post= false, $table= false) {
        $items= Pages::where(function ($query) use ($searchValue){
            $query->where('id', '=', $searchValue)
                ->orwhere('url', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('title_ru', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('content_ru', 'LIKE', '%' . $searchValue . '%');
        });
        if($post) {
            if($post['order'][0]['dir']=='asc') {
                $items= $items->oldest($post['table'][$post['order'][0]['column']]);
            } else {
                $items= $items->latest($post['table'][$post['order'][0]['column']]);
            }
            $data= [
                'total'=> $items->count(),
                'data'=> $items->skip($post['start'])->take($post['length'])->get()
            ];
            return $data;
        } else {
            return $items->get();
        }
    }

    public function ajaxTablePages($post) {
        $post['table']= [
            'id', 'url', 'title_ru'
        ];
        $items= $this->searchPages($post['search']['value'], $post);
        $accessRole=$this->accessesRoles($this->admin, 'AdminRolesController')['view'];

        $data= array();
        foreach ($items['data'] as $item) {
            $data[]= [
                $item->id,
                $item->url,
                $item->title_ru,
                "<div class='pull-right'>
                    <a href='#' class='btn btn-danger btn-sm sa-warning' data-id='".$item->id."'><i class=\"fa fa-trash\"></i></a>
                    <a href=".route('admin.pages.info', ['id'=> $item->id])." class='btn btn-primary btn-sm'><i class=\"fa fa-edit mg-r-10\"></i>Редактировать</a>
                </div>",
            ];
        }
//        if(isset($post['order'][0]['dir'])) {
//            if($post['order'][0]['dir']=='asc') {
//                $data= $data->sortByDesc(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            } else {
//                $data= $data->sortBy(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            }
//        };
        ## Response
        return array(
            "iTotalRecords" => $items['total'],
            "iTotalDisplayRecords" => $items['total'],
            "aaData" => $data
        );
    }

    public function updatePage($data, $id) {
        if($id) {
            $item= Pages::find($id);
            $mess= 'изменена';
        } else {
            $item= new Pages;
            $mess= 'создана';
        }
        if(empty($data['url'])) {
            $item->url= $this->translitSrt($data['title_ru']);
        } else {
            $item->url= $this->translitSrt($data['url']);
        }
        unset($data['url']);
        foreach ($data as $key=> $value) {
            $item->$key= $value;
        }

//        $item->save()
        if($item->save()) {
            $data= [
                'mess'=>'Страница успешно '.$mess,
                'status'=> 'success',
            ];
            $this->saveHistory($item, 'pages', (($id) ? 'Изменил' : 'Создал').' страницу ID: '.$item->id);
        } else {
            $data= [
                'mess'=>'Страница не была '.$mess,
                'status'=> 'error',
            ];
        }
        $data['item']= $item;

        if(empty($id)) {
            $str= '';
            foreach (collect($item)->except(['updated_at', 'created_at', 'deleted_at']) as $key=> $value) {
                $str.="            '".$key."'=> '".$value."',\r\n";
            }
            $seeds= file(database_path('/seeds/PagesSeeder.php'));
            $seeds[count($seeds)-4].='        $data[]= ['."\r\n".$str."        ];\r\n";
            file_put_contents( database_path('/seeds/PagesSeeder.php'), $seeds );
        };

        return $data;
    }

    public function actionPages($action, $id) {
        $item= Pages::withTrashed()->find($id);

        try {
            switch ($action) {
                case 'delete':
                    $item->delete();
                    $mess= 'Добавлено в корзину';
                    break;
                case 'forceDelete':
                    $item->forceDelete();
                    $mess= 'Удалено с базы';
                    break;
                case 'view':
                    if($item->view) {
                        $item->view= 0;
                        $mess= 'Помечено как не прочитанное';
                    } else {
                        $item->view= 1;
                        $mess= 'Помечено как прочитанное';
                    }
                    break;
                default:
                    $item->restore();
                    $mess= 'Востанновлено с корзины';
            }
            $item->save();
            $data= [
                'status'=> 'success',
                'mess'=> $mess
            ];
            return $data;
        } catch(Exception $e) {
            $data= [
                'status'=> 'error',
                'mess'=> $e
            ];
            return $data;
        }
    }
}
