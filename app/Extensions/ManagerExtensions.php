<?php

namespace App\Extensions;

use \Illuminate\Session\SessionManager;

class ManagerExtensions extends SessionManager {

    protected $app;
    protected $table;

    public function __construct($app, $table)
    {
        $this->app = $app;
        $this->table = $table;
    }

    protected function createDatabaseDriver()
    {
        $table = $this->table;
        $lifetime = $this->app['config']['session.lifetime'];
        return $this->buildSession(new DatabaseSessionHandler(
            $this->getDatabaseConnection(), $table, $lifetime, $this->app
        ));
    }
}