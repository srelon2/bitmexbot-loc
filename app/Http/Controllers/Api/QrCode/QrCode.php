<?php

declare(strict_types=1);

/*
 * (c) Jeroen van den Enden <info@endroid.nl>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Http\Controllers\Api\QrCode;

use BaconQrCode\Encoder\Encoder;

class QrCode implements QrCodeInterface
{
    const LABEL_FONT_PATH_DEFAULT = __DIR__ . '/../assets/fonts/noto_sans.otf';

    private $text;

    private $size = 245;
    private $margin = 5;

    private $foregroundColor = [
        'r' => 42,
        'g' => 45,
        'b' => 56,
        'a' => 0,
    ];

    private $backgroundColor = [
        'r' => 255,
        'g' => 255,
        'b' => 255,
        'a' => 0,
    ];

    private $primaryColor = [
        'r' => 2,
        'g' => 193,
        'b' => 116,
        'a' => 0,
    ];

    private $encoding = 'UTF-8';
    private $roundBlockSize = true;
    private $errorCorrectionLevel;

    private $logoPath;
    private $logoWidth;
    private $logoHeight;

    private $label;
    private $labelFontSize = 16;
    private $labelFontPath = self::LABEL_FONT_PATH_DEFAULT;
    private $labelAlignment;
    private $labelMargin = [
        't' => 0,
        'r' => 10,
        'b' => 10,
        'l' => 10,
    ];

    private $writerRegistry;
    private $writer;
    private $writerOptions = [];
    private $validateResult = false;

    public function __construct(string $text = '')
    {
        $this->text = $text;

        $this->errorCorrectionLevel = new ErrorCorrectionLevel(ErrorCorrectionLevel::LOW);
        $this->labelAlignment = new LabelAlignment(LabelAlignment::CENTER);
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setSize(int $size): void
    {
        $this->size = $size;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function setMargin(int $margin): void
    {
        $this->margin = $margin;
    }

    public function getMargin(): int
    {
        return $this->margin;
    }

    public function setForegroundColor(array $foregroundColor): void
    {
        if (!isset($foregroundColor['a'])) {
            $foregroundColor['a'] = 0;
        }

        foreach ($foregroundColor as &$color) {
            $color = intval($color);
        }

        $this->foregroundColor = $foregroundColor;
    }

    public function getForegroundColor(): array
    {
        return $this->foregroundColor;
    }

    public function setBackgroundColor(array $backgroundColor): void
    {
        if (!isset($backgroundColor['a'])) {
            $backgroundColor['a'] = 0;
        }

        foreach ($backgroundColor as &$color) {
            $color = intval($color);
        }

        $this->backgroundColor = $backgroundColor;
    }
    public function setPrimaryColor(array $primaryColor): void
    {
        if (!isset($primaryColor['a'])) {
            $primaryColor['a'] = 0;
        }

        foreach ($primaryColor as &$color) {
            $color = intval($color);
        }

        $this->primaryColor = $primaryColor;
    }

    public function getBackgroundColor(): array
    {
        return $this->backgroundColor;
    }
    public function getPrimaryColor(): array
    {
        return $this->primaryColor;
    }

    public function setEncoding(string $encoding): void
    {
        $this->encoding = $encoding;
    }

    public function getEncoding(): string
    {
        return $this->encoding;
    }

    public function setRoundBlockSize(bool $roundBlockSize): void
    {
        $this->roundBlockSize = $roundBlockSize;
    }

    public function getRoundBlockSize(): bool
    {
        return $this->roundBlockSize;
    }

    public function setErrorCorrectionLevel(ErrorCorrectionLevel $errorCorrectionLevel): void
    {
        $this->errorCorrectionLevel = $errorCorrectionLevel;
    }

    public function getErrorCorrectionLevel(): ErrorCorrectionLevel
    {
        return $this->errorCorrectionLevel;
    }

    public function setLogoPath(string $logoPath): void
    {
//        $logoPath = realpath($logoPath);
//        if (!is_file($logoPath)) {
//            throw new InvalidPathException('Invalid logo path: '.$logoPath);
//        }

        $this->logoPath = $logoPath;
    }

    public function getLogoPath(): ?string
    {
        return $this->logoPath;
    }

    public function setLogoSize(int $logoWidth, int $logoHeight = null): void
    {
        $this->logoWidth = $logoWidth;
        $this->logoHeight = $logoHeight;
    }

    public function setLogoWidth(int $logoWidth): void
    {
        $this->logoWidth = $logoWidth;
    }

    public function getLogoWidth(): ?int
    {
        return $this->logoWidth;
    }

    public function setLogoHeight(int $logoHeight): void
    {
        $this->logoHeight = $logoHeight;
    }

    public function getLogoHeight(): ?int
    {
        return $this->logoHeight;
    }

    public function setLabel(string $label, int $labelFontSize = null, string $labelFontPath = null, string $labelAlignment = null, array $labelMargin = null): void
    {
        $this->label = $label;

        if (null !== $labelFontSize) {
            $this->setLabelFontSize($labelFontSize);
        }

        if (null !== $labelFontPath) {
            $this->setLabelFontPath($labelFontPath);
        }

        if (null !== $labelAlignment) {
            $this->setLabelAlignment($labelAlignment);
        }

        if (null !== $labelMargin) {
            $this->setLabelMargin($labelMargin);
        }
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabelFontSize(int $labelFontSize): void
    {
        $this->labelFontSize = $labelFontSize;
    }

    public function getLabelFontSize(): ?int
    {
        return $this->labelFontSize;
    }

    public function setLabelFontPath(string $labelFontPath): void
    {
        $resolvedLabelFontPath = realpath($labelFontPath);

        if (!is_string($resolvedLabelFontPath) || !is_file($resolvedLabelFontPath)) {
            throw new InvalidPathException('Invalid label font path: '.$labelFontPath);
        }

        $this->labelFontPath = $resolvedLabelFontPath;
    }

    public function getLabelFontPath(): ?string
    {
        return $this->labelFontPath;
    }

    public function setLabelAlignment(string $labelAlignment): void
    {
        $this->labelAlignment = new LabelAlignment($labelAlignment);
    }

    public function getLabelAlignment(): ?string
    {
        return $this->labelAlignment->getValue();
    }

    public function setLabelMargin(array $labelMargin): void
    {
        $this->labelMargin = array_merge($this->labelMargin, $labelMargin);
    }

    public function getLabelMargin(): ?array
    {
        return $this->labelMargin;
    }

    public function setWriterRegistry(WriterRegistryInterface $writerRegistry): void
    {
        $this->writerRegistry = $writerRegistry;
    }

    public function setWriterOptions(array $writerOptions): void
    {
        $this->writerOptions = $writerOptions;
    }

    public function getWriterOptions(): array
    {
        return $this->writerOptions;
    }

    private function createWriterRegistry()
    {
        $this->writerRegistry = new WriterRegistry();
        $this->writerRegistry->loadDefaultWriters();
    }

    public function setWriterByName(string $name)
    {
        $this->writer = $this->getWriter($name);
    }

    public function setWriterByPath(string $path): void
    {
        $extension = pathinfo($path, PATHINFO_EXTENSION);

        $this->setWriterByExtension($extension);
    }

    public function setWriterByExtension(string $extension): void
    {
        foreach ($this->writerRegistry->getWriters() as $writer) {
            if ($writer->supportsExtension($extension)) {
                $this->writer = $writer;

                return;
            }
        }

        throw new UnsupportedExtensionException('Missing writer for extension "'.$extension.'"');
    }

    public function writeDataUri(): string
    {
        return $this->getWriter()->writeDataUri($this);
    }

    public function writeFile(string $path): void
    {
        $this->getWriter()->writeFile($this, $path);
    }

    public function getContentType(): string
    {
        return $this->getWriter()->getContentType();
    }

    public function setValidateResult(bool $validateResult): void
    {
        $this->validateResult = $validateResult;
    }

    public function getValidateResult(): bool
    {
        return $this->validateResult;
    }

    public function getData(): array
    {
        $baconErrorCorrectionLevel = $this->errorCorrectionLevel->toBaconErrorCorrectionLevel();

        $baconQrCode = Encoder::encode($this->text, $baconErrorCorrectionLevel, $this->encoding);

        $matrix = $baconQrCode->getMatrix()->getArray()->toArray();
        foreach ($matrix as &$row) {
            $row = $row->toArray();
        }

        $data = ['matrix' => $matrix];
        $data['block_count'] = count($matrix[0]);
        $data['block_size'] = $this->size / $data['block_count'];
        if ($this->roundBlockSize) {
            $data['block_size'] = intval(floor($data['block_size']));
        }
        $data['inner_width'] = $data['block_size'] * $data['block_count'];
        $data['inner_height'] = $data['block_size'] * $data['block_count'];
        $data['outer_width'] = $this->size + 2 * $this->margin;
        $data['outer_height'] = $this->size + 2 * $this->margin;
        $data['margin_left'] = ($data['outer_width'] - $data['inner_width']) / 2;
        if ($this->roundBlockSize) {
            $data['margin_left'] = intval(floor($data['margin_left']));
        }
        $data['margin_right'] = $data['outer_width'] - $data['inner_width'] - $data['margin_left'];
        return $data;
    }


    public function createBaseImage($type= 'default', $size)
    {
        $data= $this->getData();
        $block_size= 7;
        $block_count= $data['block_count'];
        $image = imagecreatetruecolor($data['block_count'] * $size, $data['block_count'] * $size);
        $width= $block_count*$size;

        $foregroundColor = imagecolorallocatealpha($image, $this->getForegroundColor()['r'], $this->getForegroundColor()['g'], $this->getForegroundColor()['b'], $this->getForegroundColor()['a']);
        $backgroundColor = imagecolorallocatealpha($image, $this->getBackgroundColor()['r'], $this->getBackgroundColor()['g'], $this->getBackgroundColor()['b'], $this->getBackgroundColor()['a']);
        $primaryColor = imagecolorallocatealpha($image, $this->getPrimaryColor()['r'], $this->getPrimaryColor()['g'], $this->getPrimaryColor()['b'], $this->getBackgroundColor()['a']);
        imagefill($image, 0, 0, $backgroundColor);

        switch ($type){
            case 'ellipse':
                $this->ImageRectangleWithRoundedCorners($image, 0, 0, $size*$block_size, $size*$block_size, $size*2, $primaryColor);
                $this->ImageRectangleWithRoundedCorners($image, $size, $size, $size*6, $size*6, $size, $backgroundColor);
                $this->ImageRectangleWithRoundedCorners($image, $size*2, $size*2, $size*5, $size*5, $size, $foregroundColor);

                $this->ImageRectangleWithRoundedCorners($image, $width-($size*$block_size), 0, $width, $size*$block_size, $size*2, $primaryColor);
                $this->ImageRectangleWithRoundedCorners($image, $width-($size*6), $size, $width-$size, $size*6, $size, $backgroundColor);
                $this->ImageRectangleWithRoundedCorners($image, $width-($size*5), $size*2, $width-($size*2), $size*5, $size, $foregroundColor);

                $this->ImageRectangleWithRoundedCorners($image, 0, $width-($size*$block_size), $size*$block_size, $width, $size*2, $primaryColor);
                $this->ImageRectangleWithRoundedCorners($image, $size, $width-($size*6), $size*6, $width-$size, $size, $backgroundColor);
                $this->ImageRectangleWithRoundedCorners($image, $size*2, $width-($size*5), $size*5, $width-($size*2), $size, $foregroundColor);
            break;
        }

        foreach ($data['matrix'] as $row => $values) {
            $horizontal_t= $row <= $block_size;
            $horizontal_b= $row+1 >= ($block_count- $block_size);
            foreach ($values as $column => $value) {
                $vertical_l= $column <= $block_size;
                $vertical_r= $column+1 >= ($block_count- $block_size);


                if (1 === $value) {
                    switch ($type){
                        case 'ellipse':
                            if(!($horizontal_t && $vertical_l || $horizontal_t && $vertical_r || $horizontal_b && $vertical_l)) {
                                imagefilledellipse($image, ($column * $size)+9, ($row * $size)+9, $size, $size,$foregroundColor);
                            }
                            break;
                        default:
                            imagefilledrectangle($image, $column * $size, $row * $size, ($column + 1) * $size, ($row + 1) * $size, $foregroundColor);
                    }
                }
            }
        }
        return $image;
    }

    public function ImageRectangleWithRoundedCorners($im, $x1, $y1, $x2, $y2, $r, $color) {
        // рисуем два прямоугольника без углов
        imagefilledrectangle ($im, $x1+$r, $y1, $x2-$r, $y2, $color);
        imagefilledrectangle ($im, $x1, $y1+$r, $x2, $y2-$r, $color);
        // рисуем круги в углах
        imagefilledellipse($im, $x1+$r, $y1+$r, $r*2, $r*2, $color);
        imagefilledellipse($im, $x2-$r, $y1+$r, $r*2, $r*2, $color);
        imagefilledellipse($im, $x1+$r, $y2-$r, $r*2, $r*2, $color);
        imagefilledellipse($im, $x2-$r, $y2-$r, $r*2, $r*2, $color);

        return $im;
    }

    public function createImage($type= 'default', $baseSize= 20)
    {
        $data= $this->getData();
        $baseImage = $this->createBaseImage($type, $baseSize);
        $interpolatedImage = $this->createInterpolatedImage($baseImage, $data);

        return $interpolatedImage;
    }

    public function createInterpolatedImage($baseImage, array $data)
    {
        $image = imagecreatetruecolor($data['outer_width'], $data['outer_height']);
        $backgroundColor = imagecolorallocatealpha($image, $this->getBackgroundColor()['r'], $this->getBackgroundColor()['g'], $this->getBackgroundColor()['b'], $this->getBackgroundColor()['a']);
        imagefill($image, 0, 0, $backgroundColor);
        imagecopyresampled($image, $baseImage, (int) 0, (int) 0, 0, 0, (int) $data['outer_width'], (int) $data['outer_height'], imagesx($baseImage), imagesy($baseImage));
        imagesavealpha($image, true);

        return $image;
    }




    public function writeString($type='default', $baseSize= 20) {
        $image = $this->createImage($type, $baseSize);

        if ($this->getLogoPath()) {
            $image = $this->addLogo($image, $this->getLogoPath(), $this->getLogoWidth(), $this->getLogoHeight());
        }

//        if ($this->getLabel()) {
//            $image = $this->addLabel($image, $this->getLabel(), $this->getLabelFontPath(), $this->getLabelFontSize(), $this->getLabelAlignment(), $this->getLabelMargin(), $this->getForegroundColor(), $this->getBackgroundColor());
//        }

        $string = $this->imageToString($image);

//        if ($this->getValidateResult()) {
//            $reader = new QrReader($string, QrReader::SOURCE_TYPE_BLOB);
//            if ($reader->text() !== $this->getText()) {
//                throw new ValidationException(
//                    'Built-in validation reader read "'.$reader->text().'" instead of "'.$this->getText().'".
//                     Adjust your parameters to increase readability or disable built-in validation.'
//                );
//            }
//        }

        return $image;
    }

    public function addLogo($sourceImage, string $logoPath, int $logoWidth = null, int $logoHeight = null)
    {
        $logoImage = imagecreatefromstring(file_get_contents($logoPath));
        $logoSourceWidth = imagesx($logoImage);
        $logoSourceHeight = imagesy($logoImage);

        if (null === $logoWidth) {
            $logoWidth = $logoSourceWidth;
        }

        if (null === $logoHeight) {
            $aspectRatio = $logoWidth / $logoSourceWidth;
            $logoHeight = intval($logoSourceHeight * $aspectRatio);
        }

        $logoX = imagesx($sourceImage) / 2 - $logoWidth / 2;
        $logoY = imagesy($sourceImage) / 2 - $logoHeight / 2;

        imagecopyresampled($sourceImage, $logoImage, (int) $logoX, (int) $logoY, 0, 0, $logoWidth, $logoHeight, $logoSourceWidth, $logoSourceHeight);

        return $sourceImage;
    }

    public function imageToString($image): string
    {
        ob_start();
        imagepng($image);
        $string = ob_get_clean();

        return $string;
    }

    public static function getSupportedExtensions(): array
    {
        return ['png'];
    }

    public function getName(): string
    {
        return 'png';
    }
}
