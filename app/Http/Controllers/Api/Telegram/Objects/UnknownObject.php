<?php

namespace App\Http\Controllers\Api\Telegram\Objects;

/**
 * Class UnknownObject.
 */
class UnknownObject extends BaseObject
{
    /**
     * {@inheritdoc}
     */
    public function relations()
    {
        return [];
    }
}
