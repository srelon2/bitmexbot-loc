<?php

/*
 * BitMex PHP REST API
 *
 * @author y0un1verse
 * @version 0.1
 * @link https://github.com/y0un1verse/bitmex-api-php
 */
namespace App\Http\Controllers\Api\Bitmex;


class Bitmex {


    const API_URL = 'https://testnet.bitmex.com';
//    const API_URL = 'https://www.bitmex.com';
    const API_PATH = '/api/v1/';
    const SYMBOL = 'XBTUSD';

    private $apiKey;
    private $apiSecret;
    private $symbol;

    private $ch;

    public $error;
    public $printErrors = false;
    public $errorCode;
    public $errorMessage;

    // To use this option you will need CURL version 7.21.7 or later
    // Sample $proxy value: 'socks5://user:login@hostname:1080'
    public $proxy = false;

    /*
     * @param string $apiKey    API Key
     * @param string $apiSecret API Secret
     */

    public function __construct($apiKey = '', $apiSecret = '', $symbol= false) {
        if(empty($symbol)) $symbol= self::SYMBOL;
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
        $this->symbol= $symbol;

        if(env('APP_ENV')=='project') {
            $this->api_url = 'https://www.bitmex.com';
        } else {
            $this->api_url = 'https://testnet.bitmex.com';
        }


        $this->curlInit();

    }

    /*
     * Public
     */

    /*
     * Get Ticker
     *
     * @return ticker array
     */

    public function getTicker() {
        $data['function'] = "instrument";
        $data['params'] = array(
            "symbol" => $this->symbol
        );

        $return = $this->publicQuery($data);

        if(!$return || count($return) != 1 || !isset($return[0]['symbol'])) return false;

        $return = array(
            "symbol" => $return[0]['symbol'],
            "last" => $return[0]['lastPrice'],
            "bid" => $return[0]['bidPrice'],
            "ask" => $return[0]['askPrice'],
            "high" => $return[0]['highPrice'],
            "low" => $return[0]['lowPrice']
        );

        return $return;

    }

    /*
     * Get Candles
     *
     * Get candles history
     *
     * @param $timeFrame can be 1m 5m 1h
     * @param $count candles count
     * @param $offset timestamp conversion offset in seconds
     *
     * @return candles array (from past to present)
     */

    public function getCandles($timeFrame,$symbol ,$count,$offset = 0) {

        $data['function'] = "trade/bucketed";
        $data['params'] = array(
            "symbol" => $symbol,
            "count" => $count,
            "binSize" => $timeFrame,
            "partial" => "true",
            "reverse" => "true"
        );

        $return = collect($this->publicQuery($data))->reverse()->values()->all();
        $candles = array();
        // Converting
        foreach($return as $item) {
            foreach ($item as $key=> $value) {
                $candles[$key][]= $value;
            }
//            $time = strtotime($item['timestamp']) + $offset; // Unix time stamp
//
//            $candles[$time] = array(
//                'timestamp' => date('Y-m-d H:i:s',$time), // Local time human-readable time stamp
//                'time' => $time,
//                'open' => $item['open'],
//                'high' => $item['high'],
//                'close' => $item['close'],
//                'low' => $item['low']
//            );

        }
        // Sorting candles from the past to the present
//        ksort($candles);
        return $candles;

    }

    /*
     * Get Order
     *
     * Get order by order ID
     *
     * @return array or false
     */

    public function getOrder($orderID,$count = 500) {
        $data['method'] = "GET";
        $data['function'] = "order";
        $data['params'] = array(
            "symbol" => $this->symbol,
            "count" => $count,
            "reverse" => "true",
            "filter" => json_encode([
                'orderID'=> $orderID
            ])
        );
        $responce= $this->authQuery($data);
        if($responce && count($responce) && empty($responce['error'])) {
            return $responce[0];
        }

        return false;
    }

    public function getExection($orderID,$count = 500) {
        $data['method'] = "GET";
        $data['function'] = "execution/tradeHistory";
        $data['params'] = array(
            "symbol" => $this->symbol,
            "count" => $count,
            "reverse" => "true",
            "filter" => json_encode([
                'orderID'=> $orderID
            ])
        );
        $responces= $this->authQuery($data);
        if(count($responces)) {
            return $responces;
        }

        return [
            'error'=>[
                'name'=> 'Execution not Found',
                'message'=> "Order ID: ".$orderID." not found"
            ]
        ];
    }

    /*
     * Get Orders
     *
     * Get last 100 orders
     *
     * @return orders array (from the past to the present)
     */

    public function getOrders($count = 100) {
        $data['method'] = "GET";
        $data['function'] = "order";
        $data['params'] = array(
            "symbol" => $this->symbol,
            "count" => $count,
            "reverse" => "true"
        );

        return array_reverse($this->authQuery($data));
    }

    /*
     * Get Open Orders
     *
     * Get open orders from the last 100 orders
     *
     * @return open orders array
     */

    public function getOpenOrders() {
        $data['method'] = "GET";
        $data['function'] = "order";
        $data['params'] = array(
            "symbol" => $this->symbol,
            "reverse" => "true"
        );

        $orders = $this->authQuery($data);

        $openOrders = array();
        foreach($orders as $order) {
            if($order['ordStatus'] == 'New' || $order['ordStatus'] == 'PartiallyFilled') $openOrders[] = $order;
        }

        return $openOrders;

    }

    /*
     * Get Open Positions
     *
     * Get all your open positions
     *
     * @return open positions array
     */

    public function getOpenPositions() {
        $data['method'] = "GET";
        $data['function'] = "position";
        $data['params'] = array(
            "symbol" => $this->symbol
        );

        $positions = $this->authQuery($data);

        if (!is_array($positions))
            return false;

        $openPositions = array();
        foreach($positions as $position) {
            if(isset($position['isOpen']) && $position['isOpen'] == true) {
                $openPositions[] = $position;
            }
        }

        return $openPositions;
    }

    /*
     * Get Positions
     *
     * Get all of your positions
     *
     * @return positions array
     */

    public function getPositions() {
        $data['method'] = "GET";
        $data['function'] = "position";
        $data['params'] = array(
            "symbol" => $this->symbol
        );

        $positions = $this->authQuery($data);

        return $positions;
    }

    /*
     * Close Position
     *
     * Close open position
     *
     * @return array
     */

    public function closePricePosition($price) {
        $data['method'] = "POST";
        $data['function'] = "order/closePosition";
        if($this->symbol=='XBTUSD') {
            $price= ceil($price/0.5)*0.5;
        } elseif($this->symbol=="ETHUSD") {
            $price= ceil($price/0.05)*0.05;
        }

        $data['params'] = array(
            "symbol" => $this->symbol,
            "price" => $price
        );

        return $this->authQuery($data);
    }
    public function closePosition() {
        $data['method'] = "POST";
        $data['function'] = "order/closePosition";
        $data['params'] = array(
            "symbol" => $this->symbol,
            "ordType" => 'Market'
        );

        return $this->authQuery($data);
    }

    /*
     * Edit Order Price
     *
     * Edit you open order price
     *
     * @param $orderID    Order ID
     * @param $price      new price
     *
     * @return new order array
     */

    public function editOrderPrice($orderID,$price, $orderQty= false) {

        $data['method'] = "PUT";
        $data['function'] = "order";
        if($this->symbol=='XBTUSD') {
            $price= ceil($price/0.5)*0.5;
        } elseif($this->symbol=="ETHUSD") {
            $price= ceil($price/0.05)*0.05;
        }
        $data['params'] = array(
            "orderID" => $orderID,
            "price" => $price,
        );
        if($orderQty) {
            $data['params']['orderQty']= $orderQty;
        }

        return $this->authQuery($data);
    }

    /*
     * Create Order
     *
     * Create new order
     *
     * @param $type can be "Limit"
     * @param $side can be "Buy" or "Sell"
     * @param $price BTC price in USD
     * @param $quantity should be in USD (number of contracts)
     * @param $maker forces platform to complete your order as a 'maker' only
     *
     * @return new order array
     */

    public function createOrder($type,$side,$price,$quantity,$maker = false) {
        $data['method'] = "POST";
        $data['function'] = "order";
        if($this->symbol=='XBTUSD') {
            $price= ceil($price/0.5)*0.5;
        } elseif($this->symbol=="ETHUSD") {
            $price= ceil($price/0.05)*0.05;
        }
        $data['params'] = array(
            "symbol" => $this->symbol,
            "side" => $side,
            "price" => $price,
            "orderQty" => ceil($quantity),
            "ordType" => $type
        );

        if($maker) {
            $data['params']['execInst'] = "ParticipateDoNotInitiate";
        }

        return $this->authQuery($data);
    }
    public function createOrders($array) {
        $data['method'] = "POST";
        $data['function'] = "order/bulk";
        $data['params'] = array(
            "orders"=> json_encode($array)
        );
        return $this->authQuery($data);
    }

    /*
     * Create Limit Order
     *
     * Create new limit order
     *
     * @param $price number of contracts
     * @param $quantity (number of contracts) positive for buy and negative for sell
     * @param $maker forces platform to complete your order as a 'maker' only
     *
     * @return new order array
     */

    public function createLimitOrder($quantity,$price,$instructions = false) {
        $data['method'] = "POST";
        $data['function'] = "order";
        if($this->symbol=='XBTUSD') {
            $price= ceil($price/0.5)*0.5;
        } elseif($this->symbol=="ETHUSD") {
            $price= ceil($price/0.05)*0.05;
        }
        $data['params'] = array(
            "symbol" => $this->symbol,
            "price" => $price,
            "orderQty" => $quantity,
            "ordType" => "Limit"
        );

        if($instructions) {
            $data['params']['execInst'] = $instructions;
        }

        return $this->authQuery($data);
    }

    /*
     * Create Stop Market Order
     *
     * Create new stop market order
     *
     * @param $stopPrice BTC trigger price
     * @param $quantity should be in USD (number of contracts)
     *
     * @return new order array
     */

    public function createStopMarketOrder($quantity,$stopPrice, $side, $instructions = 'Close,LastPrice') {
        $data['method'] = "POST";
        $data['function'] = "order";
        $data['params'] = array(
            "symbol" => $this->symbol,
            "side" => $side,
            "stopPx" => $stopPrice,
            "orderQty" => $quantity,
            "ordType" => "Stop"
        );

        if($instructions) {
            $data['params']['execInst'] = $instructions;
        }

        return $this->authQuery($data);
    }


    public function editStopMarket($orderID,$price, $orderQty= false) {
        $data['method'] = "PUT";
        $data['function'] = "order";
        if($this->symbol=='XBTUSD') {
            $price= ceil($price/0.5)*0.5;
        } elseif($this->symbol=="ETHUSD") {
            $price= ceil($price/0.05)*0.05;
        }
        $data['params'] = array(
            "orderID" => $orderID,
            "stopPx" => $price,
        );
        if($orderQty) {
            $data['params']['orderQty']= $orderQty;
        }

        return $this->authQuery($data);
    }
    public function editStopMarketQty($orderID,$orderQty, $price= false) {
        $data['method'] = "PUT";
        $data['function'] = "order";
        $data['params'] = array(
            "orderID" => $orderID,
            'orderQty'=> $orderQty
        );

        if($price) {
            $data['params']['stopPx']= $price;
        }


        return $this->authQuery($data);
    }

    public function createStopOrder($quantity,$stopPrice, $instructions = false) {
        $data['method'] = "POST";
        $data['function'] = "order";
        $data['params'] = array(
            "symbol" => $this->symbol,
            "stopPx" => $stopPrice,
            "orderQty" => $quantity,
            "execInst"=> "Close",
            "ordType" => "Market"
        );

        if($instructions) {
            $data['params']['execInst'] = $instructions;
        }

        return $this->authQuery($data);
    }

    /*
     * Create Stop Limit Order
     *
     * Create new stop limit order
     *
     * @param $quantity is a number of contracts
     * @param $stopPrice is an order trigger price
     * @param $price is an order execution price
     *
     * @return new order array
     */

    public function createStopLimitOrder($quantity,$stopPrice,$price,$instructions = false) {
        $data['method'] = "POST";
        $data['function'] = "order";
        if($this->symbol=='XBTUSD') {
            $price= ceil($price/0.5)*0.5;
        } elseif($this->symbol=="ETHUSD") {
            $price= ceil($price/0.5)*0.5;
        }
        $data['params'] = array(
            "symbol" => $this->symbol,
            "stopPx" => $stopPrice,
            "price" => $price,
            "orderQty" => $quantity,
            "ordType" => "StopLimit"
        );

        if($instructions) {
            $data['params']['execInst'] = $instructions;
        }

        return $this->authQuery($data);
    }

    /*
     * Cancel All Open Orders
     *
     * Cancels all of your open orders
     *
     * @param $text is a note to all closed orders
     *
     * @return all closed orders arrays
     */

    public function cancelAllOpenOrders($text = "") {
        $data['method'] = "DELETE";
        $data['function'] = "order/all";
        $data['params'] = array(
            "symbol" => $this->symbol,
            "text" => $text,
            "filter"=> array(
                "ordType" => ['Stop','StopLimit', 'Limit'],
            ),
        );

        return $this->authQuery($data);
    }
    public function cancelOrder($orderID, $text = "") {
        $data['method'] = "DELETE";
        $data['function'] = "order";
        $data['params'] = array(
            "symbol" => $this->symbol,
            "orderID" => $orderID,
            "text" => $text
        );

        return $this->authQuery($data);
    }

    public function cancelSideOrder($side, $text = "") {
        $data['method'] = "DELETE";
        $data['function'] = "order/all";
        $data['params'] = array(
            "side" => $side,
            "symbol" => $this->symbol,
            "text" => $text
        );

        return $this->authQuery($data);
    }

    /*
     * Get Wallet
     *
     * Get your account wallet
     *
     * @return array
     */

    public function getWallet() {

        $data['method'] = "GET";
        $data['function'] = "user/wallet";
        $data['params'] = array(
            "currency" => "XBt"
        );

        return $this->authQuery($data);
    }

    public function getWalletSummary() {

        $data['method'] = "GET";
        $data['function'] = "user/walletSummary";
        $data['params'] = array(
            "currency" => "XBt"
        );

        return collect($this->authQuery($data));
    }

    /*
     * Get Margin
     *
     * Get your account margin
     *
     * @return array
     */

    public function getMargin() {

        $data['method'] = "GET";
        $data['function'] = "user/margin";
        $data['params'] = array(
            "currency" => "XBt"
        );

        return $this->authQuery($data);
    }

    /*
     * Get Order Book
     *
     * Get L2 Order Book
     *
     * @return array
     */

    public function getOrderBook($depth = 25) {
        $data['method'] = "GET";
        $data['function'] = "orderBook/L2";
        $data['params'] = array(
            "symbol" => $this->symbol,
            "depth" => $depth
        );

        return $this->authQuery($data);
    }

    /*
     * Set Leverage
     *
     * Set position leverage
     * $leverage = 0 for cross margin
     *
     * @return array
     */

    public function setLeverage($leverage) {
        $data['method'] = "POST";
        $data['function'] = "position/leverage";
        $data['params'] = array(
            "symbol" => $this->symbol,
            "leverage" => $leverage
        );

        return $this->authQuery($data);
    }

    /*
     * Private
     *
     */

    /*
     * Auth Query
     *
     * Query for authenticated queries only
     *
     * @param $data consists method (GET,POST,DELETE,PUT),function,params
     *
     * @return return array
     */

    private function authQuery($data) {

        $method = $data['method'];
        $function = $data['function'];
        if($method == "GET" || $method == "POST" || $method == "PUT") {
            $params = http_build_query($data['params']);
        }
        elseif($method == "DELETE") {
            $params = http_build_query($data['params']);
        }
        $path = self::API_PATH . $function;
        $url = $this->api_url . self::API_PATH . $function;
        if($method == "GET" && count($data['params']) >= 1) {
            $url .= "?" . $params;
            $path .= "?" . $params;
        }
        $nonce = $this->generateNonce();
        if($method == "GET") {
            $post = "";
        }
        else {
            $post = $params;
        }

        $sign = hash_hmac('sha256', $method.$path.$nonce.$post, $this->apiSecret);

        $headers = array();

        $headers[] = "api-signature: $sign";
        $headers[] = "api-key: {$this->apiKey}";
        $headers[] = "api-nonce: $nonce";

        $headers[] = 'Connection: Keep-Alive';
        $headers[] = 'Keep-Alive: 90';

        curl_reset($this->ch);
        curl_setopt($this->ch, CURLOPT_URL, $url);
        if($data['method'] == "POST") {
            curl_setopt($this->ch, CURLOPT_POST, true);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post);
        }
        if($data['method'] == "DELETE") {
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post);
            $headers[] = 'X-HTTP-Method-Override: DELETE';
        }
        if($data['method'] == "PUT") {
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "PUT");
            //curl_setopt($this->ch, CURLOPT_PUT, true);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post);
            $headers[] = 'X-HTTP-Method-Override: PUT';
        }
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER , false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        if(!empty($this->proxy)) {
            curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy);
        }
        $return = curl_exec($this->ch);
        if(!$return) {
            $this->curlError();
            $this->error = true;
            return $this;
        }

        $return = json_decode($return,true);

        if(isset($return['error'])) {
            $this->platformError($return);
            $this->error = true;
            return $return;
        }

        $this->error = false;
        $this->errorCode = false;
        $this->errorMessage = false;

        return $return;

    }

    /*
     * Public Query
     *
     * Query for public queries only
     *
     * @param $data consists function,params
     *
     * @return return array
     */

    private function publicQuery($data) {

        $function = $data['function'];
        $params = http_build_query($data['params']);
        $url = $this->api_url . self::API_PATH . $function . "?" . $params;;

        $headers = array();

        $headers[] = 'Connection: Keep-Alive';
        $headers[] = 'Keep-Alive: 90';

        curl_reset($this->ch);
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER , false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
        if(!empty($this->proxy)) {
            curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy);
        }
        $return = curl_exec($this->ch);

        if(!$return) {
            $this->curlError();
            $this->error = true;
            return $return;
        }

        $return = json_decode($return,true);

        if(isset($return['error'])) {
            $this->platformError($return);
            $this->error = true;
            return $return;
        }

        $this->error = false;
        $this->errorCode = false;
        $this->errorMessage = false;

        return $return;

    }

    /*
     * Generate Nonce
     *
     * @return string
     */

    private function generateNonce() {

        $nonce = (string) number_format(round(microtime(true) * 100000), 0, '.', '');

        return $nonce;

    }

    /*
     * Curl Init
     *
     * Init curl header to support keep-alive connection
     */

    private function curlInit() {

        $this->ch = curl_init();

    }

    /*
     * Curl Error
     *
     * @return false
     */

    private function curlError() {

        if ($errno = curl_errno($this->ch)) {
            $this->errorCode = $errno;
            $errorMessage = curl_strerror($errno);
            $this->errorMessage = $errorMessage;
            if($this->printErrors) echo "cURL error ({$errno}) : {$errorMessage}\n";
            return $errorMessage;
        }

        return false;
    }

    /*
     * Platform Error
     *
     * @return false
     */

    private function platformError($return) {

        $this->errorCode = $return['error']['name'];
        $this->errorMessage = $return['error']['message'];
        if($this->printErrors) echo "BitMex error ({$return['error']['name']}) : {$return['error']['message']}\n";

        return $return;
    }

}