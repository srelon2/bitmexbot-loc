<?php

namespace App\Http\Controllers\Admin;

use App\Traits\BotsTraits;
use App\Traits\Admin\AdminBotsTraits;
use App\Http\Controllers\Admin\Controller;

class AdminBotsController extends Controller
{
    use BotsTraits, AdminBotsTraits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $accesses = $this->accessesRoles($this->admin, 'AdminBotsController');

            if (!$accesses['view']) abort('403');
            $this->data['edit'] = $accesses['edit'];

            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.bots', $this->data)->with('pagetitle', 'Просмотр Ботов пользователя');
    }

    public function info($id = false, $backup = false)
    {
        if ($id) {
            if ($backup) {
                $backup = $this->getBackup('bots', $id, base64_decode($backup));
            }

            if ($backup) {
                $item = $backup;
            } else {
                $item= $this->getBot($id);
            }
            $this->data['item']= $item;
            $this->data['backups'] = $this->getLog('bots', $id);
            $this->data['exections']= $this->getExProfit($id, $item->count);
        }
        return view('admin.bots-info', $this->data)->with('pagetitle', (($id) ? 'Просмотр Bot ID:' . $id : 'Create Bots') . (($backup) ? ' Restored' : ''));
    }

    public function search()
    {
        $response = $this->ajaxTableUsersBot($_POST);
        echo json_encode($response);
    }

    public function update(BotsRequest $request, $id= false) {
        $data= $request->except('_token');

        $response= $this->updateBots($data, $id);
        return redirect()->route('admin.bots.info', ['id'=>$response['item']->id])->with($response['status'], $response['mess']);
    }
    public function itemAction($action,$id= false) {
        if(empty($id)) {
            $id= Input::get('data');
        }
        $ajax= Input::get('ajax');
        $response= $this->actionBots($action, $id);
        switch ($response['status']) {
            case 'success':
                $msg= $response['mess'];
                $status= 'success';
                break;
            default:
                $msg= 'Error. Не удалось изменить! '.json_encode($response['mess']);
                $status= 'danger';
        }
        if($ajax) {
            return response()->json(array('msg'=> $msg, 'status'=>$status));
        } else {
            return redirect()->back()->with($status, $msg);
        }
    }
}