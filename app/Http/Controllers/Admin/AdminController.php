<?php

namespace App\Http\Controllers\Admin;

use Route;

use App\Http\Controllers\Api\QrCode\src\ErrorCorrectionLevel;
use App\Http\Controllers\Api\QrCode\src\QrCode;

use GuzzleHttp;

class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.dashboard')->with($this->data);
    }

    public function import($table, $format='excel', $id=false) {
        $responce= $this->importTable($table, $format, $id);
    }
}
