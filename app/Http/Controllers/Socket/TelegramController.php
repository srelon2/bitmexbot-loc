<?php

namespace App\Http\Controllers\Socket;

use Illuminate\Http\Request;
//use Telegram\Bot\Laravel\Facades\Telegram;
use App\Http\Controllers\Controller;
use App\Traits\GlobalTraits;
use App\Traits\TelegramTraits;
use App\Traits\BotsTraits;
use App\Traits\BotSignalsTraits;

use Carbon\Carbon;

class TelegramController extends Controller
{
    use GlobalTraits, TelegramTraits, BotsTraits, BotSignalsTraits;

    public function setHook(){
        $api= $this->telegramApi('bot');

        $api->setWebhook(['url' => 'https://'.env('APP_NAME').'/tkvkofaiyulgfzreesuntlbeoibmtp/webhook']);
        return response()->json(["ok" => true]);
    }
    public function stopHook(){
        $api= $this->telegramApi('bot');
        $api->removeWebhook();
        return response()->json(["ok" => true]);
    }
    
    public function hook(Request $req)
    {
//        return response()->json(["ok" => true]);
        try {
            $api= $this->telegramApi('bot');
            $update = $api->getWebhookUpdates();
            $buttons=[];
            $mess= '';
            if (!isset($update['callback_query'])){
                if (isset($req['message']['chat']['username'])) {
                    $username ='@'.$req['message']['chat']['username'];
                }else {
                    $username = $req['message']['chat']['first_name'];
                }

                $chatId = $req['message']['chat']['id'];
                if(isset($req['message']['text'])) {
                    $message = htmlspecialchars($req['message']['text']);
                }

                $data= $this->getTelUser($chatId);
                if (!$data) {
                    $data = $this->createTelUser($username, $chatId);
                }
            } else {
                $chatId=$update['callback_query']['from']['id'];
                $message = mb_strtolower($update['callback_query']['data']);
                $data= $this->getTelUser($chatId);
                file_get_contents("https://api.telegram.org/bot".$api->getAccessToken()."/answerCallbackQuery?callback_query_id=".$update['callback_query']['id']);

                $message_id= $update['callback_query']['message']['message_id'];
                if($data->message_id!==$message_id) {
                    $data->message_id= $message_id;
                    $data->save();
                }
            }


            if(!empty($message)) {
                parse_str($message, $output);

                if($message{0}=='/' && strlen($message)>1) $message= substr($message, 1);
                if(isset($output['action'])) {
                    $message= $output['action'];
                } else {
                    if(count(explode('/', $message))>1) {
                        $explode= explode('/', $message);
                    } else {
                        $split=preg_split("/[0-9]+/", $message)[0];
                        if($split==null) {
                            $explode[0]= false;
                        } else {
                            $explode= explode($split, $message);
                            $explode[0]= $split;
                        }
                    }
                    $message= $explode[0];
                }
            } else {
                $message= 'wrong';
            }
            if(!isset($output['page'])) $output['page']=0;
            $pages=[];

            if(isset($data->user)) {
                $keyboard= $this->getKeyboardTel();
                $user_id= $data->user_id;

                $reply_markup = $api->replyKeyboardMarkup([
                    'keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'selective' => false,
                    'one_time_keyboard' => false,
                ]);
//                $this->telegramNotification(mb_strtolower($message));
                    switch (mb_strtolower($message)) {
                    case "start":
                    case "my id":
                            $api->sendMessage([
                                'chat_id' => $chatId,
                                'text' => 'Your chat ID '.$chatId,
                                "reply_markup"=> $api->replyKeyboardMarkup([
                                    'keyboard' => $keyboard,
                                    'resize_keyboard' => true,
                                    'selective' => false,
                                    'one_time_keyboard' => false,
                                ]),
                            ]);

                            return response()->json(["ok" => true]);
                        break;

                    //BOTS
                    case "b":
                    case "bots":
                    case "bot list":
                    case "🤖 bot list":
                        if(isset($explode[1]) && is_numeric($explode[1])) {
                            parse_str('action=info-bots&id='.$explode[1], $output);
                        } else {
                            parse_str('action=get-bots', $output);
                            if($explode[1]!=="") $output['filter']= $explode[1];
                        }
                    case "info-bots":
                        if(isset($output['id'])) {
                            if(!isset($output['page'])) $output['page']=0;
                            $id= $output['id'];
                            $bot= $this->getBot($id, $user_id);

                            if(empty($bot) || $bot->saved) {
                                $mess.="\nYou no have bots";
                                break;
                            }

                            if(isset($output['status'])) {
                                if($output['status']== 'disabled') {
                                    $bot->active= 0;
                                    $bot->save();
                                    $mess="<b>Настройки успешно Отключены</b>\n";
                                } elseif($output['status']== 'active') {
                                    if($bot->active) {
                                        $mess.="\nНастройки уже включены.";
                                    } else {
                                        $bot->active= 2;
                                        $bot->count=$bot->count+1;
                                        $bot->save();
                                        $responce= $this->startBot($bot->id);
                                        $mess.= "<b>".$responce['status']."</b> ".$responce['mess']."\n";
                                    }
                                }
                                $bot= $this->getBot($id, $user_id);
                            }

                            if(in_array($bot->active, ['0'])) {
                                $status=hex2bin('E29D8C')." Disabled";
                            } elseif(in_array($bot->active, ['1','2', '3'])) {
                                $status=hex2bin('E29C85')." Active";
                            } else {
                                $status=hex2bin('E29D8C')." Error";
                            }
                            $mess.= "<b>".hex2bin('F09F8694')." Bot ID</b>: /b".$bot->id."\n";
                            $mess.="<b>Name</b>: ".(isset($bot->name) ? $bot->name : $bot->currency_pair)." <b>Pair</b>: ".$bot->currency_pair." <b>".$status."</b>\n";
                            $exections= $this->getExProfit($bot->id, $bot->count)->where('profit','!=', 0);
                            $profit_last= $this::printNum(($exections->last()) ? $exections->last()['profit'] : 0);
                            $profit_day= $this::printNum($exections->where('date', '>=', Carbon::now()->startOfDay())->sum('profit'));
                            $profit_week= $this::printNum($exections->where('date', '>=', Carbon::now()->startOfWeek())->sum('profit'));
                            $space= '            ';
                            if($bot->active) {
                                if(empty($bitmex)) $bitmex= $this->getBitMex($bot->api_id, $bot->currency_pair);
                                $position= collect($bitmex->getOpenPositions());
                                $position= $position->where('symbol', $bot->currency_pair)->first();
                                if($position) {
                                    $signals= $this->getSignals($bot->id, $bot->user_id, [['order_type', '!=','Limit'], ['status','=', 0]]);
                                    if(count($signals)) {
                                        $mess.= "-----------------\n<b>Позиция: ".$position['currentQty']." ".$position['quoteCurrency']."</b>";
                                        $tp= $signals->where('order_type', 'Take Profit')->first();
                                        $sl= $signals->where('order_type', 'Stop Loss')->first();

                                        $mess.= "<pre>\nAvg Price   Take Profit Stop Loss\n</pre>";
                                        $mess.= "<pre>".$position['avgCostPrice'].substr($space, 0, -mb_strlen($position['avgCostPrice'])).$tp->price.substr($space, 0, -mb_strlen($tp->price)).$sl->price."\n</pre>";
                                    }
                                }

                            }
                            $mess.= "-----------------\n<b>Доход XBT</b>";

                            $mess.= "<pre>\nLast        Day         Week\n</pre>";
                            $mess.= "<pre>".$profit_last.substr($space, 0, -mb_strlen($profit_last)).$profit_day.substr($space, 0, -mb_strlen($profit_day)).$profit_week."\n</pre>";

                            if($bot->active) {
                                $buttons[]= array([
                                    'text'=> 'Остановить',
                                    'callback_data'=> 'action=info-bots&id='.$bot->id.'&status=disabled',
                                ],[
                                    'text'=> 'Закрыть позицию',
                                    'callback_data'=> 'action=close-position&id='.$bot->id,
                                ]);
                            } else {
                                $buttons[]= array([
                                    'text'=> 'Запустить',
                                    'callback_data'=> 'action=info-bots&id='.$bot->id.'&status=active',
                                ],[
                                    'text'=> 'Закрыть позицию',
                                    'callback_data'=> 'action=close-position&id='.$bot->id,
                                ]);
                            }
                            break;
                        }
                    case 'active bot':
                    case 'disabled bot':
                    case 'active bots':
                    case 'disabled bots':
                    case 'bot active':
                    case 'bot disabled':
                    case 'bots active':
                    case 'bots disabled':
                        if(in_array(mb_strtolower($message), ['bot active', 'bots active', 'active bot', 'active bots'])){
                            parse_str('action=get-bots&filter=active', $output);
                        } elseif(in_array(mb_strtolower($message), ['bot disabled', 'bots disabled', 'disabled bot', 'disabled bots'])) {
                            parse_str('action=get-bots&filter=disabled', $output);
                        }
                    case "get-bots":
                        if(!isset($output['page'])) $output['page']=0;
                        $bots= $this->getBotSettings($user_id, [['saved', 0]]);
                        if(isset($output['filter'])) {
                            switch($output['filter']) {
                                case 'active':
                                    $filter= ['1', '2 ', '3'];
                                    $mess.= "<b>Active Bots</b>";
                                    break;
                                case 'disabled':
                                    $filter= ['0'];
                                    $mess.= "<b>Disabled Bots</b>";
                                    break;
                            }
                            $bots= $bots->whereIn('active', $filter);
                        } else {
                            $mess.= "<b>My Bots</b>";
                        }
                        if(count($bots) == 0) {
                            $mess.="\nYou no have bots";
                            break;
                        }
                        $pages= collect($bots)->chunk(4);
                        if(count($pages) >= $output['page']) {
                            $items= $pages[$output['page']];
                        } else {
                            $items= $pages[0];
                        }
                        foreach($items as $bot) {
                            if(in_array($bot->active, ['0'])) {
                                $status=hex2bin('E29D8C')." Disabled";
                            } elseif(in_array($bot->active, ['1','2', '3'])) {
                                $status=hex2bin('E29C85')."Active";
                            } else {
                                $status=hex2bin('E29D8C')." Error";
                            }
                            $mess.= "\n<b>".hex2bin('F09F8694')." Bot ID</b>: /b".$bot->id."\n";
                            $mess.="<b>Name</b>: ".(isset($bot->name) ? $bot->name : $bot->currency_pair)." <b>Pair</b>: ".$bot->currency_pair." <b>".$status."</b>\n-----------------";
                        }
                        break;


                    //BOTS
                    case "🚨 support":
                    case "support":
                    case "sup":
                        $mess= 'Вы можете написать нам в Telegram';
                        $buttons[]= array(
                                ['text'=>'Написать в поддержку', 'url'=> "https://t.me/"]
                            );
                        break;
                    case "close-position":
                    case "close-bot":
                    case "cp":
                    case "cb":
                        if(isset($explode[1]) && is_numeric($explode[1])) {
                            parse_str('action=close-position&id='.$explode[1], $output);
                        }
                        if(isset($output['id'])) {
                            if(!isset($output['page'])) $output['page']=0;
                            $id= $output['id'];
                            $bot= $this->getBot($id, $user_id);
                            if($bot) {
                                $responce= $this->clousePosition($bot);
                                if($responce) {
                                    $mess.="\nПозиция успешно закрыта";
                                } else {
                                    $mess.="\nОшибка. Позиция не была закрыта";
                                }
                                break;
                            }
                        }
                    default :
                        $test= $api->sendMessage([
                            'chat_id' => $chatId,
                            'text' => 'Wrong command.',
                            "reply_markup"=> $api->replyKeyboardMarkup([
                                'keyboard' => $keyboard,
                                'resize_keyboard' => true,
                                'selective' => false,
                                'one_time_keyboard' => false,
                            ]),
                        ]);
                        return response()->json(["ok" => true]);
                }

            } else {
                $keyboard= [
                    [
                        ['text'=>'Start']
                    ]
                ];
                switch (mb_strtolower($message)) {
                    case "start":
                    case "my id":
                        $api->sendMessage([
                            'chat_id' => $chatId,
                            'text' => 'Your chat ID '.$chatId,
                        ]);

                        return response()->json(["ok" => true]);
                        break;
                    default :
                        $test= $api->sendMessage([
                            'chat_id' => $chatId,
                            'text' => 'Wrong command.',
                            "reply_markup"=> $api->replyKeyboardMarkup([
                                'keyboard' => $keyboard,
                                'resize_keyboard' => true,
                                'selective' => false,
                                'one_time_keyboard' => false,
                            ]),
                        ]);
                        return response()->json(["ok" => true]);
                }
            }
            $page= $output['page'];
            if(count($pages)>1) {
                $btnPage= [];
                $output['page']= $page+1;
                if($page >= 1) {
                    $output['page']= $page-1;
                    $btnPage[]= [
                        'text'=> 'Previous Page',
                        'callback_data'=> http_build_query($output)
                    ];
                }
                if(count($pages)!= $page+1) {
                    $output['page']= $page+1;
                    $btnPage[]= [
                        'text'=> 'Next Page',
                        'callback_data'=> http_build_query($output)
                    ];
                }
                $buttons[]= $btnPage;
            }
            if(count($buttons)>0) {
                $reply_markup = $api->replyKeyboardMarkup([
                    'inline_keyboard'=> $buttons,
                ]);
            } else {
                $reply_markup = $api->replyKeyboardMarkup([
                    'keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'selective' => false,
                    'one_time_keyboard' => false,
                ]);
            }
            if(isset($file)) {
                if($mess) {
                    $api->sendMessage([
                        'chat_id' => $data->chat_id,
                        "text"=> $mess,
                        "parse_mode"=> "html",
                    ]);
                }
                if($file) {
                    if(isset($message_id) && count($buttons)>0) {
                        $media= [
                            'type'=> 'photo',
                            'media'=> asset('uploads/'.$file),
                        ];

                        $api->editMessageMedia([
                            'chat_id' => $chatId,
                            'message_id' => $message_id,
                            'media' => json_encode($media),
                            "reply_markup"=> $reply_markup,
                        ]);
                    } else {

                        $api->sendPhoto([
                            'chat_id' => $chatId,
                            'photo' => public_path('uploads/'.$file),
                            "reply_markup"=> $reply_markup,
                        ]);
                    }
                    unlink(public_path('uploads/'.$file));
                }


            } else {
                if(isset($message_id) && count($buttons)>0) {
                      $api->editMessageText([
                          'chat_id' => $data->chat_id,
                          'message_id' => $message_id,
                          "text"=> $mess,
                          "parse_mode"=> "html",
                          "reply_markup"=> $reply_markup,
                      ]);
                  } else {
        //                Telegram::sendMessage([
        //                    'chat_id' => '475407201',
        //                    'text' => '$mess',
        //                    "parse_mode"=> "html",
        //                ]);
                      $api->sendMessage([
                          'chat_id' => $data->chat_id,
                          "text"=> $mess,
                          "parse_mode"=> "html",
                          "reply_markup"=> $reply_markup,
                      ]);
                  }
            }

                return response()->json(["ok" => true]);


        } catch (\Exception $e) {
            $this->telegramNotification("Error Telegram Bot - ".$e->getMessage().". Line - ".$e->getLine());
            return response()->json(["ok" => true]);
        }
    }
}
