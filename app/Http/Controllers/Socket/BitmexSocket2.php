<?php

namespace App\Http\Controllers\Socket;
use App\Traits\GlobalTraits;
use Carbon\Carbon;
use App\Traits\TelegramTraits;

use App\Http\Controllers\Controller;
use Unirest\Exception;

use App\Traits\BotsTraits;
use App\Traits\BotSignalsTraits;
use App\Traits\ApiKeysTraits;


class BitmexSocket extends Controller {
    use BotsTraits, BotSignalsTraits, GlobalTraits, ApiKeysTraits, TelegramTraits;

    public function setHook($socket_id){

//        set_time_limit(0);

//        $apiKey = 'IojDQjCw0-6lC0nr36jDeXsZ';
//        $apiSecret = 'NEsN39ITERFlhn6W5giEitwJAX0qsIz9NBOs5NvT0etEgLSa';
//
//        $expires= time()+(60*60);
//        $apiSecret = 'QK6IGjmZfqwRXlbhEUHvoq6m35xGJubKjjam6vdGyIqKc4on';
//        $hash_hmac = hash_hmac('sha256','GET/realtime'.strval($expires), $apiSecret);
//        dd($hash_hmac);

        $start_date= Carbon::now()->format('Y-m-d H:i:s');

        $loop = \React\EventLoop\Factory::create();
        $reactConnector = new \React\Socket\Connector($loop, [
            'timeout' => 20
        ]);

        $connector = new \Ratchet\Client\Connector($loop, $reactConnector);
        if(env('APP_ENV')=='project') {
            $wss= 'wss://www.bitmex.com/realtimemd';
        } else {
            $wss= 'wss://testnet.bitmex.com/realtimemd';
        }
        $connector($wss)->then(function($conn) use($socket_id, $start_date) {
            $apis= $this->getApiKeys();
            $conn->send('[1, "test0", "'.$socket_id.'"]');
            $conn->send('[0, "test0", "'.$socket_id.'", {"op": "subscribe", "args": "trade"}]');
            $conn->send('[0, "test0", "'.$socket_id.'", {"op": "subscribe", "args": "connected"}]');
            foreach ($apis as $key=> $api) {
                $apiKey = $api->key;
                $apiSecret = $api->secret;
                $expires= time()+(60*60);
                $hash_hmac = hash_hmac('sha256','GET/realtime'.$expires, $apiSecret);
                $multiplexing= '"'.$api->id.'", "'.$socket_id.'"';
                $conn->send('[1, '.$multiplexing.']');
                $conn->send('[0,  '.$multiplexing.', {"op": "authKeyExpires", "args": ["'.$apiKey.'", '.$expires.', "'.$hash_hmac.'"]}]');


//                if($key==0) {
//                    $conn->send('[0, '.$multiplexing.', {"op": "subscribe", "args": "trade"}]');
//                    $conn->send('[0, '.$multiplexing.', {"op": "subscribe", "args": "connected"}]');
//                }

                $conn->send('[0, '.$multiplexing.', {"op": "subscribe", "args": "execution"}]');
                $conn->send('[0, '.$multiplexing.', {"op": "subscribe", "args": "order"}]');
                $conn->send('[0, '.$multiplexing.', {"op": "subscribe", "args": "position"}]');
            }

            $conn->on('message', function(\Ratchet\RFC6455\Messaging\MessageInterface $msg) use ($conn, $socket_id, $start_date) {
                dump("Received: {$msg}\n");
                $data= json_decode("{$msg}");
                if(isset($data[3]->status) && $data[3]->status==401) {
                    $this->apiKeysDisable($data[1]);
                }
                if(isset($data[3]->subscribe) && $data[3]->subscribe== 'connected') {
                    $this->startSocketDate($socket_id, $start_date);
                }
                if(isset($data[3]->table) && $data[3]->action!= 'partial') {

                    switch ($data[3]->table) {
                        case 'connected':
                            $this->updateSocketDate($socket_id);
                            usleep(300);
                            break;
                        case 'trade':
                            $clouse= $this->checkSocketDate($socket_id, $start_date);
                            $positon= $this->checkPositions();
                            if($clouse) {
                                $conn->close();
                                if($clouse=='reset') {
                                    $this->useCurl(['http://'.env('APP_NAME').'/tkvkofaiyulgfzreesuntlbeoibmtp/setHook/1']);
                                }
                            }
                            break;
                        case 'execution':
                            $this->updateExecution($data[3]);
                            break;
                        case 'position':
                            $this->socketUpPosition($data[3]);
                            break;
                        case 'order':
                            $this->updateSignal($data[3]);
                            break;
                        default:
                            dump('test2');
                    }
                }

            });

            $conn->on('close', function($code = null, $reason = null) use ($conn) {
                echo "Connection closed ({$code} - {$reason})\n";
            });



//            $apiKey = 'f2Cbpzly2kZ7iRKobtG1Ur7R';
//            $apiSecret = 'QK6IGjmZfqwRXlbhEUHvoq6m35xGJubKjjam6vdGyIqKc4on';
//
//            $expires= time()+(60*60);
//            $hash_hmac = hash_hmac('sha256','GET/realtime'.$expires, $apiSecret);
//
////            $conn->send('{"op": "authKeyExpires", "args": ["'.$apiKey.'", '.$expires.', "'.$hash_hmac.'"]}');
//            $conn->send('[0, "hudsyg1q5dda4", "user_1", {"op": "subscribe", "args": "order"}]');
//            $conn->send('[0, "hudsyg1q5dda4", "user_1", {"op": "subscribe", "args": "position"}]');
//            $conn->send('[0, "hudsyg1q5dda4", "user_1", {"op": "subscribe", "args": "connected"}]');
//            $conn->send('[0, "hudsyg1q5dda4", "user_1", {"op": "subscribe", "args": "trade"}]');
//
//            $conn->send('[1, "hudsyg1q5dda5", "user_2"]');
//            $conn->send('[0, "hudsyg1q5dda5", "user_2", {"op": "authKeyExpires", "args": ["'.$apiKey.'", '.$expires.', "'.$hash_hmac.'"]}]');
//
//
////            $conn->send('{"op": "authKeyExpires", "args": ["'.$apiKey.'", '.$expires.', "'.$hash_hmac.'"]}');
//            $conn->send('[0, "hudsyg1q5dda5", "user_2", {"op": "subscribe", "args": "order"}]');
//            $conn->send('[0, "hudsyg1q5dda5", "user_2", {"op": "subscribe", "args": "position"}]');
//            $conn->send('[0, "hudsyg1q5dda5", "user_2", {"op": "subscribe", "args": "connected"}]');
//            $conn->send('[0, "hudsyg1q5dda5", "user_2", {"op": "subscribe", "args": "trade"}]');
//            $conn->send('{"op": "cancelAllAfter", "args": 3600}');
        }, function ($e) use($loop) {
            echo "Could not connect: {$e->getMessage()}\n";
            $loop->stop();
        });

        $loop->run();
    }
}