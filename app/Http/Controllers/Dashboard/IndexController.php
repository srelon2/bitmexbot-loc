<?php

namespace App\Http\Controllers\Dashboard;
use App\Traits\BotSignalsTraits;
use App\Traits\BotsTraits;
use App\Traits\GlobalTraits;
use Illuminate\Support\Facades\Auth;
use App\Traits\ApiKeysTraits;

class IndexController extends Controller
{
    use BotsTraits, BotSignalsTraits, GlobalTraits, ApiKeysTraits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['apiKeys']= $this->getApiKeys(Auth::id());
        return view('dashboard.dashboard')->with($this->data);
    }
}
