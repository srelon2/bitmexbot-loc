<?php

namespace App\Http\Controllers\Dashboard;

use App\Requests\BotSettingRequest;

use App\Traits\BotsTraits;
use App\Traits\BotSignalsTraits;
use App\Traits\ApiKeysTraits;
use App\Traits\TelegramTraits;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class BotController extends Controller
{
    use BotsTraits, BotSignalsTraits, ApiKeysTraits, TelegramTraits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function info($id= false) {
        if($id) {
            $item= $this->getBot($id, Auth::id());
            if(empty($item)) abort(404);
            $this->data['item']= $item;
            $this->data['exections']= $this->getExProfit($id, $item->count);

            $group= collect();
            $signals= $this->getBotSignals($id, Auth::id())->groupBy('count')->transform(function ($item) use ($group){
                if($item->where('order_type', 'Take Profit')->where('status', 1)->count() > 0) {
                    $sigmals= $item->where('order_type', 'Limit');
                    $type= $sigmals->where('status', 1)->last()->side;
                    $group->push(['steps'=>$sigmals->where('side', $type)->where('status', 2)->count()]);
                    return $group;
                }
            });
            $this->data['remainder']= $group->sortBy('steps')->groupBy('steps');
        }
        $this->data['id']= $id;
        $this->data['patterns']= $this->getPatterns(Auth::id());
        $this->data['apiKeys']= $this->getApiKeys(Auth::id());
        return view('dashboard.bot-info')->with($this->data);
    }

    public function signals_search($bot_id) {
        $response= $this->ajaxTableSignals($_POST, $bot_id);
        echo json_encode($response);
    }
    public function exection_search($bot_id) {
        $response= $this->ajaxTableExections($_POST, $bot_id);
        echo json_encode($response);
    }
    public function bot_search() {
        $response= $this->ajaxTableBot($_POST);
        echo json_encode($response);
    }

    public function update(BotSettingRequest $request, $id= false)
    {
        $data= $request->all(['api_id', 'currency_pair','amount', 'take_profit','stop_loss','position', 'name','closed','timer', 'steps', 'submit', 'check_position']);
        $response= $this->updateBots($data, $id);
        if($response['bot_id']) {
            return redirect()->route('dashboard.bot.info', ['id'=>$response['bot_id']])->with($response['status'], $response['mess']);
        } else {
            return redirect()->back()->with($response['status'], $response['mess']);
        }
    }
    public function pattern($id= false){
        $pattern_id= Input::get('data');
        $this->data['item']= $this->getPattern($pattern_id);
        $this->data['apiKeys']= $this->getApiKeys(Auth::id());
        $this->data['patterns']= $this->getPatterns(Auth::id());
        $this->data['id']= $id;
        if($id) {
            $this->data['exections']= $this->getBalances(Auth::id(), $id)->where('profit','!=', 0);
        } else {
            $this->data['exections']= false;
        }

        $msg= 'Шаблон загружен';
        $status= 'success';
        $content=  view('dashboard.bot-info-form')->with($this->data)->render();
        return response()->json(array('msg'=> $msg, 'status'=>$status, 'content'=>$content));
    }
}
