<?php

namespace App\Http\Controllers\Dashboard;

use App\Requests\TelegramRequest;
use App\Requests\BotSettingRequest;

use App\Traits\BotsTraits;
use App\Traits\BotSignalsTraits;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Traits\ApiKeysTraits;
use App\Traits\TelegramTraits;
use App\Traits\UsersTraits;

use App\Requests\ApiKeysRequest;

class SettingsController extends Controller
{
    use BotsTraits, BotSignalsTraits, ApiKeysTraits, TelegramTraits, UsersTraits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $this->data['user']= Auth::user();
        return view('dashboard.settings')->with($this->data);
    }

    public function keys_search() {
        $response= $this->ajaxTableApiKeys($_POST);
        echo json_encode($response);
    }

    public function itemAction() {
        $id= Input::get('data');
        $action= Input::get('action');
        $ajax= Input::get('ajax');
        $response= $this->actionApiKeys($action, $id);
        if($ajax) {
            return response()->json(array('msg'=> $response['mess'], 'status'=>$response['status']));
        } else {
            return redirect()->back()->with($response['status'], $response['mess']);
        }

    }

    public function create(ApiKeysRequest $request) {
        $data= $request->all(['key', 'secret']);
        $response= $this->updateApiKeys($data);

        return redirect()->back()->with($response['status'], $response['mess']);
    }

    public function telUpdate(TelegramRequest $request){
        $data= $request->except('_token');
        $response= $this->updateTelUser($data, Auth::id());

        return redirect()->back()->with($response['status'], $response['mess']);
    }

    public function telDestroy($id) {
        $response= $this->destroyTelUser($id, Auth::id());

        return redirect()->back()->with($response['status'], $response['mess']);
    }

}
