<?php

namespace App\Http\Controllers\Dashboard;
use App\Traits\GlobalTraits;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use GlobalTraits;

    protected $data;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->middleware('auth');
            $this->data['func']= GlobalTraits::class;

            return $next($request);
        });
    }
}
