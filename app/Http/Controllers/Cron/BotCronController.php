<?php

namespace App\Http\Controllers\Cron;

use App\Traits\BotsTraits;
use App\Traits\BotSignalsTraits;
use App\Traits\GlobalTraits;
use App\Http\Controllers\Controller;
use App\Traits\ApiKeysTraits;
use App\Traits\TelegramTraits;
use App\Http\Controllers\Api\Bitmex\Bitmex;
use Carbon\Carbon;

class BotCronController extends Controller
{
    use BotsTraits, BotSignalsTraits, GlobalTraits, ApiKeysTraits, TelegramTraits;

    public function check(){
//        $bitmex= new Bitmex('f2Cbpzly2kZ7iRKobtG1Ur7R', 'QK6IGjmZfqwRXlbhEUHvoq6m35xGJubKjjam6vdGyIqKc4on');
//        dd($bitmex->getExection("4c07738c-b04d-64b7-0169-3ec7a62be1ff"));
//        dump('test');
//        dd($bitmex->getPositions());
//        dd($bitmex->getOrder('4d93a7c8-7c1f-62af-35e7-628035c7212b'));

        set_time_limit(0);
        $this->cronLogs('check');
       $bots= $this->getActiveBots();

       foreach ($bots as $bot) {
           $signals= $this->getSignals($bot->id,$bot->user_id, [['status', '=',0], ['order_type', '=', 'Limit']]);
           $bitmex= $this->getBitMex($bot->api_id, $bot->currency_pair);
           foreach ($signals as $signal) {
               $response= $this->checkSignals($signal, $bitmex);
           }
           $limits= $this->getSignals($bot->id,$bot->user_id, [['status', '=',0], ['order_type', '!=', 'Limit']]);

           foreach ($limits as $limit) {
               $response= $this->checkLimits(false, $limit, $bitmex);
                if($response && $response->status==0 && $response->order_type=='Take Profit') {
                    if(empty($bitmex)) $bitmex= $this->getBitMex($bot->api_id, $bot->currency_pair);
                    $position= collect($bitmex->getOpenPositions());
                    $position= $position->where('symbol', $bot->currency_pair)->first();
                    if($bot->active==1 && is_array($position) && $position['currentQty']==0) {
                        $this->telegramNotification(json_encode($position));
                        $this->telegramNotification(json_encode($response));
                        $this->telegramNotification(json_encode($bot));
                        $this->clousePosition($bot, $bitmex);
                    }
                }
           }
       }
    }

    public function startSocket(){
        set_time_limit(0);
        sleep(5);
        $this->cronLogs('startSocket');
        $soket= $this->getSocket();
        dump($soket->date);
            $this->useCurl(['http://'.env('APP_NAME').'/tkvkofaiyulgfzreesuntlbeoibmtp/setHook/1']);
//        }
    }

    public function checkStart(){
        set_time_limit(0);
        $this->cronLogs('checkStart');
        $bots= $this->getActiveBots(3)->where('start_date', '<', Carbon::now()->format('Y-m-d H:i:s'));
        foreach ($bots as $bot) {
            $bitmex= $this->getBitMex($bot->api_id, $bot->currency_pair);
            $positon = $this->positonQty($bot, $bitmex);
            if ($positon == 0 || $bot->check_position==2) {
                $bot= $this->updateBotStatus($bot);
                if($bot->timer==0 && $bot->active==2  && $bot->type!='default') {
                    $bot->start_date = Carbon::now()->addSeconds(30)->format('Y-m-d H:i:s');
                    $bot->save();
                    $this->startBot($bot->id);
                }
            } elseif($bot->check_position==1){
                $response= $this->clousePosition($bot, $bitmex);
                $mess= "<b>Notification!</b>\n";
                $mess.= "<b>".hex2bin('F09F8694')." Bot ID</b>: /b".$bot->id."\n";
                $mess.= "<b>Name</b>: ".(($bot->name)? $bot->name : $bot->currency_pair)." Count: ".$bot->count."\n";
                if($response) {
                    $mess.= "<b>Status</b>: Осталась открытая позиция ".$positon." кредитов. Позиция была закрыта по рынку!";
                } else {
                    $mess.= "<b>Status Error</b>: Позиция ".$positon." кредитов. Не была закрыта!";
                }
                $this->sendMess($bot->user_id, $mess);
                $this->telegramNotification($mess);
            } else {
                $bot->active = 1;
                $bot->save();
                $this->updatePosition(false, $bot, $bitmex);
                $this->createStopLoss($bot->id, $bitmex);


                $mess= "<b>Notification!</b>\n";
                $mess.= "<b>".hex2bin('F09F8694')." Bot ID</b>: /b".$bot->id."\n";
                $mess.= "<b>Name</b>: ".(($bot->name)? $bot->name : $bot->currency_pair)." Count: ".$bot->count."\n";
                $mess.= "<b>Status</b>: Осталась открытая позиция ".$positon." кредитов. Вы можете закрыть ее, или же подождать пока он закроется автоматически!";
                $this->sendMess($bot->user_id, $mess, [[['text'=>'Закрыть позицию', 'callback_data'=> 'action=close-position&id='.$bot->id]]]);
                $this->telegramNotification($mess);
            }
        }
    }
    public function setGrid(){
        set_time_limit(0);
        $this->cronLogs('setGrid');
        $bots= $this->getActiveBots([2,4])->where('start_date', '<', Carbon::now()->format('Y-m-d H:i:s'))->where('type', 'default');
        foreach ($bots as $bot) {
            $response= $this->startBot($bot->id);
        }
    }

    public function startTechBot() {
        $botsGroup= $this->getSettings([['active', '=', 2], ['type', 'technical']])->groupBy('currency_pair');
        foreach ($botsGroup as $key=> $bots) {
            $start= $this->checkStrategy($key);
//            dump($key);
//            dd($start);
            if($start) {
                foreach ($bots as $bot) {
                    $response= $this->startBot($bot->id);
                }
            }
        }
    }
}
