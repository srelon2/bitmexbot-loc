<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Mockery\Exception;
use Request;
use App\Models\Languages;
use Illuminate\Support\Facades\Artisan;

class LocaleMiddleware
{
    public static function mainLanguage() {
        $lang= Languages::where([
            ['default','=',1],
        ])->first();
        if(empty($lang)) {
            $lang= Languages::first()->key;
        } else {
            $lang= $lang->key;
        }

        return $lang;
    }
    public static function languages() {
        return Languages::pluck('key')->all();
    }
    /*
     * Проверяет наличие корректной метки языка в текущем URL
     * Возвращает метку или значеие null, если нет метки
     */
    public static function getLocale()
    {

        $uri = Request::path(); //получаем URI

        $segmentsURI = explode('/',$uri); //делим на части по разделителю "/"


        //Проверяем метку языка  - есть ли она среди доступных языков
        if (!empty($segmentsURI[0]) && in_array($segmentsURI[0], self::languages())) {

            if ($segmentsURI[0] != self::mainLanguage()) return $segmentsURI[0];

        }
        return null;
    }

    /*
    * Устанавливает язык приложения в зависимости от метки языка из URL
    */
    public function handle($request, Closure $next)
    {
        $locale = self::getLocale();
        if($locale) App::setLocale($locale);
        //если метки нет - устанавливаем основной язык $mainLanguage
        else App::setLocale(self::mainLanguage());

        return $next($request); //пропускаем дальше - передаем в следующий посредник
    }
}
